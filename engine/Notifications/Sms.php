<?php

namespace EA\Engine\Notifications;

use Config;
use infobip\api\client\SendMultipleTextualSmsAdvanced;
use infobip\api\configuration\BasicAuthConfiguration;
use infobip\api\model\Destination;
use infobip\api\model\sms\mt\send\Message;
use infobip\api\model\sms\mt\send\textual\SMSAdvancedTextualRequest;

/**
 * Created by PhpStorm.
 * User: mtellaeche
 * Date: 09/01/19
 * Time: 12:43
 *
 * @package Controllers
 */
class Sms
{
    /**
     * @param string $number
     * @return bool
     * @throws Exception
     */
    public function send(string $number, string $messageBody)
    {

        if ($this->checkNumber($number)) {
            $configuration = new BasicAuthConfiguration(Config::USER_SMS, Config::PASS_SMS, Config::URL_SMS);
            $client = new SendMultipleTextualSmsAdvanced($configuration);

            $destination = new Destination();
            $destination->setTo(Config::COD_PAIS. 9 .$number);

            $message = new Message();
            $message->setDestinations([$destination]);
            $message->setText($messageBody);

            $requestBody = new SMSAdvancedTextualRequest();
            $requestBody->setMessages([$message]);

            try {
                $apiResponse = $client->execute($requestBody);
                return true;
            } catch (Exception $apiCallException) {
                throw new Exception($apiCallException->getMessage(), $apiCallException->getCode());
            }
        }else{
            throw new Exception('El número ingresado es incorrecto.');
        }
    }

    protected function checkNumber(string $number)
    {
        return preg_match('/^[0-9]{10}+$/', $number);
    }
}