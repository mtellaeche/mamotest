DESCRIPCIÓN
========
Script para ejecutar External API odoo XML-RPC.

PRE-REQUISITOS
========

PHP 7.3
>$ ```php -v```

Extension XML-RPC
>$ ```sudo apt-get install php-xmlrpc```


CONFIGURACIÓN INICIAL
========

Colocar los valores en las constantes para conectar con el API de odoo XML-RPC; dentro de index.php

El script se ejecuta al realizar una solicitud `POST` con los siguientes parametros:


| PARAMETRO | VALOR |
| ------ | ------ |
| `$_GET['action']` | string=patients |
| `$_POST['x_Apellido']` | string |
| `$_POST['x_Nombre'` | string |
| `$_POST['x_DNI']` | string |

Estos son solo los campos requeridos, los comunes
tambien son tomados en cuenta.

AUTOR
========

* Christian Giménez [cgimenez@persiscalconsulting.com](cgimenez@persiscalconsulting.com)

COPYRIGHT
========
Persiscal Consulting
