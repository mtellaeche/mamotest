<?php

require_once('connecttoodoo.class.php');

//define("HOST", "https://mamotest-testing-792735.dev.odoo.com"); // ODOO HOST
define("HOST", "https://mamotest-testing-933184.dev.odoo.com"); // ODOO HOST
define("DATABASE", "mamotest-testing-933184"); // BASE DE DATOS
define("USERNAME", "ramirocorletti@gmail.com"); // USUARIO ODOO ADMINISTRADOR
define("PASSWORD", "rama123"); // CONTRASEÑA DE USUARIO ODOO ADMINISTRADOR

$c = new ConnectToOdoo(HOST, DATABASE, USERNAME, PASSWORD);

if (isset($_GET['action']) && $_GET['action'] == "patients") {

	if (isset($_POST['x_Nombre']) && isset($_POST['x_Apellido']) && isset($_POST['x_DNI'])) {
		$params = [];
		$params['x_is_patient'] = true;
		$params['x_is_doctor'] = false;

		$merge = array_merge($_POST, $params);
		$merge['x_Nombre'] = strtoupper($merge['x_Nombre']);
		$merge['x_Apellido'] = strtoupper($merge['x_Apellido']);
		$merge['name'] = $merge['x_Apellido'].' '.$merge['x_Nombre'];
		$merge['display_name'] = $merge['x_Apellido'].' '.$merge['x_Nombre'];
        //phone
        //email
        //email_formatted

		$findPatient = $c->searchRead('res.partner', [['x_DNI', '=', $merge['x_DNI']], ['x_is_patient', '=', true]], []);

		//var_dump($findPatient); exit();
		/*
		 * var_dump($findPatient);
		 * exit();
		*/

		if (count($findPatient) >= 1) {
		    $patient = current($findPatient);
			unset($merge['x_DNI']);

			$update = $c->update('res.partner', [$patient['id']], $merge);

			if ($update) {
				echo "Patient ".$patient['id']." updated";
			} else {
				echo "Error to updated";
			}
		} else {
            $merge['x_etiquetas_id'] = [
                [6, 0, [1]]
            ];

			$create = $c->create('res.partner', $merge);

			if ($create) {
				echo "Patient ".$create." created";
			} else {
				echo "Error to updated";
			}
		}
	} else {
		echo "Empty vars in POST \n";
	}
} else {
	echo "nothing to do";
}

/*
 * Ejemplo de ejecución por cada método
 */

// $create = $t->create('res.partner', ['x_Nombre' => strtoupper('Marianna'), 'x_Apellido' => strtoupper('Navarro'), 'name' => strtoupper('Navarro Marianna'), 'x_is_patient' => true, 'x_is_doctor' => false, 'x_DNI' => 12345678]);
// $update = $t->update('res.partner', [180327], ['phone' => '12345678']);
// $search =  $t->search('res.partner', [['is_company', '=', true], ['customer', '=', true]]);
// $read =  $t->read('res.partner', $find, ['name']);
// $searchRead = $t->searchRead('res.partner', [['id', '=', 176220]], ['id', 'x_DNI', 'display_name']);