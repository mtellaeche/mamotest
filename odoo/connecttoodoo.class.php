<?php

require_once('ripcord/ripcord.php');

class ConnectToOdoo {

	private $url;
	private $url_auth;
	private $url_exec;
	private $db;
	private $user;
	private $passwd;
	private $uid;

	public $models;

	function __construct(string $url, string $database, string $username, string $password) {

		$this->url_auth = $url.'/xmlrpc/2/common';
		$this->url_exec = $url.'/xmlrpc/2/object';
		$this->db = $database;
		$this->user = $username;
		$this->passwd = $password;

		$common = ripcord::client($this->url_auth);

		$this->uid = $common->authenticate($this->db, $this->user, $this->passwd, array());
		$this->models = ripcord::client($this->url_exec);
	}

	public function search(string $model, array $criteria) {

		return $this->models->execute_kw($this->db, $this->uid, $this->passwd,
			$model, 'search',
			array($criteria)
		);
	}

	public function read(string $model, array $ids, array $fields) {

		return $this->models->execute_kw($this->db, $this->uid, $this->passwd,
			$model, 'read',
			array($ids),
			array('fields' => $fields));
	}

	public function searchRead(string $model, array $criteria, array $fields) {

		return $this->models->execute_kw($this->db, $this->uid, $this->passwd,
			$model, 'search_read',
			array($criteria),
			array('fields' => $fields)
		);
	}

	public function create(string $model, array $fields) {

		return $this->models->execute_kw($this->db, $this->uid, $this->passwd,
			$model,
			'create',
			array($fields)
		);
	}

	public function update(string $model, array $id, array $fields) {

		return $this->models->execute_kw($this->db, $this->uid, $this->passwd,
			$model,
			'write',
			array($id, $fields)
		);
	}

}