-- MariaDB dump 10.17  Distrib 10.4.7-MariaDB, for Linux (x86_64)
--
-- Host: localhost    Database: mamotest
-- ------------------------------------------------------
-- Server version	10.4.7-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ea_appointments`
--

use eamamo;

DROP TABLE IF EXISTS `ea_appointments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ea_appointments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `book_datetime` datetime DEFAULT NULL,
  `start_datetime` datetime DEFAULT NULL,
  `end_datetime` datetime DEFAULT NULL,
  `notes` text DEFAULT NULL,
  `hash` text DEFAULT NULL,
  `is_unavailable` tinyint(4) DEFAULT 0,
  `id_users_provider` int(11) DEFAULT NULL,
  `id_users_customer` int(11) DEFAULT NULL,
  `id_services` int(11) DEFAULT NULL,
  `id_google_calendar` text DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `no_show` tinyint(4) DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `id_users_customer` (`id_users_customer`),
  KEY `id_services` (`id_services`),
  KEY `id_users_provider` (`id_users_provider`),
  KEY `created_by` (`created_by`),
  CONSTRAINT `appointments_created_by` FOREIGN KEY (`created_by`) REFERENCES `ea_users` (`id`),
  CONSTRAINT `appointments_services` FOREIGN KEY (`id_services`) REFERENCES `ea_services` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `appointments_users_customer` FOREIGN KEY (`id_users_customer`) REFERENCES `ea_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `appointments_users_provider` FOREIGN KEY (`id_users_provider`) REFERENCES `ea_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=204 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ea_appointments`
--

LOCK TABLES `ea_appointments` WRITE;
/*!40000 ALTER TABLE `ea_appointments` DISABLE KEYS */;
INSERT INTO `ea_appointments` VALUES (37,'2019-05-20 19:00:37','2019-05-21 08:30:00','2019-05-21 08:45:00',NULL,'f334d2f51f8c59d78b5ecbe18f8cb42e',0,2,22,2,NULL,NULL,0),(38,'2019-05-21 16:31:33','2019-05-27 14:00:00','2019-05-27 14:10:00',NULL,'009ec79d37a5b9e07f882403c2b74beb',0,11,23,8,NULL,NULL,0),(39,'2019-05-22 16:56:15','2019-06-11 08:00:00','2019-06-11 08:10:00',NULL,'7dbe79c7977ab7bac54c3cd59b2a9cc3',0,2,24,1,NULL,NULL,0),(40,'2019-05-22 19:10:31','2019-05-23 16:00:00','2019-05-23 16:10:00',NULL,'b4d6207cc5b39c0eb62aa09b8a75497a',0,7,25,4,NULL,NULL,0),(41,'2019-05-22 23:09:32','2019-06-06 13:00:00','2019-06-06 13:10:00',NULL,'d74a29e1447057ca359ee07193b4d80c',0,2,26,1,NULL,NULL,0),(42,'2019-05-23 07:52:41','2019-05-28 10:30:00','2019-05-28 10:45:00',NULL,'8f33fc66c55930d7cf8b5809cd3a9df3',0,2,27,2,NULL,NULL,0),(43,'2019-05-23 07:54:08','2019-05-23 09:00:00','2019-05-23 09:10:00',NULL,'b3114d7ce3109849d5b50d856e58339b',0,2,27,1,NULL,NULL,0),(44,'2019-05-23 09:41:41','2019-05-23 16:20:00','2019-05-23 16:30:00',NULL,'574312a13c53da6b3475a220767b9915',0,14,28,6,NULL,NULL,0),(45,'2019-05-23 20:06:04','2019-05-28 10:00:00','2019-05-28 10:10:00',NULL,'a99af9b4c13f53e8d5737cc79538a874',0,14,29,6,NULL,NULL,0),(46,'2019-05-24 09:06:12','2019-05-28 09:00:00','2019-05-28 09:10:00',NULL,'b4e262011084b411e1e0cb77d28bfa3a',0,11,30,8,NULL,NULL,1),(47,'2019-05-24 16:49:59','2019-05-28 09:00:00','2019-05-28 09:10:00',NULL,'ebaee5be31717d0135554ae597896c4a',0,7,31,4,NULL,NULL,0),(48,'2019-05-24 19:21:14','2019-05-30 09:30:00','2019-05-30 09:40:00',NULL,'89cef5798761d4d0a1a6caa9e5feac67',0,11,32,8,NULL,NULL,0),(50,'2019-05-26 23:34:45','2019-06-06 14:10:00','2019-06-06 14:20:00',NULL,'8da4a15b8dac61ba4932e35cd8993443',0,2,34,1,NULL,NULL,0),(51,'2019-05-27 13:56:27','2019-05-27 17:00:00','2019-05-27 17:10:00',NULL,'16e19aacf3d919c079dde65dd2a1efdd',0,14,35,6,NULL,NULL,0),(52,'2019-05-27 17:23:32','2019-05-29 17:45:00','2019-05-29 18:00:00',NULL,'4978dfa00770e3d8e6c1f071ca0fde71',0,11,36,9,NULL,NULL,0),(53,'2019-05-27 21:17:00','2019-08-08 15:00:00','2019-08-08 15:15:00',NULL,'976712df7280a4fb0bed01a9529aa80d',0,11,37,9,NULL,NULL,1),(54,'2019-05-28 07:58:42','2019-06-14 11:20:00','2019-06-14 11:30:00',NULL,'3963bef4c35d46f375b57a6614c4d7c5',0,7,38,4,NULL,NULL,1),(55,'2019-05-28 19:28:13','2019-05-30 18:00:00','2019-05-30 18:10:00',NULL,'dbdbb6722c93e772adb8ae9526e97b47',0,11,39,8,NULL,NULL,0),(56,'2019-05-28 19:32:54','2019-05-30 17:45:00','2019-05-30 18:00:00',NULL,'0b77c757bd1526cab3974898c79dce3a',0,11,39,9,NULL,NULL,0),(57,'2019-05-29 13:36:11','2019-05-30 15:00:00','2019-05-30 15:15:00',NULL,'0308f008cb995ac058a406aa2172968a',0,11,40,9,NULL,NULL,0),(58,'2019-05-29 17:27:26','2019-05-30 16:30:00','2019-05-30 16:40:00',NULL,'cbdf0176f433c4e1ff11f12bd66560b9',0,7,41,4,NULL,NULL,0),(60,'2019-05-30 14:12:58','2019-06-14 16:30:00','2019-06-14 16:45:00',NULL,'91fd177f0b8afe76ebd8a82a4f6b7a0b',0,11,42,9,NULL,NULL,0),(61,'2019-05-30 16:22:54','2019-06-05 09:30:00','2019-06-05 09:40:00',NULL,'aa08afdb6e90ee2798622da9886ace84',0,11,43,8,NULL,NULL,0),(62,'2019-05-30 19:57:32','2019-05-31 09:00:00','2019-05-31 09:10:00',NULL,'a9797382b7b285f2202333873574cd5a',0,2,44,1,NULL,NULL,1),(63,'2019-05-30 20:17:47','2019-06-07 11:15:00','2019-06-07 11:25:00',NULL,'22baf02c4f51e9ea7b4d5462189767a2',0,11,45,8,NULL,NULL,0),(64,'2019-05-31 06:52:01','2019-05-31 10:30:00','2019-05-31 10:40:00',NULL,'597b0266f7355115f7b2593fa605b583',0,11,46,8,NULL,NULL,0),(66,'2019-05-31 20:04:37','2019-06-06 15:00:00','2019-06-06 15:10:00',NULL,'f36392892a48dd81a722a17b09795438',0,11,48,8,NULL,NULL,0),(67,'2019-06-01 11:38:21','2019-06-01 12:40:00','2019-06-01 12:50:00',NULL,'7327f2983cb551ee6454df1bd1215da9',0,2,47,1,NULL,NULL,0),(68,'2019-06-01 12:19:40','2019-06-04 16:30:00','2019-06-04 16:40:00',NULL,'ea329d1dc5821d9c5d3231edae16d6f2',0,14,49,6,NULL,NULL,0),(70,'2019-06-03 09:23:58','2019-06-03 16:30:00','2019-06-03 16:40:00',NULL,'677d29694d1752b770e092bdc460e008',0,11,51,8,NULL,NULL,0),(71,'2019-06-03 09:29:39','2019-06-03 15:45:00','2019-06-03 16:00:00',NULL,'cbeac7de55fe766e07e54d8c7de751b1',0,11,51,9,NULL,NULL,0),(72,'2019-06-03 11:03:55','2019-06-03 16:00:00','2019-06-03 16:10:00',NULL,'baca36a8785fdbb3696434e5f2b5a81d',0,14,52,6,NULL,NULL,0),(73,'2019-06-04 10:26:00','2019-06-05 08:00:00','2019-06-05 08:10:00',NULL,'96615e1b246863d99535481417b6dcb0',0,11,53,8,NULL,NULL,0),(74,'2019-06-04 10:39:07','2019-06-05 12:15:00','2019-06-05 12:30:00',NULL,'ffec395ce93627a9960e53e5275e0bb2',0,11,53,9,NULL,NULL,0),(75,'2019-06-05 21:08:08','2019-06-07 14:00:00','2019-06-07 14:10:00',NULL,'e30ea5c44c7717154e25d10b82253b6a',0,2,54,1,NULL,NULL,1),(77,'2019-06-05 21:44:31','2019-06-11 10:15:00','2019-06-11 10:30:00',NULL,'a4096405bd26289a22319482d2c6b0f6',0,2,55,2,NULL,NULL,0),(78,'2019-06-05 21:47:47','2019-06-11 10:00:00','2019-06-11 10:10:00',NULL,'c0f03beff7cc77946ee064da3b83ec9e',0,2,55,1,NULL,NULL,0),(79,'2019-06-05 21:51:01','2019-06-17 10:45:00','2019-06-17 11:00:00',NULL,'4a7ea3c3a6aa7585c9598ae3017b2225',0,2,55,3,NULL,NULL,0),(80,'2019-06-06 22:57:19','2019-06-11 11:30:00','2019-06-11 11:45:00',NULL,'2ebe9e3633e35d118f9defc47403c467',0,2,56,2,NULL,NULL,0),(81,'2019-06-06 22:58:56','2019-06-11 11:20:00','2019-06-11 11:30:00',NULL,'f244746d8b8212f744ff6fd9f9775f5f',0,2,56,1,NULL,NULL,0),(82,'2019-06-07 09:46:26','2019-07-02 14:50:00','2019-07-02 15:00:00',NULL,'15fadadb8afe33be83df3039f00b569c',0,11,57,8,NULL,NULL,0),(83,'2019-06-07 13:40:31','2019-06-18 11:00:00','2019-06-18 11:10:00',NULL,'9e9bc75431b86242be1ed146e5ee6b76',0,7,58,4,NULL,NULL,0),(84,'2019-06-07 13:41:33','2019-06-07 18:00:00','2019-06-07 18:10:00',NULL,'ca79ee1fb4162bf9a3ffcb4c962a5298',0,11,59,8,NULL,NULL,0),(85,'2019-06-07 17:15:51','2019-06-10 08:40:00','2019-06-10 08:50:00',NULL,'1862da82b5c5def6de5a4a12b632a0ff',0,14,60,6,NULL,NULL,0),(86,'2019-06-09 19:29:46','2019-06-13 10:45:00','2019-06-13 10:55:00',NULL,'39eb67dfe979170473e4013506f09aea',0,11,61,8,NULL,NULL,0),(87,'2019-06-09 21:49:50','2019-06-10 17:50:00','2019-06-10 18:00:00',NULL,'f63903daeb4e33024c34924f43a1bd73',0,14,62,6,NULL,NULL,0),(88,'2019-06-10 08:43:57','2019-06-11 09:30:00','2019-06-11 09:45:00',NULL,'a191d63bc157493c5b67fcb6ff56d915',0,2,63,2,NULL,NULL,0),(90,'2019-06-11 11:42:01','2019-06-14 08:15:00','2019-06-14 08:25:00',NULL,'12ed7ed5949ad503eabbe14728eeaa11',0,11,65,8,NULL,NULL,0),(91,'2019-06-11 11:45:55','2019-06-14 12:15:00','2019-06-14 12:30:00',NULL,'35f524204f062ecf254c918754fb439b',0,11,65,9,NULL,NULL,0),(92,'2019-06-11 18:34:57','2019-06-12 10:00:00','2019-06-12 10:10:00',NULL,'045fc31cf71cfafddbcca785266b2174',0,14,66,6,NULL,NULL,0),(94,'2019-06-12 15:27:06','2019-06-18 15:00:00','2019-06-18 15:15:00',NULL,'6ef6e1519822e9a4dcf0535846268183',0,11,68,9,NULL,NULL,0),(95,'2019-06-12 18:18:59','2019-06-18 11:00:00','2019-06-18 11:10:00',NULL,'3326bfb26050afcd81416e422f87aa8a',0,7,58,4,NULL,NULL,0),(96,'2019-06-12 21:08:28','2019-06-15 10:40:00','2019-06-15 10:50:00',NULL,'54e02e1a320b93660fa661dabe325516',0,2,69,1,NULL,NULL,0),(97,'2019-06-12 21:15:12','2019-06-18 08:00:00','2019-06-18 08:15:00',NULL,'b4b9da725c678d691473467391ce782e',0,2,69,2,NULL,NULL,1),(98,'2019-06-13 01:44:08','2019-06-28 09:00:00','2019-06-28 09:10:00',NULL,'f9be8bf05cc187c6f73b9019329735e3',0,2,70,1,NULL,NULL,1),(99,'2019-06-13 18:38:03','2019-06-21 15:45:00','2019-06-21 16:00:00',NULL,'b5ee6ced2553c41b0131639586bbaee7',0,11,71,9,NULL,NULL,0),(100,'2019-06-14 16:17:33','2019-06-24 09:10:00','2019-06-24 09:20:00',NULL,'550121a89f3c3cce48c3480038e64bc0',0,2,72,1,NULL,NULL,0),(101,'2019-06-14 21:50:28','2019-06-18 11:20:00','2019-06-18 11:30:00',NULL,'bc5aa2ede8bd5251f1c3c3120756cb28',0,7,73,4,NULL,NULL,0),(102,'2019-06-18 03:21:24','2019-06-21 15:00:00','2019-06-21 15:15:00',NULL,'04ed1d6cef1ef2250f2a8e804d06915f',0,11,74,9,NULL,NULL,0),(104,'2019-06-19 11:12:21','2019-06-21 08:40:00','2019-06-21 08:50:00',NULL,'af03892b02fd13548f7dd992ec4dd8ae',0,14,76,6,NULL,NULL,0),(105,'2019-06-19 20:32:52','2019-06-27 09:20:00','2019-06-27 09:30:00',NULL,'6fd5675ec0589ca09126c09b8e22c03d',0,7,77,4,NULL,NULL,1),(106,'2019-06-20 17:25:32','2019-06-24 09:00:00','2019-06-24 09:10:00',NULL,'2fe6919e1c3380154129ae11cbed06d0',0,2,78,1,NULL,NULL,0),(107,'2019-06-22 10:24:53','2019-06-26 09:00:00','2019-06-26 09:10:00',NULL,'b505499b3fa6f80babbfe5d907e09cb1',0,7,79,4,NULL,NULL,1),(108,'2019-06-23 23:32:08','2019-06-24 16:15:00','2019-06-24 16:25:00',NULL,'ed193e50136a763e2d5b3ad15fd20f80',0,11,80,8,NULL,NULL,1),(109,'2019-06-24 08:35:13','2019-06-24 17:50:00','2019-06-24 18:00:00',NULL,'029fa25767bf0a9beee9c6ef7e9bde78',0,7,81,4,NULL,NULL,0),(110,'2019-06-24 18:50:50','2019-06-28 10:50:00','2019-06-28 11:00:00',NULL,'d7d7c5ac79df100044c325cd2bab5e5d',0,2,82,1,NULL,NULL,0),(111,'2019-06-25 12:18:11','2019-06-25 16:20:00','2019-06-25 16:30:00',NULL,'e36c5dda146a548d12ec503b5bdfc89d',0,14,83,6,NULL,NULL,0),(112,'2019-06-25 14:35:24','2019-06-28 17:00:00','2019-06-28 17:10:00',NULL,'7fa0842dff1b297b23f7ab393b6b1fec',0,14,84,6,NULL,NULL,0),(113,'2019-06-25 16:44:50','2019-07-02 17:00:00','2019-07-02 17:10:00',NULL,'e0420ff53035092dff5b93a74f743dbd',0,7,85,4,NULL,NULL,0),(116,'2019-06-25 17:32:55','2019-07-02 17:30:00','2019-07-02 17:40:00',NULL,'3ab0cb2eab1a3d81c555a1890dc503a3',0,7,87,4,NULL,NULL,0),(118,'2019-06-25 21:15:16','2019-07-12 09:00:00','2019-07-12 09:10:00',NULL,'23f34af8401e7a81162ad458a69ada27',0,14,88,6,NULL,NULL,0),(119,'2019-06-25 21:20:19','2019-07-12 09:10:00','2019-07-12 09:20:00',NULL,'7a9db0aba1021904949b66062db35b3a',0,14,88,6,NULL,NULL,0),(120,'2019-06-25 21:21:56','2019-07-12 09:20:00','2019-07-12 09:30:00',NULL,'1e4c23c2d6e22df7204bfd8937c110d4',0,14,88,6,NULL,NULL,0),(121,'2019-06-26 11:04:53','2019-07-15 08:00:00','2019-07-15 08:10:00',NULL,'7df20e59946326fec11e1f438746ef4e',0,14,89,6,NULL,NULL,0),(122,'2019-06-26 11:22:48','2019-07-03 15:30:00','2019-07-03 15:40:00',NULL,'7ad210ee4b96ac46376acfbb10598f86',0,11,90,8,NULL,NULL,0),(123,'2019-06-26 11:24:21','2019-07-03 17:00:00','2019-07-03 17:15:00',NULL,'58d579102474d5a10f5148bb54c14038',0,11,90,9,NULL,NULL,0),(124,'2019-06-26 13:17:39','2019-06-28 17:30:00','2019-06-28 17:40:00',NULL,'da05688d077b8555822913ece7939408',0,11,91,8,NULL,NULL,0),(125,'2019-06-26 17:07:35','2019-07-01 09:40:00','2019-07-01 09:50:00',NULL,'6c76caa2bc063d880dd6b5627f30a0d9',0,7,92,4,NULL,NULL,0),(126,'2019-06-28 11:06:40','2019-07-02 09:15:00','2019-07-02 09:25:00',NULL,'9952413fc8be326c64d1e0697e8b62eb',0,11,93,8,NULL,NULL,0),(128,'2019-07-01 09:16:17','2019-07-05 17:15:00','2019-07-05 17:25:00',NULL,'bdde70052eff2fc77f6c207e77f94c51',0,11,67,8,NULL,NULL,1),(129,'2019-07-02 09:37:13','2019-07-16 14:00:00','2019-07-16 14:10:00',NULL,'21610e0537850fea50ca6555edc47292',0,11,95,8,NULL,NULL,0),(130,'2019-07-02 12:17:21','2019-07-05 16:00:00','2019-07-05 16:10:00',NULL,'bcb7bdd933abb54713bb44e10b45bc58',0,12,96,7,NULL,NULL,0),(131,'2019-07-02 15:02:20','2019-07-05 15:45:00','2019-07-05 16:00:00',NULL,'14fe4ec50bbb344118e0b49b9c14c746',0,11,97,9,NULL,NULL,0),(132,'2019-07-02 20:18:58','2019-07-04 15:00:00','2019-07-04 15:15:00',NULL,'9210fa243877b62dfb496b6374e60c34',0,11,98,9,NULL,NULL,0),(133,'2019-07-03 15:14:32','2019-07-11 10:00:00','2019-07-11 10:10:00',NULL,'f014c0e42bc6a0dadb3fd23dc709770a',0,7,99,4,NULL,NULL,0),(134,'2019-07-03 17:46:43','2019-07-03 18:20:00','2019-07-03 18:30:00',NULL,'fb1da533bd183d68a093e7da10d7a207',0,7,100,4,NULL,NULL,1),(135,'2019-07-03 17:56:14','2019-07-05 10:00:00','2019-07-05 10:10:00',NULL,'ec47070c755659bb4c835899563fbce1',0,14,101,6,NULL,NULL,0),(136,'2019-07-03 18:26:39','2019-07-08 17:20:00','2019-07-08 17:30:00',NULL,'81fc34559a397e863255af3fb4109d30',0,14,102,6,NULL,NULL,0),(137,'2019-07-03 23:13:11','2019-07-11 12:15:00','2019-07-11 12:25:00',NULL,'bc1c9de6355e70b52cc812f23b4e892b',0,11,103,8,NULL,NULL,0),(142,'2019-07-09 10:00:05','2019-07-18 08:00:00','2019-07-18 08:10:00',NULL,'2b63ce0aced697e7c831fa019e0f859d',0,7,107,4,NULL,NULL,0),(144,'2019-07-10 18:02:31','2019-07-12 13:15:00','2019-07-12 13:25:00',NULL,'6de01ffbfee1873306ddc41c121ba0cd',0,11,108,8,NULL,NULL,0),(145,'2019-07-11 13:23:27','2019-07-25 10:30:00','2019-07-25 10:40:00',NULL,'81d485259bf5e55b3d60825993f06dd2',0,11,75,8,NULL,NULL,0),(147,'2019-07-13 07:36:23','2019-07-16 08:00:00','2019-07-16 08:10:00',NULL,'cc533cfa8b80b155fcc3928a06f237bb',0,14,110,6,NULL,NULL,0),(148,'2019-07-13 13:59:43','2019-07-15 10:30:00','2019-07-15 10:40:00',NULL,'fc9b73b8cea4fb3b4d0ded0e784b5d01',0,11,111,8,NULL,NULL,1),(149,'2019-07-13 16:40:18','2019-07-17 17:15:00','2019-07-17 17:30:00',NULL,'0f289b4e25d28892a8eee3b36605d345',0,11,112,9,NULL,NULL,0),(150,'2019-07-13 16:43:05','2019-07-17 15:45:00','2019-07-17 15:55:00',NULL,'70078b377ba66ba4c9721081d0e36052',0,11,112,8,NULL,NULL,0),(152,'2019-07-15 09:58:52','2019-07-18 16:00:00','2019-07-18 16:15:00',NULL,'e5869bc29db8ec108ce781a3454fb616',0,11,114,9,NULL,NULL,0),(153,'2019-07-15 11:34:25','2019-07-19 09:40:00','2019-07-19 09:50:00',NULL,'14165f87e15547bf3d3f3fd1ed7260a8',0,14,115,6,NULL,NULL,0),(154,'2019-07-15 17:54:56','2019-07-16 11:30:00','2019-07-16 11:40:00',NULL,'915e2efe9b38ed3253efca082ccc0c83',0,14,116,6,NULL,NULL,0),(155,'2019-07-15 22:18:04','2019-07-17 08:00:00','2019-07-17 08:10:00',NULL,'a606fb1c1b9949e02da9a614e4fbd815',0,14,117,6,NULL,NULL,0),(157,'2019-07-16 22:22:42','2019-07-18 16:00:00','2019-07-18 16:10:00',NULL,'f5610069b0e836a3f24987f3bbeeb6ce',0,7,41,4,NULL,NULL,1),(160,'2019-07-17 10:32:14','2019-07-19 18:30:00','2019-07-19 18:40:00',NULL,'686c4abbf309897366615c0a2c2f1f85',0,11,120,8,NULL,NULL,0),(161,'2019-07-17 10:52:00','2019-07-19 10:00:00','2019-07-19 10:10:00',NULL,'2af57afa490a4881dd5f86812926722d',0,14,115,6,NULL,NULL,1),(162,'2019-07-17 12:44:24','2019-07-18 16:00:00','2019-07-18 16:10:00',NULL,'a9e922db28d98ad6762bf07b6517f3b7',0,7,121,4,NULL,NULL,0),(164,'2019-07-18 20:31:54','2019-07-19 16:00:00','2019-07-19 16:10:00',NULL,'e682d2fec48047fd48eda075cb2c8639',0,7,122,4,NULL,NULL,0),(165,'2019-07-22 09:44:51','2019-07-22 16:00:00','2019-07-22 16:10:00',NULL,'efb2c3b6a3826c65ed6b5039851ae1c6',0,14,123,6,NULL,NULL,0),(166,'2019-07-22 10:46:34','2019-08-13 10:30:00','2019-08-13 10:40:00',NULL,'298bd6202fde0edafaac7e1107ad5c31',0,11,124,8,NULL,NULL,0),(167,'2019-07-22 11:07:04','2019-07-26 09:00:00','2019-07-26 09:10:00',NULL,'a82a9f4a3a11679ebb12850ad7f5a4a7',0,13,125,5,NULL,NULL,0),(168,'2019-07-22 17:39:11','2019-07-25 09:40:00','2019-07-25 09:50:00',NULL,'b18025454916bd0165f09a9efee45ae7',0,14,126,6,NULL,NULL,0),(169,'2019-07-23 10:32:57','2019-07-23 18:45:00','2019-07-23 18:55:00',NULL,'e902988eecc1b5cddbf5d1b96fe19fb6',0,11,127,8,NULL,NULL,0),(170,'2019-07-23 22:45:08','2019-07-24 09:40:00','2019-07-24 09:50:00',NULL,'4b7b6b92c4d8fee8a8cc030cdef3f0d8',0,14,128,6,NULL,NULL,0),(171,'2019-07-24 13:45:50','2019-07-25 15:00:00','2019-07-25 15:10:00',NULL,'e8615f351e92cbf551b575961ec112ec',0,11,129,8,NULL,NULL,0),(172,'2019-07-24 13:47:31','2019-07-26 15:00:00','2019-07-26 15:15:00',NULL,'2ad7f4baa446a44ccce9f5f3ecdcc187',0,11,129,9,NULL,NULL,0),(173,'2019-07-25 19:48:17','2019-07-26 09:40:00','2019-07-26 09:50:00',NULL,'be01ccffa2baa6e39e8419620880fb1f',0,14,130,6,NULL,NULL,0),(174,'2019-07-26 10:49:24','2019-07-30 16:45:00','2019-07-30 17:00:00',NULL,'a6522a478966a479d80272686da05d8a',0,11,131,9,NULL,NULL,0),(175,'2019-07-26 12:02:11','2019-07-31 10:30:00','2019-07-31 10:40:00',NULL,'6758e25ab281a27b28d16e6af59bf1f6',0,11,132,8,NULL,NULL,0),(176,'2019-07-26 19:07:27','2019-07-29 10:00:00','2019-07-29 10:10:00',NULL,'d8480e11179ee9f58b358911141c89c4',0,11,133,8,NULL,NULL,0),(177,'2019-07-27 20:52:06','2019-08-07 12:00:00','2019-08-07 12:15:00',NULL,'6b71d2e5a38c4bed80c2ca59fd153333',0,11,134,9,NULL,NULL,0),(178,'2019-07-29 07:44:11','2019-07-29 08:20:00','2019-07-29 08:30:00',NULL,'e6ab0dfd8f7e1a7db54ea5b22fec321e',0,14,135,6,NULL,NULL,0),(179,'2019-07-29 19:26:15','2019-08-09 09:40:00','2019-08-09 09:50:00',NULL,'af6403a6768905f51f14e00e791c3975',0,14,136,6,NULL,NULL,0),(180,'2019-07-30 17:07:05','2019-07-30 18:30:00','2019-07-30 18:40:00',NULL,'7b18235efb444ac4b6e76893428b3efc',0,11,137,8,NULL,NULL,0),(181,'2019-07-30 18:04:19','2019-07-31 09:00:00','2019-07-31 09:10:00',NULL,'822ae820395c2b399e698a7f7b5b3473',0,11,138,8,NULL,NULL,0),(182,'2019-07-31 18:42:43','2019-07-31 19:15:00','2019-07-31 19:25:00',NULL,'da15cdfde7b60eaa82dafa206e594f71',0,11,139,8,NULL,NULL,1),(183,'2019-08-01 00:05:38','2019-08-09 10:00:00','2019-08-09 10:10:00',NULL,'52dc0ff75eb078a140fa6fdac1fe62ac',0,12,140,7,NULL,NULL,1),(184,'2019-08-01 07:06:48','2019-08-15 18:00:00','2019-08-15 18:10:00',NULL,'8d91788822d67f196e60b2173bc8fa1c',0,7,141,4,NULL,NULL,0),(185,'2019-08-01 09:57:16','2019-08-01 13:15:00','2019-08-01 13:25:00',NULL,'cbbaaa2993dec2af0bda20b8735087ff',0,11,142,8,NULL,NULL,0),(186,'2019-08-01 12:39:03','2019-08-05 08:30:00','2019-08-05 08:40:00',NULL,'bcbb515136a3a0d4949250e0d8980864',0,11,143,8,NULL,NULL,0),(188,'2019-08-03 23:21:47','2019-08-12 09:00:00','2019-08-12 09:10:00',NULL,'f7c2516d44420c6ba8222363c12025fc',0,11,145,8,NULL,NULL,1),(189,'2019-08-04 08:35:01','2019-08-05 09:40:00','2019-08-05 09:50:00',NULL,'dfbde48bcb1e4d23c82805b99f3ffe7f',0,12,146,7,NULL,NULL,1),(190,'2019-08-04 11:51:40','2019-08-09 18:30:00','2019-08-09 18:40:00',NULL,'61d7fbc6cf998553d65daaeb812b14ba',0,13,147,5,NULL,NULL,0),(192,'2019-08-04 22:44:57','2019-08-26 13:00:00','2019-08-26 13:15:00',NULL,'2f19423420b06ec3de636fb196ffd93d',0,11,148,9,NULL,NULL,0),(193,'2019-08-05 08:15:35','2019-08-23 08:00:00','2019-08-23 08:10:00',NULL,'4355174a3c1b164c9e3cf436eb9cfe82',0,11,149,8,NULL,NULL,0),(196,'2019-08-06 16:50:42','2019-08-07 10:30:00','2019-08-07 10:40:00',NULL,'42b4b2837ccde948bb6981a3ef0897f3',0,11,151,8,NULL,NULL,0),(197,'2019-08-10 02:18:22','2019-08-12 09:00:00','2019-08-12 09:10:00',NULL,'ebf17aa3d83a97e1408b907e313a1d73',0,7,152,4,NULL,NULL,0),(198,'2019-08-11 08:54:59','2019-08-13 08:00:00','2019-08-13 08:10:00',NULL,'fb987a1fb4194f20ee75eee7d89ebafa',0,13,153,5,NULL,NULL,0),(199,'2019-08-12 18:45:10','2019-08-13 15:00:00','2019-08-13 15:15:00',NULL,'2dcfbc47c7a913cb57e48e6d10480643',0,11,154,9,NULL,NULL,0),(200,'2019-08-13 04:39:49','2019-08-23 14:00:00','2019-08-23 14:15:00',NULL,'70394be92d0f1a527c2cd8eec6fcd4f5',0,11,155,9,NULL,NULL,0),(201,'2019-08-13 08:37:08','2019-08-14 17:00:00','2019-08-14 17:10:00',NULL,'17dd839de25f6f3fb756643b875ada2a',0,12,156,7,NULL,NULL,0),(202,'2019-08-13 16:06:45','2019-08-13 16:40:00','2019-08-13 16:50:00',NULL,'204dfe3ee5f145914d4d42e2d420cb85',0,14,157,6,NULL,NULL,0),(203,'2019-08-13 22:16:13','2019-08-29 18:30:00','2019-08-29 18:40:00',NULL,'bfbdcc6e5bc098286bf851098b648919',0,7,158,4,NULL,NULL,0);
/*!40000 ALTER TABLE `ea_appointments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ea_consents`
--

DROP TABLE IF EXISTS `ea_consents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ea_consents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT current_timestamp(),
  `modified` datetime DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `first_name` varchar(256) DEFAULT NULL,
  `last_name` varchar(256) DEFAULT NULL,
  `email` varchar(512) DEFAULT NULL,
  `ip` varchar(256) DEFAULT NULL,
  `type` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ea_consents`
--

LOCK TABLES `ea_consents` WRITE;
/*!40000 ALTER TABLE `ea_consents` DISABLE KEYS */;
/*!40000 ALTER TABLE `ea_consents` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ea_migrations`
--

DROP TABLE IF EXISTS `ea_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ea_migrations` (
  `version` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ea_migrations`
--

LOCK TABLES `ea_migrations` WRITE;
/*!40000 ALTER TABLE `ea_migrations` DISABLE KEYS */;
INSERT INTO `ea_migrations` VALUES (12);
/*!40000 ALTER TABLE `ea_migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ea_roles`
--

DROP TABLE IF EXISTS `ea_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ea_roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(256) DEFAULT NULL,
  `slug` varchar(256) DEFAULT NULL,
  `is_admin` tinyint(4) DEFAULT NULL,
  `appointments` int(11) DEFAULT NULL,
  `customers` int(11) DEFAULT NULL,
  `services` int(11) DEFAULT NULL,
  `users` int(11) DEFAULT NULL,
  `system_settings` int(11) DEFAULT NULL,
  `user_settings` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ea_roles`
--

LOCK TABLES `ea_roles` WRITE;
/*!40000 ALTER TABLE `ea_roles` DISABLE KEYS */;
INSERT INTO `ea_roles` VALUES (1,'Administrator','admin',1,15,15,15,15,15,15),(2,'Provider','provider',0,15,15,0,0,0,15),(3,'Customer','customer',0,0,0,0,0,0,0),(4,'Secretary','secretary',0,15,15,0,0,0,15);
/*!40000 ALTER TABLE `ea_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ea_secretaries_providers`
--

DROP TABLE IF EXISTS `ea_secretaries_providers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ea_secretaries_providers` (
  `id_users_secretary` int(11) NOT NULL,
  `id_users_provider` int(11) NOT NULL,
  PRIMARY KEY (`id_users_secretary`,`id_users_provider`),
  KEY `id_users_secretary` (`id_users_secretary`),
  KEY `id_users_provider` (`id_users_provider`),
  CONSTRAINT `secretaries_users_provider` FOREIGN KEY (`id_users_provider`) REFERENCES `ea_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `secretaries_users_secretary` FOREIGN KEY (`id_users_secretary`) REFERENCES `ea_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ea_secretaries_providers`
--

LOCK TABLES `ea_secretaries_providers` WRITE;
/*!40000 ALTER TABLE `ea_secretaries_providers` DISABLE KEYS */;
INSERT INTO `ea_secretaries_providers` VALUES (4,2),(10,7),(21,2),(21,7),(21,11),(21,12),(21,13),(21,14);
/*!40000 ALTER TABLE `ea_secretaries_providers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ea_service_categories`
--

DROP TABLE IF EXISTS `ea_service_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ea_service_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(256) DEFAULT NULL,
  `description` text DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ea_service_categories`
--

LOCK TABLES `ea_service_categories` WRITE;
/*!40000 ALTER TABLE `ea_service_categories` DISABLE KEYS */;
INSERT INTO `ea_service_categories` VALUES (1,'Centro Constitucion',''),(2,'Centro Curuzú Cuatia',''),(3,'Centro Corrientes TC2000',''),(4,'Centro Esquina',''),(5,'Centro Paso de los Libres',''),(6,'Centro Saenz Peña','');
/*!40000 ALTER TABLE `ea_service_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ea_services`
--

DROP TABLE IF EXISTS `ea_services`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ea_services` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(256) DEFAULT NULL,
  `duration` int(11) DEFAULT NULL,
  `price` decimal(10,2) DEFAULT NULL,
  `currency` varchar(32) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `active_warning` tinyint(4) DEFAULT 0,
  `warning` text DEFAULT NULL,
  `availabilities_type` varchar(32) DEFAULT 'flexible',
  `attendants_number` int(11) DEFAULT 1,
  `id_service_categories` int(11) DEFAULT NULL,
  `working_plan` text DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_service_categories` (`id_service_categories`),
  CONSTRAINT `services_service_categories` FOREIGN KEY (`id_service_categories`) REFERENCES `ea_service_categories` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ea_services`
--

LOCK TABLES `ea_services` WRITE;
/*!40000 ALTER TABLE `ea_services` DISABLE KEYS */;
INSERT INTO `ea_services` VALUES (1,'Mamografia ',10,980.00,'ARS','El valor de este servicio es de referencia, puede variar de acuerdo a promociones vigentes.',1,'----- SERVICIO NO DISPONIBLE -----\n\nMonto a abonar: $980\nMedio de pago: Efectivo - Tarjeta Débito - Tarjeta Crédito (en cuotas sin interés)','fixed',5,1,'{\"sunday\":null,\"monday\":null,\"tuesday\":null,\"wednesday\":null,\"thursday\":null,\"friday\":null,\"saturday\":null}'),(2,'Ecografía Mamaria',15,700.00,'ARS','El valor de este servicio es de referencia, puede variar de acuerdo a promociones vigentes.',1,'----- SERVICIO NO DISPONIBLE -----\n\nMonto a abonar: $700\nMedio de pago: Efectivo - Tarjeta Débito - Tarjeta Crédito (en cuotas sin interés)','fixed',5,1,'{\"sunday\":null,\"monday\":null,\"tuesday\":null,\"wednesday\":null,\"thursday\":null,\"friday\":null,\"saturday\":null}'),(3,'Interpretación de Resultados',15,500.00,'ARS','El valor de este servicio es de referencia, puede variar de acuerdo a promociones vigentes.',1,'----- SERVICIO NO DISPONIBLE -----\n\nEl servicio incluye una consulta con una médica mastóloga, y la interpretación de los resultados de la mamografía o ecografía mamaria. \n\nMonto a abonar: $500\nMedio de pago: Efectivo - Tarjeta Débito - Tarjeta Crédito (en cuotas sin interés)','fixed',5,1,'{\"sunday\":null,\"monday\":null,\"tuesday\":null,\"wednesday\":null,\"thursday\":null,\"friday\":null,\"saturday\":null}'),(4,'Mamografía',10,0.00,'ARS','El valor de este servicio es de referencia, puede variar de acuerdo a promociones vigentes y convenio con Obras Sociales',1,'Obras Sociales con orden: Sin Costo (No cobramos plus)\nParticulares Mamografía digital: $1.200\nParticulares Magnificación: $600\n','fixed',5,2,'{\"sunday\":null,\"monday\":{\"start\":\"08:00\",\"end\":\"20:00\",\"breaks\":[{\"start\":\"12:00\",\"end\":\"16:00\"}]},\"tuesday\":{\"start\":\"08:00\",\"end\":\"20:00\",\"breaks\":[{\"start\":\"12:00\",\"end\":\"16:00\"}]},\"wednesday\":{\"start\":\"08:00\",\"end\":\"20:00\",\"breaks\":[{\"start\":\"12:00\",\"end\":\"16:00\"}]},\"thursday\":{\"start\":\"08:00\",\"end\":\"20:00\",\"breaks\":[{\"start\":\"12:00\",\"end\":\"16:00\"}]},\"friday\":{\"start\":\"08:00\",\"end\":\"20:00\",\"breaks\":[{\"start\":\"12:00\",\"end\":\"16:00\"}]},\"saturday\":null}'),(5,'Mamografía',10,0.00,'ARS','El valor de este servicio es de referencia, puede variar de acuerdo a promociones vigentes y convenio con Obras Sociales',1,'Obras Sociales con orden: Sin Costo (No cobramos plus)\nParticulares Mamografía digital: $1.200\nParticulares Magnificación: $600','fixed',5,4,'{\"sunday\":null,\"monday\":{\"start\":\"08:00\",\"end\":\"19:00\",\"breaks\":[{\"start\":\"12:00\",\"end\":\"17:00\"}]},\"tuesday\":{\"start\":\"08:00\",\"end\":\"19:00\",\"breaks\":[{\"start\":\"12:00\",\"end\":\"17:00\"}]},\"wednesday\":{\"start\":\"08:00\",\"end\":\"19:00\",\"breaks\":[{\"start\":\"12:00\",\"end\":\"17:00\"}]},\"thursday\":{\"start\":\"08:00\",\"end\":\"19:00\",\"breaks\":[{\"start\":\"12:00\",\"end\":\"17:00\"}]},\"friday\":{\"start\":\"08:00\",\"end\":\"19:00\",\"breaks\":[{\"start\":\"12:00\",\"end\":\"17:00\"}]},\"saturday\":null}'),(6,'Mamografía',10,0.00,'ARS','El valor de este servicio es de referencia, puede variar de acuerdo a promociones vigentes y convenio con Obras Sociales',1,'Obras Sociales con orden: Sin Costo (No cobramos plus)\nParticulares Mamografía digital: $1.200\nParticulares Magnificación: $600','fixed',5,6,'{\"sunday\":null,\"monday\":{\"start\":\"08:00\",\"end\":\"18:00\",\"breaks\":[{\"start\":\"12:00\",\"end\":\"16:00\"}]},\"tuesday\":{\"start\":\"08:00\",\"end\":\"18:00\",\"breaks\":[{\"start\":\"12:00\",\"end\":\"16:00\"}]},\"wednesday\":{\"start\":\"08:00\",\"end\":\"18:00\",\"breaks\":[{\"start\":\"12:00\",\"end\":\"16:00\"}]},\"thursday\":{\"start\":\"08:00\",\"end\":\"18:00\",\"breaks\":[{\"start\":\"12:00\",\"end\":\"16:00\"}]},\"friday\":{\"start\":\"08:00\",\"end\":\"18:00\",\"breaks\":[{\"start\":\"12:00\",\"end\":\"16:00\"}]},\"saturday\":null}'),(7,'Mamografía',10,0.00,'ARS','El valor de este servicio es de referencia, puede variar de acuerdo a promociones vigentes y convenio con Obras Sociales',1,'Obras Sociales con orden: Sin Costo (No cobramos plus)\nParticulares Mamografía digital: $1.200\nParticulares Magnificación: $600','fixed',5,5,'{\"sunday\":null,\"monday\":{\"start\":\"08:00\",\"end\":\"19:00\",\"breaks\":[{\"start\":\"12:00\",\"end\":\"17:00\"}]},\"tuesday\":{\"start\":\"08:00\",\"end\":\"19:00\",\"breaks\":[{\"start\":\"12:00\",\"end\":\"17:00\"}]},\"wednesday\":{\"start\":\"08:00\",\"end\":\"19:00\",\"breaks\":[{\"start\":\"12:00\",\"end\":\"17:00\"}]},\"thursday\":{\"start\":\"08:00\",\"end\":\"19:00\",\"breaks\":[{\"start\":\"12:00\",\"end\":\"17:00\"}]},\"friday\":{\"start\":\"08:00\",\"end\":\"18:00\",\"breaks\":[{\"start\":\"12:00\",\"end\":\"16:00\"}]},\"saturday\":null}'),(8,'Mamografía',10,0.00,'ARS','El valor de este servicio es de referencia, puede variar de acuerdo a promociones vigentes y convenio con Obras Sociales',1,'Obras Sociales con orden: Sin Costo (No cobramos plus)\nParticulares Mamografía digital: $1.200\nParticulares Magnificación: $600','flexible',5,3,'{\"sunday\":null,\"monday\":{\"start\":\"08:00\",\"end\":\"20:00\",\"breaks\":[]},\"tuesday\":{\"start\":\"08:00\",\"end\":\"20:00\",\"breaks\":[]},\"wednesday\":{\"start\":\"08:00\",\"end\":\"20:00\",\"breaks\":[]},\"thursday\":{\"start\":\"08:00\",\"end\":\"20:00\",\"breaks\":[]},\"friday\":{\"start\":\"08:00\",\"end\":\"20:00\",\"breaks\":[]},\"saturday\":null}'),(9,'Ecografía Mamaria',15,0.00,'ARS','El valor de este servicio es de referencia, puede variar de acuerdo a promociones vigentes y convenio con Obras Sociales',0,'Obras Sociales con orden: Sin Costo (No cobramos plus)\nParticulares Ecografía: $900','flexible',3,3,'{\"sunday\":null,\"monday\":{\"start\":\"12:00\",\"end\":\"16:00\",\"breaks\":[]},\"tuesday\":{\"start\":\"15:00\",\"end\":\"18:00\",\"breaks\":[]},\"wednesday\":{\"start\":\"12:00\",\"end\":\"18:00\",\"breaks\":[]},\"thursday\":{\"start\":\"15:00\",\"end\":\"18:00\",\"breaks\":[]},\"friday\":{\"start\":\"12:00\",\"end\":\"16:00\",\"breaks\":[]},\"saturday\":null}');
/*!40000 ALTER TABLE `ea_services` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ea_services_providers`
--

DROP TABLE IF EXISTS `ea_services_providers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ea_services_providers` (
  `id_users` int(11) NOT NULL,
  `id_services` int(11) NOT NULL,
  PRIMARY KEY (`id_users`,`id_services`),
  KEY `id_services` (`id_services`),
  CONSTRAINT `services_providers_services` FOREIGN KEY (`id_services`) REFERENCES `ea_services` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `services_providers_users_provider` FOREIGN KEY (`id_users`) REFERENCES `ea_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ea_services_providers`
--

LOCK TABLES `ea_services_providers` WRITE;
/*!40000 ALTER TABLE `ea_services_providers` DISABLE KEYS */;
INSERT INTO `ea_services_providers` VALUES (2,1),(2,2),(2,3),(7,4),(11,8),(11,9),(12,7),(13,5),(14,6);
/*!40000 ALTER TABLE `ea_services_providers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ea_settings`
--

DROP TABLE IF EXISTS `ea_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ea_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(512) DEFAULT NULL,
  `value` longtext DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ea_settings`
--

LOCK TABLES `ea_settings` WRITE;
/*!40000 ALTER TABLE `ea_settings` DISABLE KEYS */;
INSERT INTO `ea_settings` VALUES (1,'company_working_plan','{\"sunday\":null,\"monday\":{\"start\":\"08:00\",\"end\":\"18:00\",\"breaks\":[]},\"tuesday\":{\"start\":\"08:00\",\"end\":\"18:00\",\"breaks\":[]},\"wednesday\":{\"start\":\"08:00\",\"end\":\"18:00\",\"breaks\":[]},\"thursday\":{\"start\":\"08:00\",\"end\":\"18:00\",\"breaks\":[]},\"friday\":{\"start\":\"08:00\",\"end\":\"18:00\",\"breaks\":[]},\"saturday\":null}'),(2,'service_working_plan','{\"sunday\":null,\"monday\":{\"start\":\"09:00\",\"end\":\"18:00\",\"breaks\":[{\"start\":\"14:30\",\"end\":\"15:00\"}]},\"tuesday\":{\"start\":\"09:00\",\"end\":\"18:00\",\"breaks\":[{\"start\":\"14:30\",\"end\":\"15:00\"}]},\"wednesday\":{\"start\":\"09:00\",\"end\":\"18:00\",\"breaks\":[{\"start\":\"14:30\",\"end\":\"15:00\"}]},\"thursday\":{\"start\":\"09:00\",\"end\":\"18:00\",\"breaks\":[{\"start\":\"14:30\",\"end\":\"15:00\"}]},\"friday\":{\"start\":\"09:00\",\"end\":\"18:00\",\"breaks\":[{\"start\":\"14:30\",\"end\":\"15:00\"}]},\"saturday\":null}'),(3,'book_advance_timeout','30'),(4,'google_analytics_code',''),(5,'customer_notifications','1'),(6,'date_format','DMY'),(7,'time_format','military'),(8,'require_captcha','0'),(9,'display_cookie_notice','0'),(10,'cookie_notice_content','Cookie notice content.'),(11,'display_terms_and_conditions','0'),(12,'terms_and_conditions_content','Terms and conditions content.'),(13,'display_privacy_policy','0'),(14,'privacy_policy_content','Privacy policy content.'),(15,'company_name','Mamotest'),(16,'company_email','info@mamotest.com.ar'),(17,'company_link','www.mamotest.net');
/*!40000 ALTER TABLE `ea_settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ea_user_settings`
--

DROP TABLE IF EXISTS `ea_user_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ea_user_settings` (
  `id_users` int(11) NOT NULL,
  `username` varchar(256) DEFAULT NULL,
  `password` varchar(512) DEFAULT NULL,
  `salt` varchar(512) DEFAULT NULL,
  `working_plan` text DEFAULT NULL,
  `notifications` tinyint(4) DEFAULT 0,
  `google_sync` tinyint(4) DEFAULT 0,
  `google_token` text DEFAULT NULL,
  `google_calendar` varchar(128) DEFAULT NULL,
  `sync_past_days` int(11) DEFAULT 5,
  `sync_future_days` int(11) DEFAULT 5,
  `calendar_view` varchar(32) DEFAULT 'default',
  PRIMARY KEY (`id_users`),
  CONSTRAINT `user_settings_users` FOREIGN KEY (`id_users`) REFERENCES `ea_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ea_user_settings`
--

LOCK TABLES `ea_user_settings` WRITE;
/*!40000 ALTER TABLE `ea_user_settings` DISABLE KEYS */;
INSERT INTO `ea_user_settings` VALUES (1,'demo','742e7fba831f3cd1e6635c81ebc2bcda8528a21d65ea3a1b585b6a2fab25e78c','688aedf7b0ea128f895d7f0ec2c6d33d62d1351d449ac85c10e74f45c7a024b0',NULL,0,0,NULL,NULL,5,5,'default'),(2,'mamoconstitucion@gmail.com','36d9914c66b04f1b873e4079787ae365ac79b52f230cae2631ff4925e86a5a3f','12ca8bcf3e7782d53e82b1865054cc322f4d1470324f6a73e29d351dd85c9b85','{\"sunday\":null,\"monday\":{\"start\":\"08:00\",\"end\":\"18:00\",\"breaks\":[]},\"tuesday\":{\"start\":\"08:00\",\"end\":\"18:00\",\"breaks\":[]},\"wednesday\":{\"start\":\"08:00\",\"end\":\"18:00\",\"breaks\":[]},\"thursday\":{\"start\":\"08:00\",\"end\":\"18:00\",\"breaks\":[]},\"friday\":{\"start\":\"08:00\",\"end\":\"18:00\",\"breaks\":[]},\"saturday\":{\"start\":\"10:00\",\"end\":\"16:00\",\"breaks\":[]}}',1,0,NULL,NULL,5,5,'default'),(4,'sec.constitucion','b28b5a7bfc88319c58c4afd58aab343e5e12211ed7e23e12676f73756a684d08','d22ab3c9ce1d97c90f55f35b0ca5df739aa5453c5563c5641ed548df6c600af3',NULL,0,0,NULL,NULL,5,5,'default'),(7,'mamotestcuruzu@gmail.com','499f97d24c5f0f3f6f56027f040fd85b2ad3a7bc1e0db3273739c0a7dc5016e3','b9e7452c0ef06fb6b19dbaabfc5137cfdab5b0acaab51ed0477a2cd737e89e38','{\"sunday\":null,\"monday\":{\"start\":\"08:00\",\"end\":\"18:00\",\"breaks\":[]},\"tuesday\":{\"start\":\"08:00\",\"end\":\"18:00\",\"breaks\":[]},\"wednesday\":{\"start\":\"08:00\",\"end\":\"18:00\",\"breaks\":[]},\"thursday\":{\"start\":\"08:00\",\"end\":\"18:00\",\"breaks\":[]},\"friday\":{\"start\":\"08:00\",\"end\":\"18:00\",\"breaks\":[]},\"saturday\":null}',1,0,NULL,NULL,5,5,'default'),(10,'camila@mamotest.net','29adf2dec7af18a1574baefc7492f3ad4ddeeaf85bcfb84196fd999de393aa36','a78b6905a828b592185018be361df8afe3880dbd31fdca28129c66e4e1fc9048',NULL,1,0,NULL,NULL,5,5,'default'),(11,'mamofichas@gmail.com','3d06f03822def8fbfab915fd23d783c5feb6c64fc6be66bedf7c5cd6c82cf2c0','fc1bee7e44ffbd7aaf5014cc307d6315d3bf7d4d169b226bb2c715cbd69136b0','{\"sunday\":null,\"monday\":{\"start\":\"08:00\",\"end\":\"18:00\",\"breaks\":[]},\"tuesday\":{\"start\":\"08:00\",\"end\":\"18:00\",\"breaks\":[]},\"wednesday\":{\"start\":\"08:00\",\"end\":\"18:00\",\"breaks\":[]},\"thursday\":{\"start\":\"08:00\",\"end\":\"18:00\",\"breaks\":[]},\"friday\":{\"start\":\"08:00\",\"end\":\"18:00\",\"breaks\":[]},\"saturday\":null}',1,0,NULL,NULL,5,5,'default'),(12,'libres@mamotest.com.ar','682593f93b42c4b3c316c60247db59d26c9ca4829c15cdece66848f1cc409f50','9233eaa0b36c3ee2c2828d6cbdf6e86b12314e826d2ad2f58f217342bfadaa85','{\"sunday\":null,\"monday\":{\"start\":\"08:00\",\"end\":\"18:00\",\"breaks\":[]},\"tuesday\":{\"start\":\"08:00\",\"end\":\"18:00\",\"breaks\":[]},\"wednesday\":{\"start\":\"08:00\",\"end\":\"18:00\",\"breaks\":[]},\"thursday\":{\"start\":\"08:00\",\"end\":\"18:00\",\"breaks\":[]},\"friday\":{\"start\":\"08:00\",\"end\":\"18:00\",\"breaks\":[]},\"saturday\":null}',1,0,NULL,NULL,5,5,'default'),(13,'esquina@mamotest.com.ar','b7630bc630c9ba96d81788f9e26d82e3cec96cc5d9fc5ae83f0ec295e59bfb14','71d8dbb3605b5056734a3220be107b4609792b34a6643b9be5cf92f6698bdc73','{\"sunday\":null,\"monday\":{\"start\":\"08:00\",\"end\":\"18:00\",\"breaks\":[]},\"tuesday\":{\"start\":\"08:00\",\"end\":\"18:00\",\"breaks\":[]},\"wednesday\":{\"start\":\"08:00\",\"end\":\"18:00\",\"breaks\":[]},\"thursday\":{\"start\":\"08:00\",\"end\":\"18:00\",\"breaks\":[]},\"friday\":{\"start\":\"08:00\",\"end\":\"18:00\",\"breaks\":[]},\"saturday\":null}',1,0,NULL,NULL,5,5,'default'),(14,'saenzpena@mamotest.com.ar','cdd1308fa200a228c854cc38bc68956833f385a31b4b0e02bb1fba09499f1c04','e44e85cedd2ca46e99ee65cc103260cec706f2a8d2fbe54a47b503161d293b30','{\"sunday\":null,\"monday\":{\"start\":\"08:00\",\"end\":\"18:00\",\"breaks\":[]},\"tuesday\":{\"start\":\"08:00\",\"end\":\"18:00\",\"breaks\":[]},\"wednesday\":{\"start\":\"08:00\",\"end\":\"18:00\",\"breaks\":[]},\"thursday\":{\"start\":\"08:00\",\"end\":\"18:00\",\"breaks\":[]},\"friday\":{\"start\":\"08:00\",\"end\":\"18:00\",\"breaks\":[]},\"saturday\":null}',1,0,NULL,NULL,5,5,'default'),(15,'cdepamphilis@gmail.com','f57c1e19fc281a73d0a58f87508c9196cac0706af0152b8762283a06f1e5d352','d94033c28282dc4fcd190c241840bfc6feeae185c2ff67bc558c74524cc5d038',NULL,1,0,NULL,NULL,5,5,'default'),(21,'mamotest.atencion@gmail.com','3b21d90d18c8ce139bc56bb8d9b1f9d3381cd9fb73d04bc8e7e28da1df516d23','ddd0af0ed3903235ff7502fdf4279bb8de79f48735b88d3c882410fa75016f75',NULL,1,0,NULL,NULL,5,5,'default');
/*!40000 ALTER TABLE `ea_user_settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ea_users`
--

DROP TABLE IF EXISTS `ea_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ea_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(256) DEFAULT NULL,
  `last_name` varchar(512) DEFAULT NULL,
  `email` varchar(512) DEFAULT NULL,
  `mobile_number` varchar(128) DEFAULT NULL,
  `phone_number` varchar(128) DEFAULT NULL,
  `address` varchar(256) DEFAULT NULL,
  `city` varchar(256) DEFAULT NULL,
  `state` varchar(128) DEFAULT NULL,
  `zip_code` varchar(64) DEFAULT NULL,
  `document_number` varchar(11) DEFAULT NULL,
  `notes` text DEFAULT NULL,
  `id_roles` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_roles` (`id_roles`),
  CONSTRAINT `users_roles` FOREIGN KEY (`id_roles`) REFERENCES `ea_roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=159 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ea_users`
--

LOCK TABLES `ea_users` WRITE;
/*!40000 ALTER TABLE `ea_users` DISABLE KEYS */;
INSERT INTO `ea_users` VALUES (1,'Mariano','Tellaeche','marianotellaeche@gmail.com',NULL,'1123413014',NULL,NULL,NULL,NULL,NULL,NULL,1),(2,'Mamotest','Constitucion','constitucion@mamotest.net',NULL,'53685400','Av. Brasil 1128 (Subsuelo)','Capital Federal, Buenos Aires',NULL,NULL,NULL,NULL,2),(4,'Secretaria','Constitucion','ramirocorletti@gmail.com','','01167162133','','CABA','Ciudad Autónoma de Buenos Aires','1426',NULL,'',4),(7,'Mamotest','Curuzú Cuatiá','curuzu@mamotest.net',NULL,'+54 9 379 466 6696','Juan de Vera 765','Curuzú Cuatiá, Corrientes',NULL,NULL,NULL,NULL,2),(10,'Secretaria','Curuzu Cuatia','camila@mamotest.net','1151108890','1151108890','xxxx','','','',NULL,'',4),(11,'Mamotest ','Corrientes TC2000','corrientes@mamotest.com.ar',NULL,'+54 9 379 466 6696','Catamarca 744','Corrientes',NULL,NULL,NULL,NULL,2),(12,'Mamotest','Paso de los Libres','libres@mamotest.com.ar',NULL,'+54 9 379 466 6696','Colón 1488','Paso de los Libres',NULL,NULL,NULL,NULL,2),(13,'Mamotest','Esquina','esquina@mamotest.com.ar',NULL,'+54 9 379 466 6696','San Martin 330','Esquina',NULL,NULL,NULL,NULL,2),(14,'Mamotest','Saenz Peña','saenzpena@mamotest.com.ar',NULL,'+54 9 379 466 6696','Laprida 472 (Calle 15 entre 12 y 10)','Saenz Peña',NULL,NULL,NULL,NULL,2),(15,'Camila','de Pamphilis','cdepamphilis@gmail.com','','1151108890','','','','',NULL,'',1),(21,'Contact Center','Constitución','mamotest.atencion@gmail.com','','1156449323','','','','',NULL,'',4),(22,'María Cecilia','Navarro acevedo','navarrocecilia2305@hotmail.com',NULL,'1133075295',NULL,NULL,NULL,NULL,'29500829',NULL,3),(23,'Maria Edith','Casco','mariae-casco@hotmail.com',NULL,'3794202491',NULL,NULL,NULL,NULL,'17432913',NULL,3),(24,'Diana yasmin','Vazquez','yaspiola1@gmail.com',NULL,'1126314175',NULL,NULL,NULL,NULL,'34020491',NULL,3),(25,'Celina','Espinosa','celinaespinosa@hotmail.com',NULL,'3774632656',NULL,NULL,NULL,NULL,'24954473',NULL,3),(26,'Marcia Carolina','Wagner','marciawag_25@hotmail.com',NULL,'1168864886',NULL,NULL,NULL,NULL,'25253053',NULL,3),(27,'Vanesa Verónica ','Viva','vanesavviva@hotmail.com',NULL,'1159912523',NULL,NULL,NULL,NULL,'22935723',NULL,3),(28,'Maria Evangelina ','Soto','evange_linasoto@hotmail.com',NULL,'3735419365',NULL,NULL,NULL,NULL,'25812793',NULL,3),(29,'Olga ','Pandzic','olga_pandzic@outlook.com',NULL,'3735441938',NULL,NULL,NULL,NULL,'25486065',NULL,3),(30,'MARISA','SOTO','b@homatil.com',NULL,'3794261360',NULL,NULL,NULL,NULL,'36885666',NULL,3),(31,'RAMONA','GONZALEZ','osprera.mercedes@hotmail.com',NULL,'0377344169',NULL,NULL,NULL,NULL,'25009581',NULL,3),(32,'Sonia','Chavez','s-o-n-i-a@live.com.ar',NULL,'3794311974',NULL,NULL,NULL,NULL,'23347568',NULL,3),(33,'Mary','Munaylla','mary_chana@yahoo.com.ar',NULL,'1125542315',NULL,NULL,NULL,NULL,'93980279',NULL,3),(34,'Isabel','Romero','isabeelromero335@gmail.com',NULL,'1150182767',NULL,NULL,NULL,NULL,'34502332',NULL,3),(35,'ELSA NANCY','ESQUIVEL','elsanancy1957@gmail.com',NULL,'3644373401',NULL,NULL,NULL,NULL,'13306760',NULL,3),(36,'ERIKA LORNA','MONZON','emonzon@drogueria-avenida.com.ar',NULL,'3794021693',NULL,NULL,NULL,NULL,'28088794',NULL,3),(37,'Adriana raquel','Oporto','raqueloporto@hotmail.com',NULL,'3764514782',NULL,NULL,NULL,NULL,'22496498',NULL,3),(38,'Graciela ','Graisaro','camila07_5@hotmail.com',NULL,'3773514028',NULL,NULL,NULL,NULL,'29556117',NULL,3),(39,'Carmen Rafaela','Corrales','carmencorrales2@hotmail.com',NULL,'3786619765',NULL,NULL,NULL,NULL,'25192053',NULL,3),(40,'Antonella Noemi','Garcia','antonellangarcia@hotmail.com',NULL,'3794874880',NULL,NULL,NULL,NULL,'39193760',NULL,3),(41,'María Pabla ','Vallejos','mariapablav@gmail.com',NULL,'3775497829',NULL,NULL,NULL,NULL,'21715904',NULL,3),(42,'Rosa Emiliana ','Galvez ','galvezrosa001@gmail.com',NULL,'3756510772',NULL,NULL,NULL,NULL,'31787624',NULL,3),(43,'Carmen analia','Roman','micaelaroman63@gmail.com',NULL,'3794708246',NULL,NULL,NULL,NULL,'28099393',NULL,3),(44,'Lorena','Alen','alenlorena@hotmail.com',NULL,'1158342597',NULL,NULL,NULL,NULL,'23431992',NULL,3),(45,'marta cristina','vallejos','martacristinavallejos@gmail.com',NULL,'3794630079',NULL,NULL,NULL,NULL,'23077267',NULL,3),(46,'Beatriz','Falcon roa','angelesfr94@gmail.com',NULL,'3794813531',NULL,NULL,NULL,NULL,'31209096',NULL,3),(47,'Marta','Amalio','marevanamalio@gmail.com',NULL,'1139110720',NULL,NULL,NULL,NULL,'24686435',NULL,3),(48,'Adriana','Rebollo','adrianageo@hotmail.com',NULL,'3794273839',NULL,NULL,NULL,NULL,'29089304',NULL,3),(49,'Alejandra','Cabrera','soyalvatroz@gmail.com',NULL,'3624792174',NULL,NULL,NULL,NULL,'24079144',NULL,3),(50,'Natalicia','Sotelo','natysonic@hotmail.com',NULL,'1166788602',NULL,NULL,NULL,NULL,'31703839',NULL,3),(51,'Gabriela ','Canteros ','directvcorrientes@hotmail.com.ar',NULL,'3794521160',NULL,NULL,NULL,NULL,'24937091',NULL,3),(52,'Emilce Marisel','D\'Aosta','emdaos@gmail.com',NULL,'3735405484',NULL,NULL,NULL,NULL,'30145729',NULL,3),(53,'CLARISA','RODRIGUEZ DEL INO','clarirdp@gmail.com',NULL,'3794283615',NULL,NULL,NULL,NULL,'25876323',NULL,3),(54,'cecilia','fernandez ','cecyy.wuandy@gmail.com',NULL,'0112077930',NULL,NULL,NULL,NULL,'30893377',NULL,3),(55,'alejandra','flores','pablopi_bostero@hotmail.com',NULL,'1169704132',NULL,NULL,NULL,NULL,'20057176',NULL,3),(56,'Karina','Figueroa','karofigueroa95@gmail.com',NULL,'1124906513',NULL,NULL,NULL,NULL,'25051088',NULL,3),(57,'Lidia','Montenegro ','valeriaalecabj@gmail.com',NULL,'3781409374',NULL,NULL,NULL,NULL,'14816973',NULL,3),(58,'Mariela','Leyes','lucrevi22@hotmail.com',NULL,'1131227448',NULL,NULL,NULL,NULL,'27552752',NULL,3),(59,'Gabriela','Aprile','aprilegabriela5@gmail.com',NULL,'3794392240',NULL,NULL,NULL,NULL,'27934456',NULL,3),(60,'Mabel ','Solier','danieliitagarcia94@gmail.com',NULL,'3734448770',NULL,NULL,NULL,NULL,'13463251',NULL,3),(61,'Patricia','Lacoste','patrilaco@hotmail.com',NULL,'3794227065',NULL,NULL,NULL,NULL,'14742026',NULL,3),(62,'Inés','Gonzalez','milenaalvarez_vb@hotmail.com',NULL,'3735418132',NULL,NULL,NULL,NULL,'02425357',NULL,3),(63,'Noelia','Peralta ','noeliaelizabethperalta@hotmail.com',NULL,'1138067629',NULL,NULL,NULL,NULL,'39916050',NULL,3),(64,'Daniela ','Paccor','daniellapaccor@hotmail.com',NULL,'1134667993',NULL,NULL,NULL,NULL,'26733950',NULL,3),(65,'Pura Estela','Salina','estelapurasalina@hotmail.com',NULL,'3794750008',NULL,NULL,NULL,NULL,'21770280',NULL,3),(66,'Araceli','Di Pietro','stachaco@gmail.com',NULL,'3735626085',NULL,NULL,NULL,NULL,'17751750',NULL,3),(67,'MARIA SILVIMA','BROGLIA','totolin2025@hotmail.com',NULL,'3794515966',NULL,NULL,NULL,NULL,'27306984',NULL,3),(68,'Irina Noemi','Sosa','lucasexe40420979@gmail.com',NULL,'3782520214',NULL,NULL,NULL,NULL,'40590587',NULL,3),(69,'Carina','Torres','carinalines@live.com',NULL,'1154500407',NULL,NULL,NULL,NULL,'24626419',NULL,3),(70,'Gisella','Federmann','gisella_federmann@hotmail.com',NULL,'1169788219',NULL,NULL,NULL,NULL,'28153613',NULL,3),(71,'Molina','Mariana','mariana-mmm@hotmail.com.ar',NULL,'3794408316',NULL,NULL,NULL,NULL,'24107140',NULL,3),(72,'Zunilda','Sotelo','sunyctes_73@hotmail.com',NULL,'1165704639',NULL,NULL,NULL,NULL,'32370884',NULL,3),(73,'Maria Lorena','Borgo','lobo335@hotmail.com',NULL,'3775442975',NULL,NULL,NULL,NULL,'26261525',NULL,3),(74,'María Victoria','Rodríguez Fernández','rodriguezmarita30@gmail.com',NULL,'3794395075',NULL,NULL,NULL,NULL,'29241245',NULL,3),(75,'Liliana','Ayala','limaya@hotmail.com',NULL,'3777583111',NULL,NULL,NULL,NULL,'23074553',NULL,3),(76,'Rosana','Gómez','rosanagomez748@gmail.com',NULL,'3734445382',NULL,NULL,NULL,NULL,'25854438',NULL,3),(77,'Karina ','Parras','mariel822019@outlook.es',NULL,'3756483390',NULL,NULL,NULL,NULL,'29689397',NULL,3),(78,'Karina ','Centurión ','kcenturion468@gmail.com',NULL,'1144042842',NULL,NULL,NULL,NULL,'26595633',NULL,3),(79,'Amalia Beatriz','Gallardo','beatriz_gallardo@ive.com.ar',NULL,'3775432624',NULL,NULL,NULL,NULL,'12235443',NULL,3),(80,'María Cristina ','Acevedo Urturi ','crisacevedo78@hotmail.com',NULL,'3794606123',NULL,NULL,NULL,NULL,'20676183',NULL,3),(81,' CLAUDIA','CLAUDIA','creativfull_ctes@hotmail.com',NULL,'0377347041',NULL,NULL,NULL,NULL,'27866207',NULL,3),(82,'Valeria ','Suarez','Agustina.dominguez@hotmail.com',NULL,'1139158497',NULL,NULL,NULL,NULL,'28631799',NULL,3),(83,'Marina isabel','Diaz','Maridiazcle@2018gmail.com',NULL,'2915113679',NULL,NULL,NULL,NULL,'24134808',NULL,3),(84,'Ariela','Villalba','arielavillalba35@gmail.com',NULL,'3644469423',NULL,NULL,NULL,NULL,'22968691',NULL,3),(85,'Mirta','Lanaro','mirtalanaro@hotmail.com',NULL,'3773417656',NULL,NULL,NULL,NULL,'20274836',NULL,3),(86,'Viviana','Gonzalez','alexandragua333@gmail.com',NULL,'3735419770',NULL,NULL,NULL,NULL,'22320780',NULL,3),(87,'angela','RIQUELME','ANYITRA@HOTMAIL.COM',NULL,'3773406627',NULL,NULL,NULL,NULL,'10485314',NULL,3),(88,'Nelida','Drobec','natalialazarus@hotmail.com',NULL,'3735446336',NULL,NULL,NULL,NULL,'13866164',NULL,3),(89,'PATRICIA LORENA','FERNANDEZ','lflorenapa@hotmail.com',NULL,'3735626371',NULL,NULL,NULL,NULL,'25743923',NULL,3),(90,'Rosana','Luxen','roluxen@hotmail.com',NULL,'3782462948',NULL,NULL,NULL,NULL,'25566377',NULL,3),(91,'Sandra Fabiana','Ayala','sandrafaayala@gmail.com',NULL,'3794917352',NULL,NULL,NULL,NULL,'24046179',NULL,3),(92,'Marta Esther','Binni','martuchi00@outlook.com',NULL,'3775401596',NULL,NULL,NULL,NULL,'14628108',NULL,3),(93,'Maria Cristina','Carruego','crishpjp11@gmail.com',NULL,'3624749530',NULL,NULL,NULL,NULL,'24744173',NULL,3),(94,'Emiliana Damacia','Romero','gimenamartii@gmail.com',NULL,'3794666854',NULL,NULL,NULL,NULL,'10843081',NULL,3),(95,'RAMONA CATALINA ','NUÑEZ','moreyra_miguelangel@yahoo.com.ar',NULL,'3773510263',NULL,NULL,NULL,NULL,'23527188',NULL,3),(96,'angelica ','acuña','angel3279@hotmail.com.ar',NULL,'3772402929',NULL,NULL,NULL,NULL,'26602921',NULL,3),(97,'Rocio','Caceres','rocioaleli32@hotmail.com',NULL,'3704308887',NULL,NULL,NULL,NULL,'35003040',NULL,3),(98,'Gladys adela','Rajoy','gladysrajoy@hotmail.com',NULL,'3777633455',NULL,NULL,NULL,NULL,'10890870',NULL,3),(99,'Laura Beatriz','Benítez','laurabebenitez_@hotmail.com',NULL,'3773418996',NULL,NULL,NULL,NULL,'20628759',NULL,3),(100,'Claudia','Gomez','claualegomez@hotmail.com',NULL,'3773481458',NULL,NULL,NULL,NULL,'29262679',NULL,3),(101,'patricia gabriela','faure brilada','patripat.fau@gmail.com',NULL,'3731621657',NULL,NULL,NULL,NULL,'25210850',NULL,3),(102,'GISELA','BALLATORE','giselaballatore@hotmail.com.ar',NULL,'3644409687',NULL,NULL,NULL,NULL,'29151078',NULL,3),(103,'Maria Julieta','Fernandez Pierlorenzi','julipierlo@gmail.com',NULL,'3794235811',NULL,NULL,NULL,NULL,'39187144',NULL,3),(104,'Vanina','Larrea','vanynalarrea@hotmail.com',NULL,'3774463771',NULL,NULL,NULL,NULL,'35223256',NULL,3),(105,'Mariana ','Zenón ','zenonmariana5@gmail.com',NULL,'3513957246',NULL,NULL,NULL,NULL,'22765690',NULL,3),(106,'Carmen del valle','Bedoya ','Pelu_05_999@hotmail.com',NULL,'3794358703',NULL,NULL,NULL,NULL,'22704864',NULL,3),(107,'Anabrita','Bonomo','any03@hotmail.com.ar',NULL,'3773432347',NULL,NULL,NULL,NULL,'25193088',NULL,3),(108,'Alejandra','Lugo','alelugo_@hotmail.com',NULL,'3794827426',NULL,NULL,NULL,NULL,'28302346',NULL,3),(109,'Eliana','Puleo','puleoeliana@gmail.com',NULL,'1131375092',NULL,NULL,NULL,NULL,'39100994',NULL,3),(110,'Martha Julia','Siquerena','martha_siquerena@hotmail.com',NULL,'3644324757',NULL,NULL,NULL,NULL,'21490175',NULL,3),(111,'Juana del Carmen','Scarella','sabrina393.sgs@gmail.com',NULL,'3794315143',NULL,NULL,NULL,NULL,'12119961',NULL,3),(112,'Rafaela','Altamirano','altamirafaela@gmail.com',NULL,'3773467654',NULL,NULL,NULL,NULL,'17711623',NULL,3),(113,'Silvia','Cantero','silvicanteros@hotmail.com',NULL,'3794826159',NULL,NULL,NULL,NULL,'26560497',NULL,3),(114,'Rosario','Delgado','rosariodelgado1107@gmai.com',NULL,'3794676727',NULL,NULL,NULL,NULL,'37430901',NULL,3),(115,'Ernestina','Chaparro','noehk_23@hotmail.com',NULL,'3735485489',NULL,NULL,NULL,NULL,'4849068',NULL,3),(116,'Patricia ','Silva','patricialiliansilva@outlook.com',NULL,'3644406105',NULL,NULL,NULL,NULL,'24259159',NULL,3),(117,'Susana','Miranda','beljimmiranda@hotmail.com',NULL,'3644523455',NULL,NULL,NULL,NULL,'16817127',NULL,3),(118,'REBECA NOEMI ','LEIVA','maxiperrello@gmail.com',NULL,'1124633915',NULL,NULL,NULL,NULL,'33786645',NULL,3),(119,'Roxana','Gomez','roxana-go@hotmail.com',NULL,'3782527447',NULL,NULL,NULL,NULL,'33451201',NULL,3),(120,'María Rocio','Grillo','grillorocio83@gmail.com',NULL,'3794399455',NULL,NULL,NULL,NULL,'30359616',NULL,3),(121,'Elbia Odila ','Pedrozo','gaby_rom_26@outlook.com',NULL,'1133024362',NULL,NULL,NULL,NULL,'14419935',NULL,3),(122,'Ramona ','Azcona','antonellajarzal91@hotmail.com',NULL,'3775412813',NULL,NULL,NULL,NULL,'18297651',NULL,3),(123,'Fanny Marina ','Tello','fannymarinatello@hotmail.com',NULL,'3644577530',NULL,NULL,NULL,NULL,'32552863',NULL,3),(124,'Mariana ','Zenón ','zenon1072mariana@gmail.com',NULL,'3513957246',NULL,NULL,NULL,NULL,'22765690',NULL,3),(125,'Fabiana','Barrios ','fabianafabiolabarrios@hotmail.com',NULL,'3777615264',NULL,NULL,NULL,NULL,'27304936',NULL,3),(126,'Mirna Elisabeth ','Tirado ','tiradomirna@yahoo.com.ar',NULL,'3734401717',NULL,NULL,NULL,NULL,'24005752',NULL,3),(127,'CYNTHIA GIOVANNA','ERNST','cernst@tmcsa.com.ar',NULL,'3794214264',NULL,NULL,NULL,NULL,'33613137',NULL,3),(128,'Analía','Suárez','vero10349@hotmail.com',NULL,'3716400259',NULL,NULL,NULL,NULL,'27324015',NULL,3),(129,'Fernanda Graciela ','Romero ','fernanda-2009@live.com.ar',NULL,'3794392117',NULL,NULL,NULL,NULL,'29672865',NULL,3),(130,'Cristina','Hryncyszyn','hrynchyszyn@gmail.com',NULL,'3735472909',NULL,NULL,NULL,NULL,'20387664',NULL,3),(131,'Maria del Socorro','Alegre','arq_alegre@yahoo.com.ar',NULL,'3794407043',NULL,NULL,NULL,NULL,'26111010',NULL,3),(132,'Sandra ','Cuenca','sandra_cuenca09@hotmail.com',NULL,'3794347577',NULL,NULL,NULL,NULL,'18578507',NULL,3),(133,'María Victoria','Sosa','mariavictoria1979@hotmail.com.ar',NULL,'3794659001',NULL,NULL,NULL,NULL,'27347153',NULL,3),(134,'Teresa','Zárate','terelolo4@hotmail.com',NULL,'3644326416',NULL,NULL,NULL,NULL,'24414415',NULL,3),(135,'Claudia Jesús','Leiva','eduardo_bu13@gmail.com',NULL,'3735612925',NULL,NULL,NULL,NULL,'21540459',NULL,3),(136,'Claudia veronica','Barreto','barret-claudi@hotmail.com',NULL,'3735470195',NULL,NULL,NULL,NULL,'22857842',NULL,3),(137,'MARIA CLAUDIA','FARIZANO','claudiafarizano@gmail.com',NULL,'3794862325',NULL,NULL,NULL,NULL,'21827200',NULL,3),(138,'Verónica Alejandra','Medina','fridamedina32@gmail.com',NULL,'3794899804',NULL,NULL,NULL,NULL,'23076276',NULL,3),(139,'susana del carmn','gomez','susana45_gomez@hotmail.com',NULL,'3794442738',NULL,NULL,NULL,NULL,'20266173',NULL,3),(140,'Maria luisa','Retamozo','loisa_alvear@hotmail.com',NULL,'3772431608',NULL,NULL,NULL,NULL,'30094689',NULL,3),(141,'Mercedes Noemi','Collinet','mercedesnoemicollinet.mnc@gmail.com',NULL,'3774435739',NULL,NULL,NULL,NULL,'27165372',NULL,3),(142,'Paola ivana','Martínez ','majosmiethsosa@gmail.com',NULL,'3794728452',NULL,NULL,NULL,NULL,'26645388',NULL,3),(143,'Roxana Itati','Obregón ','roxana.itati.obregon@gmail.com',NULL,'3794690987',NULL,NULL,NULL,NULL,'28666039',NULL,3),(144,'Graciela ','maidana ','gracielamaidana099@gmail.com',NULL,'3772526177',NULL,NULL,NULL,NULL,'27081478',NULL,3),(145,'Mabel Norma','Pereyra','meybel_08@hotmail.com.ar',NULL,'3794729215',NULL,NULL,NULL,NULL,'34207351',NULL,3),(146,'Mariana','Solan','gabrielg@hotmail.com',NULL,'3772445916',NULL,NULL,NULL,NULL,'29990754',NULL,3),(147,'Gladis Isabel','Sandoval','gla13fer@hotmail.com',NULL,'3777624498',NULL,NULL,NULL,NULL,'24198582',NULL,3),(148,'Soledad ','Segovia ','solepereirao726@gmail.com',NULL,'3794008449',NULL,NULL,NULL,NULL,'31726077',NULL,3),(149,'María luisa','Soler','marialuisasoler@gmail.com',NULL,'3794606426',NULL,NULL,NULL,NULL,'28303192',NULL,3),(150,'María Rosana','Duarte','rosiduarte15@hotmail.com',NULL,'3782441506',NULL,NULL,NULL,NULL,'21463907',NULL,3),(151,'Francia elena','Otalvaro arredondo','franciotalvaro791@gmail.com',NULL,'3794291798',NULL,NULL,NULL,NULL,'Au905110',NULL,3),(152,'Verónica','Ocampo','veronicaocampo2012@gmail.com',NULL,'3775403275',NULL,NULL,NULL,NULL,'25264188',NULL,3),(153,'Teresa','Ortíz','teor.64@hotmail.com.ar',NULL,'3777371327',NULL,NULL,NULL,NULL,'17343608',NULL,3),(154,'Cecilia Beatriz','Dominguez','cecibeatrizdominguez@hotmail.com',NULL,'3795058671',NULL,NULL,NULL,NULL,'24937265',NULL,3),(155,'Jhanet','Ramos','stef7@hot.comail.es',NULL,'1122891465',NULL,NULL,NULL,NULL,'95430206',NULL,3),(156,'Isolina','Fontes','isolinafontes@gmail.com',NULL,'3772432684',NULL,NULL,NULL,NULL,'37329271',NULL,3),(157,'carolina ','Lickiewicz','carolick@hotmail.com',NULL,'3644648680',NULL,NULL,NULL,NULL,'31396446',NULL,3),(158,'María Paula','Aguirre','pau760590@gmail.com',NULL,'3773458518',NULL,NULL,NULL,NULL,'26760590',NULL,3);
/*!40000 ALTER TABLE `ea_users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-08-16 21:36:23
