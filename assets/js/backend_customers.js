/* ----------------------------------------------------------------------------
 * Easy!Appointments - Open Source Web Scheduler
 *
 * @package     EasyAppointments
 * @author      A.Tselegidis <alextselegidis@gmail.com>
 * @copyright   Copyright (c) 2013 - 2018, Alex Tselegidis
 * @license     http://opensource.org/licenses/GPL-3.0 - GPLv3
 * @link        http://easyappointments.org
 * @since       v1.0.0
 * ---------------------------------------------------------------------------- */

window.BackendCustomers = window.BackendCustomers || {};

/**
 * Backend Customers
 *
 * Backend Customers javascript namespace. Contains the main functionality of the backend customers
 * page. If you need to use this namespace in a different page, do not bind the default event handlers
 * during initialization.
 *
 * @module BackendCustomers
 */
(function (exports) {

    'use strict';

    /**
     * The page helper contains methods that implement each record type functionality
     * (for now there is only the CustomersHelper).
     *
     * @type {Object}
     */
    var helper = {};

    var customersHelper = new CustomersHelper();
    var prestadorasHelper = new PrestadorasHelper();

    /**
     * This method initializes the backend customers page. If you use this namespace
     * in a different page do not use this method.
     *
     * @param {Boolean} defaultEventHandlers Optional (false), whether to bind the default
     * event handlers or not.
     */
    exports.initialize = function (defaultEventHandlers) {
        defaultEventHandlers = defaultEventHandlers || false;

        helper = customersHelper;
        helper.resetForm();
        helper.filter('');

        if (defaultEventHandlers) {
            _bindEventHandlers();
        }
    };

    /**
     * Binds the default event handlers of the backend customers page.
     *
     * Do not use this method if you include the "BackendCustomers" namespace on another page.
     */
    function _bindEventHandlers() {
        /**
         * Event: Page Tab Button "Click"
         *
         * Changes the displayed tab.
         */
        $('a[data-toggle="tab"]').on('shown.bs.tab', function () {
            if ($(this).attr('href') === '#customers') {
                helper = customersHelper;
            } else if ($(this).attr('href') === '#prestadoras') {
                helper = prestadorasHelper;
            }

            helper.resetForm();
            helper.filter('');
            $('.filter-key').val('');
            Backend.placeFooterToBottom();
        });

        customersHelper.bindEventHandlers();
        prestadorasHelper.bindEventHandlers();
    }

    /**
     * Update the service prestadora listbox.
     *
     * Use this method every time a change is made to the service prestadoras db table.
     */
    exports.updateAvailablePrestadoras = function () {
        var url = GlobalVariables.baseUrl + '/index.php/backend_api/ajax_filter_user_prestadoras';
        var data = {
            csrfToken: GlobalVariables.csrfToken,
            key: ''
        };

        $.post(url, data, function (response) {
            if (!GeneralFunctions.handleAjaxExceptions(response)) {
                return;
            }

            GlobalVariables.prestadoras = response;
            var $select = $('#service-prestadora');
            $select.empty();
            $.each(response, function (index, prestadora) {
                var option = new Option(prestadora.name, prestadora.id);
                $select.append(option);
            });
            $select.append(new Option('- ' + EALang.no_prestadora + ' -', null)).val('null');
        }, 'json').fail(GeneralFunctions.ajaxFailureHandler);
    };
})(window.BackendCustomers);
