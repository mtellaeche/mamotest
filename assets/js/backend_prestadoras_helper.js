/* ----------------------------------------------------------------------------
 * Easy!Appointments - Open Source Web Scheduler
 *
 * @package     EasyAppointments
 * @author      A.Tselegidis <alextselegidis@gmail.com>
 * @copyright   Copyright (c) 2013 - 2018, Alex Tselegidis
 * @license     http://opensource.org/licenses/GPL-3.0 - GPLv3
 * @link        http://easyappointments.org
 * @since       v1.0.0
 * ---------------------------------------------------------------------------- */

(function () {

    'use strict';

    /**
     * PrestadorasHelper Class
     *
     * This class contains the core method implementations that belong to the prestadoras tab
     * of the backend services page.
     *
     * @class PrestadorasHelper
     */
    function PrestadorasHelper() {
        this.filterResults = {};
    }

    /**
     * Binds the default event handlers of the prestadoras tab.
     */
    PrestadorasHelper.prototype.bindEventHandlers = function () {
        var instance = this;

        /**
         * Event: Filter Prestadoras Cancel Button "Click"
         */
        $('#filter-prestadoras .clear').click(function () {
            $('#filter-prestadoras .key').val('');
            instance.filter('');
            instance.resetForm();
        });

        /**
         * Event: Filter Prestadoras Form "Submit"
         */
        $('#filter-prestadoras form').submit(function () {
            var key = $('#filter-prestadoras .key').val();
            $('.selected').removeClass('selected');
            instance.resetForm();
            instance.filter(key);
            return false;
        });

        /**
         * Event: Filter Prestadoras Row "Click"
         *
         * Displays the selected row data on the right side of the page.
         */
        $(document).on('click', '.prestadora-row', function () {
            if ($('#filter-prestadoras .filter').prop('disabled')) {
                $('#filter-prestadoras .results').css('color', '#AAA');
                return; // exit because we are on edit mode
            }

            var prestadoraId = $(this).attr('data-id');
            var prestadora = {};
            $.each(instance.filterResults, function (index, item) {
                if (item.id === prestadoraId) {
                    prestadora = item;
                    return false;
                }
            });

            instance.display(prestadora);
            $('#filter-prestadoras .selected').removeClass('selected');
            $(this).addClass('selected');
            $('#edit-prestadora, #delete-prestadora').prop('disabled', false);
        });

        /**
         * Event: Add Prestadora Button "Click"
         */
        $('#add-prestadora').click(function () {
            instance.resetForm();
            $('#prestadoras .add-edit-delete-group').hide();
            $('#prestadoras .save-cancel-group').show();
            $('#prestadoras .record-details').find('input, textarea').prop('readonly', false);
            $('#prestadoras .record-details').find('select, input[type=checkbox]').attr('disabled', false);
            $('#filter-prestadoras button').prop('disabled', true);
            $('#filter-prestadoras .results').css('color', '#AAA');
        });

        /**
         * Event: Edit Prestadora Button "Click"
         */
        $('#edit-prestadora').click(function () {
            $('#prestadoras .add-edit-delete-group').hide();
            $('#prestadoras .save-cancel-group').show();
            $('#prestadoras .record-details').find('input, textarea').prop('readonly', false);
            $('#prestadoras .record-details').find('select, input[type=checkbox]').attr('disabled', false);

            $('#filter-prestadoras button').prop('disabled', true);
            $('#filter-prestadoras .results').css('color', '#AAA');
        });

        /**
         * Event: Delete Prestadora Button "Click"
         */
        $('#delete-prestadora').click(function () {
            var prestadoraId = $('#prestadora-id').val();

            var buttons = [
                {
                    text: EALang.delete,
                    click: function () {
                        instance.delete(prestadoraId);
                        $('#message_box').dialog('close');
                    }
                },
                {
                    text: EALang.cancel,
                    click: function () {
                        $('#message_box').dialog('close');
                    }
                }
            ];

            GeneralFunctions.displayMessageBox(EALang.delete_prestadora,
                EALang.delete_record_prompt, buttons);
        });

        /**
         * Event: Prestadoras Save Button "Click"
         */
        $('#save-prestadora').click(function () {
            var prestadora = {
                name: $('#prestadora-name').val(),
                description: $('#prestadora-description').val(),
                active: $('#prestadora-active').is(':checked')
            };

            if ($('#prestadora-id').val() !== '') {
                prestadora.id = $('#prestadora-id').val();
            }

            if (!instance.validate()) {
                return;
            }

            instance.save(prestadora);
        });

        /**
         * Event: Cancel Prestadora Button "Click"
         */
        $('#cancel-prestadora').click(function () {
            var id = $('#prestadora-id').val();
            instance.resetForm();
            if (id !== '') {
                instance.select(id, true);
            }
        });
    };

    /**
     * Filter service prestadoras records.
     *
     * @param {String} key This key string is used to filter the prestadora records.
     * @param {Number} selectId Optional, if set then after the filter operation the record with the given
     * ID will be selected (but not displayed).
     * @param {Boolean} display Optional (false), if true then the selected record will be displayed on the form.
     */
    PrestadorasHelper.prototype.filter = function (key, selectId, display) {
        var postUrl = GlobalVariables.baseUrl + '/index.php/backend_api/ajax_filter_user_prestadoras';
        var postData = {
            csrfToken: GlobalVariables.csrfToken,
            key: key
        };

        $.post(postUrl, postData, function (response) {
            if (!GeneralFunctions.handleAjaxExceptions(response)) {
                return;
            }

            this.filterResults = response;

            $('#filter-prestadoras .results').html('');
            $.each(response, function (index, prestadora) {
                var html = this.getFilterHtml(prestadora);
                $('#filter-prestadoras .results').append(html);
            }.bind(this));

            if (response.length === 0) {
                $('#filter-prestadoras .results').html('<em>' + EALang.no_records_found + '</em>');
            }

            if (selectId !== undefined) {
                this.select(selectId, display);
            }
        }.bind(this), 'json').fail(GeneralFunctions.ajaxFailureHandler);
    };

    /**
     * Save a prestadora record to the database (via AJAX post).
     *
     * @param {Object} prestadora Contains the prestadora data.
     */
    PrestadorasHelper.prototype.save = function (prestadora) {
        var postUrl = GlobalVariables.baseUrl + '/index.php/backend_api/ajax_save_user_prestadora';
        var postData = {
            csrfToken: GlobalVariables.csrfToken,
            prestadora: JSON.stringify(prestadora)
        };

        $.post(postUrl, postData, function (response) {
            if (!GeneralFunctions.handleAjaxExceptions(response)) {
                return;
            }

            Backend.displayNotification(EALang.user_prestadora_saved);
            this.resetForm();
            $('#filter-prestadoras .key').val('');
            this.filter('', response.id, true);
            BackendCustomers.updateAvailablePrestadoras();
        }.bind(this), 'json').fail(GeneralFunctions.ajaxFailureHandler);
    };

    /**
     * Delete prestadora record.
     *
     * @param Number} id Record ID to be deleted.
     */
    PrestadorasHelper.prototype.delete = function (id) {
        var postUrl = GlobalVariables.baseUrl + '/index.php/backend_api/ajax_delete_user_prestadora';
        var postData = {
            csrfToken: GlobalVariables.csrfToken,
            prestadora_id: id
        };

        $.post(postUrl, postData, function (response) {
            if (!GeneralFunctions.handleAjaxExceptions(response)) {
                return;
            }

            Backend.displayNotification(EALang.user_prestadora_deleted);

            this.resetForm();
            this.filter($('#filter-prestadoras .key').val());
            BackendCustomers.updateAvailablePrestadoras();
        }.bind(this), 'json').fail(GeneralFunctions.ajaxFailureHandler);
    };

    /**
     * Display a prestadora record on the form.
     *
     * @param {Object} prestadora Contains the prestadora data.
     */
    PrestadorasHelper.prototype.display = function (prestadora) {
        $('#prestadora-id').val(prestadora.id);
        $('#prestadora-name').val(prestadora.name);
        $('#prestadora-description').val(prestadora.description);
        $('#prestadora-active').prop('checked', prestadora.active * 1)
    };

    /**
     * Validate prestadora data before save (insert or update).
     *
     * @return {Boolean} Returns the validation result.
     */
    PrestadorasHelper.prototype.validate = function () {
        $('#prestadoras .has-error').removeClass('has-error');

        try {
            var missingRequired = false;

            $('#prestadoras .required').each(function () {
                if ($(this).val() === '' || $(this).val() === undefined) {
                    $(this).closest('.form-group').addClass('has-error');
                    missingRequired = true;
                }
            });

            if (missingRequired) {
                throw EALang.fields_are_required;
            }

            return true;
        } catch (message) {
            return false;
        }
    };

    /**
     * Bring the prestadora form back to its initial state.
     */
    PrestadorasHelper.prototype.resetForm = function () {
        $('#prestadoras .add-edit-delete-group').show();
        $('#prestadoras .save-cancel-group').hide();
        $('#prestadoras .record-details').find('input, textarea').val('');
        $('#prestadoras .record-details').find('input, textarea').prop('readonly', true);
        $('#prestadoras .record-details').find('select, input[type=checkbox]').attr('disabled', true);
        $('#edit-prestadora, #delete-prestadora').prop('disabled', true);

        $('#filter-prestadoras .selected').removeClass('selected');
        $('#filter-prestadoras .results').css('color', '');
        $('#filter-prestadoras button').prop('disabled', false);
    };

    /**
     * Get the filter results row HTML code.
     *
     * @param {Object} prestadora Contains the prestadora data.
     *
     * @return {String} Returns the record HTML code.
     */
    PrestadorasHelper.prototype.getFilterHtml = function (prestadora) {
        var html =
            '<div class="prestadora-row entry" data-id="' + prestadora.id + '">' +
            '<strong>' + prestadora.name + '</strong>' +
            '</div><hr>';

        return html;
    };

    /**
     * Select a specific record from the current filter results.
     *
     * If the prestadora ID does not exist in the list then no record will be selected.
     *
     * @param {Number} id The record ID to be selected from the filter results.
     * @param {Boolean} display Optional (false), if true then the method will display the record
     * on the form.
     */
    PrestadorasHelper.prototype.select = function (id, display) {
        display = display || false;

        $('#filter-prestadoras .selected').removeClass('selected');

        $('#filter-prestadoras .prestadora-row').each(function () {
            if ($(this).attr('data-id') == id) {
                $(this).addClass('selected');
                return false;
            }
        });

        if (display) {
            $.each(this.filterResults, function (index, prestadora) {
                if (prestadora.id == id) {
                    this.display(prestadora);
                    $('#edit-prestadora, #delete-prestadora').prop('disabled', false);
                    return false;
                }
            }.bind(this));
        }
    };

    window.PrestadorasHelper = PrestadorasHelper;
})();
