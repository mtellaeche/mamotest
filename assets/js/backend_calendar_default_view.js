/* ----------------------------------------------------------------------------
 * Easy!Appointments - Open Source Web Scheduler
 *
 * @package     EasyAppointments
 * @author      A.Tselegidis <alextselegidis@gmail.com>
 * @copyright   Copyright (c) 2013 - 2018, Alex Tselegidis
 * @license     http://opensource.org/licenses/GPL-3.0 - GPLv3
 * @link        http://easyappointments.org
 * @since       v1.2.0
 * ---------------------------------------------------------------------------- */

/**
 * Backend Calendar
 *
 * This module implements the default calendar view of backend.
 *
 * @module BackendCalendarDefaultView
 */
window.BackendCalendarDefaultView = window.BackendCalendarDefaultView || {};

(function (exports) {
    'use strict';

    // Constants
    let FILTER_TYPE_PROVIDER = 'provider';
    let FILTER_TYPE_SERVICE = 'service';

    // Variables
    let lastFocusedEventData; // Contains event data for later use.

    /**
     * Bind event handlers for the calendar view.
     */
    function _bindEventHandlers() {
        let $calendarPage = $('#calendar-page');

        /**
         * Event: Reload Button "Click"
         *
         * When the user clicks the reload button an the calendar items need to be refreshed.
         */
        $('#reload-appointments').click(function () {
            $('#select-filter-item').trigger('change');
        });

        /**
         * Event: Popover Close Button "Click"
         *
         * Hides the open popover element.
         */
        $calendarPage.on('click', '.close-popover', function () {
            $(this).parents().eq(2).remove();
        });

        /**
         * Event: Popover Edit Button "Click"
         *
         * Enables the edit dialog of the selected calendar event.
         */
        $calendarPage.on('click', '.edit-popover', function () {
            $(this).parents().eq(2).remove(); // Hide the popover

            let $dialog;

            if (lastFocusedEventData.data.is_unavailable == false) {
                let appointment = lastFocusedEventData.data;
                $dialog = $('#manage-appointment');

                BackendCalendarAppointmentsModal.resetAppointmentDialog();

                // Apply appointment data and show modal dialog.
                $dialog.find('.modal-header h3').text(EALang.edit_appointment_title);
                $dialog.find('#appointment-id').val(appointment.id);
                $dialog.find('#select-provider').val(appointment.id_users_provider).trigger('change');
                $dialog.find('#select-service').val(appointment.id_services);
                $dialog.find('#appointment-date').val(moment(appointment.start_datetime).format('YYYY-MM-DD'));

                let customer = appointment.customer;

                $dialog.find('#customer-id').val(appointment.id_users_customer);
                $dialog.find('#first-name').val(customer.first_name);
                $dialog.find('#last-name').val(customer.last_name);
                $dialog.find('#observations').val(appointment.observations);
                $dialog.find('#document-number').val(customer.document_number);
                $dialog.find('#email').val(customer.email);
                $dialog.find('#phone-number').val(customer.phone_number);
                $dialog.find('#address').val(customer.address);

                BackendCalendarUnavailabilitiesModal.resetDialogAvailablesHours($dialog, appointment);

            } else {
                let unavailable = lastFocusedEventData.data;

                // Replace string date values with actual date objects.
                unavailable.start_datetime = lastFocusedEventData.start.format('YYYY-MM-DD HH:mm:ss');
                let startDatetime = Date.parseExact(unavailable.start_datetime, 'yyyy-MM-dd HH:mm:ss');
                unavailable.end_datetime = lastFocusedEventData.end.format('YYYY-MM-DD HH:mm:ss');
                let endDatetime = Date.parseExact(unavailable.end_datetime, 'yyyy-MM-dd HH:mm:ss');

                $dialog = $('#manage-unavailable');
                BackendCalendarUnavailabilitiesModal.resetUnavailableDialog();

                // Apply unavailable data to dialog.
                $dialog.find('.modal-header h3').text('Edit Unavailable Period');
                $dialog.find('#unavailable-start').datetimepicker('setDate', startDatetime);
                $dialog.find('#unavailable-id').val(unavailable.id);
                $dialog.find('#unavailable-provider').val(unavailable.id_users_provider);
                $dialog.find('#unavailable-end').datetimepicker('setDate', endDatetime);
                $dialog.find('#unavailable-notes').val(unavailable.notes);
            }

            // :: DISPLAY EDIT DIALOG
            $dialog.modal('show');
        });

        /**
         * Event: Popover Delete Button "Click"
         *
         * Displays a prompt on whether the user wants the appointment to be deleted. If he confirms the
         * deletion then an AJAX call is made to the server and deletes the appointment from the database.
         */
        $calendarPage.on('click', '.delete-popover', function () {
            $(this).parents().eq(2).remove(); // Hide the popover.

            if (lastFocusedEventData.data.is_unavailable == false) {
                let buttons = [
                    {
                        text: 'OK',
                        click: function () {
                            let url = GlobalVariables.baseUrl + '/index.php/backend_api/ajax_delete_appointment';
                            let data = {
                                csrfToken: GlobalVariables.csrfToken,
                                appointment_id: lastFocusedEventData.data.id,
                                delete_reason: $('#delete-reason').val()
                            };

                            $.post(url, data, function (response) {
                                $('#message_box').dialog('close');

                                if (response.exceptions) {
                                    response.exceptions = GeneralFunctions.parseExceptions(response.exceptions);
                                    GeneralFunctions.displayMessageBox(GeneralFunctions.EXCEPTIONS_TITLE,
                                        GeneralFunctions.EXCEPTIONS_MESSAGE);
                                    $('#message_box').append(GeneralFunctions.exceptionsToHtml(response.exceptions));
                                    return;
                                }

                                if (response.warnings) {
                                    response.warnings = GeneralFunctions.parseExceptions(response.warnings);
                                    GeneralFunctions.displayMessageBox(GeneralFunctions.WARNINGS_TITLE,
                                        GeneralFunctions.WARNINGS_MESSAGE);
                                    $('#message_box').append(GeneralFunctions.exceptionsToHtml(response.warnings));
                                }

                                // Refresh calendar event items.
                                $('#select-filter-item').trigger('change');
                            }, 'json').fail(GeneralFunctions.ajaxFailureHandler);
                        }
                    },
                    {
                        text: EALang.cancel,
                        click: function () {
                            $('#message_box').dialog('close');
                        }
                    }
                ];


                GeneralFunctions.displayMessageBox(EALang.delete_appointment_title,
                    EALang.write_appointment_removal_reason, buttons);

                $('#message_box').append('<textarea id="delete-reason" rows="3"></textarea>');
                $('#delete-reason').css('width', '100%');
            } else {
                // Do not display confirmation prompt.
                let url = GlobalVariables.baseUrl + '/index.php/backend_api/ajax_delete_unavailable';
                let data = {
                    csrfToken: GlobalVariables.csrfToken,
                    unavailable_id: lastFocusedEventData.data.id
                };

                $.post(url, data, function (response) {
                    $('#message_box').dialog('close');

                    if (response.exceptions) {
                        response.exceptions = GeneralFunctions.parseExceptions(response.exceptions);
                        GeneralFunctions.displayMessageBox(GeneralFunctions.EXCEPTIONS_TITLE, GeneralFunctions.EXCEPTIONS_MESSAGE);
                        $('#message_box').append(GeneralFunctions.exceptionsToHtml(response.exceptions));
                        return;
                    }

                    if (response.warnings) {
                        response.warnings = GeneralFunctions.parseExceptions(response.warnings);
                        GeneralFunctions.displayMessageBox(GeneralFunctions.WARNINGS_TITLE, GeneralFunctions.WARNINGS_MESSAGE);
                        $('#message_box').append(GeneralFunctions.exceptionsToHtml(response.warnings));
                    }

                    // Refresh calendar event items.
                    $('#select-filter-item').trigger('change');
                }, 'json').fail(GeneralFunctions.ajaxFailureHandler);
            }
        });

        /**
         * Event: Calendar Filter Item "Change"
         *
         * Load the appointments that correspond to the select filter item and display them on the calendar.
         */
        $('#select-filter-item').change(function () {
            // If current value is service, then the sync buttons must be disabled.
            if ($('#select-filter-item option:selected').attr('type') === FILTER_TYPE_SERVICE) {
                $('#google-sync, #enable-sync, #insert-appointment, #insert-unavailable').prop('disabled', false);
                $('#calendar').fullCalendar('option', 'selectable', true);
                $('#calendar').fullCalendar('option', 'editable', true);
            } else {
                $('#google-sync, #enable-sync, #insert-appointment, #insert-unavailable').prop('disabled', false);
                $('#calendar').fullCalendar('option', 'selectable', true);
                $('#calendar').fullCalendar('option', 'editable', true);

                // If the user has already the sync enabled then apply the proper style changes.
                if ($('#select-filter-item option:selected').attr('google-sync') === 'true') {
                    $('#enable-sync').addClass('btn-danger enabled');
                    $('#enable-sync span:eq(1)').text(EALang.disable_sync);
                    $('#google-sync').prop('disabled', false);
                } else {
                    $('#enable-sync').removeClass('btn-danger enabled');
                    $('#enable-sync span:eq(1)').text(EALang.enable_sync);
                    $('#google-sync').prop('disabled', true);
                }
            }

            let $label = $('#select-filter-item :selected').parent().attr('label');

            $('label[for=select-filter-item]').text($label + ': ');
            document.title = $label;

            if (GeneralFunctions.getUrlParameter(location.href, 'select-filter-item') != $(this).val()) {
                window.history.replaceState({}, document.title, "/" + "index.php/backend"); // limpio la url

                if ($.contains(document.location.href, '?')) {
                    document.location = document.location.href+"&select-filter-item="+$(this).val();
                } else {
                    document.location = document.location.href+"?select-filter-item="+$(this).val();
                }
            }

        });
    }

    /**
     * Get Calendar Component Height
     *
     * This method calculates the proper calendar height, in order to be displayed correctly, even when the
     * browser window is resizing.
     *
     * @return {Number} Returns the calendar element height in pixels.
     */
    function _getCalendarHeight() {
        let result = window.innerHeight - $('#footer').outerHeight() - $('#header').outerHeight()
            - $('#calendar-toolbar').outerHeight() - 60; // 60 for fine tuning
        return (result > 500) ? result : 500; // Minimum height is 500px
    }

    /**
     * Calendar Event "Click" Callback
     *
     * When the user clicks on an appointment object on the calendar, then a data preview popover is display
     * above the calendar item.
     */
    function _calendarEventClick(event, jsEvent, view) {
        $('.popover').remove(); // Close all open popovers.

        let html;
        let displayEdit;
        let displayDelete;

        // Depending where the user clicked the event (title or empty space) we
        // need to use different selectors to reach the parent element.
        let $parent = $(jsEvent.target.offsetParent);
        let $altParent = $(jsEvent.target).parents().eq(1);

        if ($(this).hasClass('fc-unavailable') || $parent.hasClass('fc-unavailable') || $altParent.hasClass('fc-unavailable')) {
            displayEdit = (($parent.hasClass('fc-custom') || $altParent.hasClass('fc-custom'))
                && GlobalVariables.user.privileges.appointments.edit == true)
                ? '' : 'hide';
            displayDelete = (($parent.hasClass('fc-custom') || $altParent.hasClass('fc-custom'))
                && GlobalVariables.user.privileges.appointments.delete == true)
                ? '' : 'hide'; // Same value at the time.

            let notes = '';
            if (event.data) { // Only custom unavailable periods have notes.
                notes = '<strong>Notes</strong> ' + event.data.notes;
            }

            html =
                '<style type="text/css">'
                + '.popover-content strong {min-width: 80px; display:inline-block;}'
                + '.popover-content button {margin-right: 10px;}'
                + '</style>' +
                '<strong>' + EALang.start + '</strong> '
                + GeneralFunctions.formatDate(event.start.format('YYYY-MM-DD HH:mm:ss'), GlobalVariables.dateFormat, true)
                + '<br>' +
                '<strong>' + EALang.end + '</strong> '
                + GeneralFunctions.formatDate(event.end.format('YYYY-MM-DD HH:mm:ss'), GlobalVariables.dateFormat, true)
                + '<br>'
                + notes
                + '<hr>' +
                '<center>' +
                '<button class="edit-popover btn btn-primary ' + displayEdit + '">' + EALang.edit + '</button>' +
                '<button class="delete-popover btn btn-danger ' + displayDelete + '">' + EALang.delete + '</button>' +
                '<button class="close-popover btn btn-default" data-po=' + jsEvent.target + '>' + EALang.close + '</button>' +
                '</center>';
        } else {
            displayEdit = (GlobalVariables.user.privileges.appointments.edit == true)
                ? '' : 'hide';
            displayDelete = (GlobalVariables.user.privileges.appointments.delete == true)
                ? '' : 'hide';

            let isAfter = moment().isAfter(event.start.format('YYYY-MM-DD HH:mm:ss'));

            //Muestra el botón de imprimir siempre y cuando el turno no haya pasado
            let hidePrint = isAfter ? 'hidden' : '';

            //Muestra el botón si el turno ya paso y si no fue marcado como no-show
            let hideNoShow = (!isAfter || (event.data.no_show == 1)) ? 'hidden' : '';

            html =
                '<style type="text/css">'
                + '.popover-content strong {min-width: 80px; display:inline-block;}'
                + '.popover-content button {margin-right: 10px;}'
                + '</style>' +
                '<strong>' + EALang.provider + '</strong> '
                + event.data.provider.first_name + ' '
                + event.data.provider.last_name
                + '<br>' +
                '<strong>' + EALang.service + '</strong> '
                + event.data.service.name
                + '<br>' +
                '<strong>' + EALang.start + '</strong> '
                + GeneralFunctions.formatDate(event.start.format('YYYY-MM-DD HH:mm:ss'), GlobalVariables.dateFormat, true)
                + '<br>' +
                '<strong>' + EALang.end + '</strong> '
                + GeneralFunctions.formatDate(event.end.format('YYYY-MM-DD HH:mm:ss'), GlobalVariables.dateFormat, true)
                + '<hr>' +
                '<strong>' + EALang.customer + '</strong> '
                + event.data.customer.first_name + ' '
                + event.data.customer.last_name
                + '<br>' +
                '<strong>' + EALang.short_document_number + '</strong> '
                + event.data.customer.document_number
                + '<br>' +
                '<strong>' + EALang.email + '</strong> '
                + (event.data.customer.email ? event.data.customer.email : 'No tiene email')
                + '<br>' +
                '<strong>' + EALang.phone_number + '</strong> '
                + event.data.customer.phone_number
                + '<br>' +
                '<strong>' + EALang.prestadora + '</strong> '
                + event.data.customer.prestadora
                + '<br>' +
                // Agrego el campo, created_by
                '<strong>' + EALang.created_by + '</strong> '
                + event.data.created_by
                + '<hr>' +
                '<div class="text-center">' +
                '<a href="/index.php/backend/printAppointment/' + event.data.hash + '" class="' + hidePrint + ' btn btn-info" title="Imprimir" target="_blank"><i class="glyphicon glyphicon-print"></i></a>' +
                '<button id="no-show-appointment" data-appointment-id="' + event.id + '" class="' + hideNoShow + ' btn btn-warning ' + displayEdit + '" title="No se presentó"><i class="glyphicon glyphicon-ban-circle"></i></button>' +
                '<button class="edit-popover btn btn-primary ' + displayEdit + '" title="Editar"><i class="glyphicon glyphicon-pencil"></i></button>' +
                '<button class="delete-popover btn btn-danger ' + displayDelete + '" title="Borrar"><i class="glyphicon glyphicon-trash"></i></button>' +
                '<button class="close-popover btn btn-default" data-po=' + jsEvent.target + ' title="Cerrar"><i class="glyphicon glyphicon-remove"></i></button>' +
                '</div>';
        }


        if (event.data.no_show == 1) {
            let html2 = '<div class="no-show-popover">No se presentó ' +
                '<button id="show-appointment" data-appointment-id="' + event.id + '" class="btn-default" title="Quitar"><i class="glyphicon glyphicon-remove-circle"></i></button>' +
                '</div> ';
            html = html2 + html;
        }

        jsEvent.currentTarget.classList.remove('shadowAppointment'); //Remueve el shadow
        window.history.replaceState({}, document.title, "/" + "index.php/backend"); // limpio la url

        $(jsEvent.target).popover({
            placement: 'top',
            title: event.title,
            content: html,
            html: true,
            container: '#calendar',
            trigger: 'manual'
        });

        lastFocusedEventData = event;

        $(jsEvent.target).popover('toggle');

        // Fix popover position.
        if ($('.popover').length > 0 && $('.popover').position().top < 200) {
            $('.popover').css('top', '200px');
        }

        /**
         * Event: Manage Appointments Dialog Save Button "Click"
         *
         * Stores the appointment changes or inserts a new appointment depending the dialog mode.
         */
        $('button#no-show-appointment').click(function (e) {
            // Before doing anything the appointment data need to be validated.
            let appointmentId = e.currentTarget.dataset.appointmentId;

            // Prepare appointment data for AJAX request.
            let appointment = {
                id: appointmentId,
                no_show: true
            };

            // Define success callback.
            let successCallback = function (response) {
                $('.popover').remove(); // Close all open popovers.
                $('#select-filter-item').trigger('change');
            };

            // Define error callback.
            let errorCallback = function () {
                /*$dialog.find('.modal-message').text(EALang.service_communication_error);
                $dialog.find('.modal-message').addClass('alert-danger').removeClass('hidden');
                $dialog.find('.modal-body').scrollTop(0);*/
            };

            BackendCalendarApi.saveNoShow(appointment, successCallback, errorCallback);
        });

        /**
         * Event: Manage Appointments Dialog Save Button "Click"
         *
         * Stores the appointment changes or inserts a new appointment depending the dialog mode.
         */
        $('button#show-appointment').click(function (e) {
            // Before doing anything the appointment data need to be validated.
            let appointmentId = e.currentTarget.dataset.appointmentId;

            // Prepare appointment data for AJAX request.
            let appointment = {
                id: appointmentId,
                no_show: false
            };

            // Define success callback.
            let successCallback = function (response) {
                $('.popover').remove(); // Close all open popovers.
                $('#select-filter-item').trigger('change');
            };

            // Define error callback.
            let errorCallback = function () {
                /*$dialog.find('.modal-message').text(EALang.service_communication_error);
                $dialog.find('.modal-message').addClass('alert-danger').removeClass('hidden');
                $dialog.find('.modal-body').scrollTop(0);*/
            };

            BackendCalendarApi.saveNoShow(appointment, successCallback, errorCallback);
        });
    }

    /**
     * Calendar Event "Resize" Callback
     *
     * The user can change the duration of an event by resizing an appointment object on the calendar. This
     * change needs to be stored to the database too and this is done via an ajax call.
     *
     * @see updateAppointmentData()
     */
    function _calendarEventResize(event, delta, revertFunc, jsEvent, ui, view) {
        if (GlobalVariables.user.privileges.appointments.edit == false) {
            revertFunc();
            Backend.displayNotification(EALang.no_privileges_edit_appointments);
            return;
        }

        let $calendar = $('#calendar');

        if ($('#notification').is(':visible')) {
            $('#notification').hide('bind');
        }

        if (event.data.is_unavailable == false) {
            // Prepare appointment data.
            event.data.end_datetime = Date.parseExact(
                event.data.end_datetime, 'yyyy-MM-dd HH:mm:ss')
                .add({days: delta.days(), hours: delta.hours(), minutes: delta.minutes()})
                .toString('yyyy-MM-dd HH:mm:ss');

            let appointment = GeneralFunctions.clone(event.data);

            // Must delete the following because only appointment data should be provided to the AJAX call.
            delete appointment.customer;
            delete appointment.provider;
            delete appointment.service;

            // Success callback
            let successCallback = function (response) {
                if (response.exceptions) {
                    response.exceptions = GeneralFunctions.parseExceptions(response.exceptions);
                    GeneralFunctions.displayMessageBox(GeneralFunctions.EXCEPTIONS_TITLE, GeneralFunctions.EXCEPTIONS_MESSAGE);
                    $('#message_box').append(GeneralFunctions.exceptionsToHtml(response.exceptions));
                    return;
                }

                if (response.warnings) {
                    // Display warning information to the user.
                    response.warnings = GeneralFunctions.parseExceptions(response.warnings);
                    GeneralFunctions.displayMessageBox(GeneralFunctions.WARNINGS_TITLE, GeneralFunctions.WARNINGS_MESSAGE);
                    $('#message_box').append(GeneralFunctions.exceptionsToHtml(response.warnings));
                }

                // Display success notification to user.
                let undoFunction = function () {
                    appointment.end_datetime = event.data.end_datetime = Date.parseExact(
                        appointment.end_datetime, 'yyyy-MM-dd HH:mm:ss')
                        .add({days: -delta.days(), hours: -delta.hours(), minutes: -delta.minutes()})
                        .toString('yyyy-MM-dd HH:mm:ss');

                    let url = GlobalVariables.baseUrl + '/index.php/backend_api/ajax_save_appointment';

                    let data = {
                        csrfToken: GlobalVariables.csrfToken,
                        appointment_data: JSON.stringify(appointment)
                    };

                    $.post(url, data, function (response) {
                        $('#notification').hide('blind');
                        revertFunc();
                    }, 'json').fail(GeneralFunctions.ajaxFailureHandler);
                };

                Backend.displayNotification(EALang.appointment_updated, [
                    {
                        'label': 'Undo',
                        'function': undoFunction
                    }
                ]);
                $('#footer').css('position', 'static'); // Footer position fix.

                // Update the event data for later use.
                $calendar.fullCalendar('updateEvent', event);
            };

            // Update appointment data.
            BackendCalendarApi.saveAppointment(appointment, undefined, successCallback);
        } else {
            // Update unavailable time period.
            let unavailable = {
                id: event.data.id,
                start_datetime: event.start.format('YYYY-MM-DD HH:mm:ss'),
                end_datetime: event.end.format('YYYY-MM-DD HH:mm:ss'),
                id_users_provider: event.data.id_users_provider
            };

            event.data.end_datetime = unavailable.end_datetime;

            // Define success callback function.
            let successCallback = function (response) {
                if (response.exceptions) {
                    response.exceptions = GeneralFunctions.parseExceptions(response.exceptions);
                    GeneralFunctions.displayMessageBox(GeneralFunctions.EXCEPTIONS_TITLE, GeneralFunctions.EXCEPTIONS_MESSAGE);
                    $('#message_box').append(GeneralFunctions.exceptionsToHtml(response.exceptions));
                    return;
                }

                if (response.warnings) {
                    // Display warning information to the user.
                    response.warnings = GeneralFunctions.parseExceptions(response.warnings);
                    GeneralFunctions.displayMessageBox(GeneralFunctions.WARNINGS_TITLE, GeneralFunctions.WARNINGS_MESSAGE);
                    $('#message_box').append(GeneralFunctions.exceptionsToHtml(response.warnings));
                }

                // Display success notification to user.
                let undoFunction = function () {
                    unavailable.end_datetime = event.data.end_datetime = Date.parseExact(
                        unavailable.end_datetime, 'yyyy-MM-dd HH:mm:ss')
                        .add({minutes: -delta.minutes()})
                        .toString('yyyy-MM-dd HH:mm:ss');

                    let url = GlobalVariables.baseUrl + '/index.php/backend_api/ajax_save_unavailable';
                    let data = {
                        csrfToken: GlobalVariables.csrfToken,
                        unavailable: JSON.stringify(unavailable)
                    };

                    $.post(url, data, function (response) {
                        $('#notification').hide('blind');
                        revertFunc();
                    }, 'json').fail(GeneralFunctions.ajaxFailureHandler);
                };

                Backend.displayNotification(EALang.unavailable_updated, [
                    {
                        'label': 'Undo',
                        'function': undoFunction
                    }
                ]);

                $('#footer').css('position', 'static'); // Footer position fix.

                // Update the event data for later use.
                $calendar.fullCalendar('updateEvent', event);
            };

            BackendCalendarApi.saveUnavailable(unavailable, successCallback);
        }
    }

    /**
     * Calendar Window "Resize" Callback
     *
     * The calendar element needs to be re-sized too in order to fit into the window. Nevertheless, if the window
     * becomes very small the the calendar won't shrink anymore.
     *
     * @see _getCalendarHeight()
     */
    function _calendarWindowResize(view) {
        $('#calendar').fullCalendar('option', 'height', _getCalendarHeight());
    }

    /**
     * Calendar Day "Click" Callback
     *
     * When the user clicks on a day square on the calendar, then he will automatically be transferred to that
     * day view calendar.
     */
    function _calendarDayClick(date, jsEvent, view) {
        if (!date.hasTime()) {


            $('#calendar').fullCalendar('changeView', 'agendaDay');
            $('#calendar').fullCalendar('gotoDate', date);

        }
    }

    /**
     * Calendar Event "Drop" Callback
     *
     * This event handler is triggered whenever the user drags and drops an event into a different position
     * on the calendar. We need to update the database with this change. This is done via an ajax call.
     */
    function _calendarEventDrop(event, delta, revertFunc, jsEvent, ui, view) {
        if (GlobalVariables.user.privileges.appointments.edit == false) {
            revertFunc();
            Backend.displayNotification(EALang.no_privileges_edit_appointments);
            return;
        }

        if ($('#notification').is(':visible')) {
            $('#notification').hide('bind');
        }

        if (event.data.is_unavailable == false) {
            // Prepare appointment data.
            let appointment = GeneralFunctions.clone(event.data);

            // Must delete the following because only appointment data should be provided to the ajax call.
            delete appointment.customer;
            delete appointment.provider;
            delete appointment.service;

            appointment.start_datetime = Date.parseExact(
                appointment.start_datetime, 'yyyy-MM-dd HH:mm:ss')
                .add({days: delta.days(), hours: delta.hours(), minutes: delta.minutes()})
                .toString('yyyy-MM-dd HH:mm:ss');

            appointment.end_datetime = Date.parseExact(
                appointment.end_datetime, 'yyyy-MM-dd HH:mm:ss')
                .add({days: delta.days(), hours: delta.hours(), minutes: delta.minutes()})
                .toString('yyyy-MM-dd HH:mm:ss');

            event.data.start_datetime = appointment.start_datetime;
            event.data.end_datetime = appointment.end_datetime;

            // Define success callback function.
            let successCallback = function (response) {
                if (response.exceptions) {
                    response.exceptions = GeneralFunctions.parseExceptions(response.exceptions);
                    GeneralFunctions.displayMessageBox(GeneralFunctions.EXCEPTIONS_TITLE, GeneralFunctions.EXCEPTIONS_MESSAGE);
                    $('#message_box').append(GeneralFunctions.exceptionsToHtml(response.exceptions));
                    return;
                }

                if (response.warnings) {
                    // Display warning information to the user.
                    response.warnings = GeneralFunctions.parseExceptions(response.warnings);
                    GeneralFunctions.displayMessageBox(GeneralFunctions.WARNINGS_TITLE, GeneralFunctions.WARNINGS_MESSAGE);
                    $('#message_box').append(GeneralFunctions.exceptionsToHtml(response.warnings));
                }

                // Define the undo function, if the user needs to reset the last change.
                let undoFunction = function () {
                    appointment.start_datetime = Date.parseExact(
                        appointment.start_datetime, 'yyyy-MM-dd HH:mm:ss')
                        .add({days: -delta.days(), hours: -delta.hours(), minutes: -delta.minutes()})
                        .toString('yyyy-MM-dd HH:mm:ss');

                    appointment.end_datetime = Date.parseExact(
                        appointment.end_datetime, 'yyyy-MM-dd HH:mm:ss')
                        .add({days: -delta.days(), hours: -delta.hours(), minutes: -delta.minutes()})
                        .toString('yyyy-MM-dd HH:mm:ss');

                    event.data.start_datetime = appointment.start_datetime;
                    event.data.end_datetime = appointment.end_datetime;

                    let url = GlobalVariables.baseUrl + '/index.php/backend_api/ajax_save_appointment';
                    let data = {
                        csrfToken: GlobalVariables.csrfToken,
                        appointment_data: JSON.stringify(appointment)
                    };

                    $.post(url, data, function (response) {
                        $('#notification').hide('blind');
                        revertFunc();
                    }, 'json').fail(GeneralFunctions.ajaxFailureHandler);
                };

                Backend.displayNotification(EALang.appointment_updated, [
                    {
                        'label': 'Undo',
                        'function': undoFunction
                    }
                ]);

                $('#footer').css('position', 'static'); // Footer position fix.
            };

            // Update appointment data.
            BackendCalendarApi.saveAppointment(appointment, undefined, successCallback);
        } else {
            // Update unavailable time period.
            let unavailable = {
                id: event.data.id,
                start_datetime: event.start.format('YYYY-MM-DD HH:mm:ss'),
                end_datetime: event.end.format('YYYY-MM-DD HH:mm:ss'),
                id_users_provider: event.data.id_users_provider
            };

            let successCallback = function (response) {
                if (response.exceptions) {
                    response.exceptions = GeneralFunctions.parseExceptions(response.exceptions);
                    GeneralFunctions.displayMessageBox(GeneralFunctions.EXCEPTIONS_TITLE, GeneralFunctions.EXCEPTIONS_MESSAGE);
                    $('#message_box').append(GeneralFunctions.exceptionsToHtml(response.exceptions));
                    return;
                }

                if (response.warnings) {
                    response.warnings = GeneralFunctions.parseExceptions(response.warnings);
                    GeneralFunctions.displayMessageBox(GeneralFunctions.WARNINGS_TITLE, GeneralFunctions.WARNINGS_MESSAGE);
                    $('#message_box').append(GeneralFunctions.exceptionsToHtml(response.warnings));
                }

                let undoFunction = function () {
                    unavailable.start_datetime = Date.parseExact(
                        unavailable.start_datetime, 'yyyy-MM-dd HH:mm:ss')
                        .add({days: -delta.days(), minutes: -delta.minutes()})
                        .toString('yyyy-MM-dd HH:mm:ss');

                    unavailable.end_datetime = Date.parseExact(
                        unavailable.end_datetime, 'yyyy-MM-dd HH:mm:ss')
                        .add({days: -delta.days(), minutes: -delta.minutes()})
                        .toString('yyyy-MM-dd HH:mm:ss');

                    event.data.start_datetime = unavailable.start_datetime;
                    event.data.end_datetime = unavailable.end_datetime;

                    let url = GlobalVariables.baseUrl + '/index.php/backend_api/ajax_save_unavailable';
                    let data = {
                        csrfToken: GlobalVariables.csrfToken,
                        unavailable: JSON.stringify(unavailable)
                    };

                    $.post(url, data, function (response) {
                        $('#notification').hide('blind');
                        revertFunc();
                    }, 'json').fail(GeneralFunctions.ajaxFailureHandler);
                };

                Backend.displayNotification(EALang.unavailable_updated, [
                    {
                        label: 'Undo',
                        function: undoFunction
                    }
                ]);

                $('#footer').css('position', 'static'); // Footer position fix.
            };

            BackendCalendarApi.saveUnavailable(unavailable, successCallback);
        }
    }

    /**
     * Calendar "View Render" Callback
     *
     * Whenever the calendar changes or refreshes its view certain actions need to be made, in order to
     * display proper information to the user.
     */
    function _calendarViewRender() {
        if ($('#select-filter-item').val() === null) {
            return;
        }

        let $option = $('#select-filter-item').find(':selected');

        let optionId = getParameterByName('optionId');

        /*let recordId;

        if (optionId) {
            recordId = optionId;
        } else {
            recordId = $('#select-filter-item').val();
        }*/

        _refreshCalendarAppointments(
            $('#calendar'),
            $option.data('provider'), //id del centro
            $option.val(), //id del servicio
            $('#select-filter-item option:selected').attr('type'),
            $('#calendar').fullCalendar('getView').start,
            $('#calendar').fullCalendar('getView').end
        );

        $(window).trigger('resize'); // Places the footer on the bottom.

        // Remove all open popovers.
        $('.close-popover').each(function () {
            $(this).parents().eq(2).remove();
        });

        // Add new pop overs.
        $('.fv-events').each(function (index, eventHandle) {
            $(eventHandle).popover();
        });

    }

    /**
     * Convert titles to HTML
     *
     * On some calendar events the titles contain html markup that is not displayed properly due to the
     * FullCalendar plugin. This plugin sets the .fc-event-title value by using the $.text() method and
     * not the $.html() method. So in order for the title to display the html properly we convert all the
     * .fc-event-titles where needed into html.
     */
    function _convertTitlesToHtml() {
        // Convert the titles to html code.
        $('.fc-custom').each(function () {
            let title = $(this).find('.fc-event-title').text();
            $(this).find('.fc-event-title').html(title);
            let time = $(this).find('.fc-event-time').text();
            $(this).find('.fc-event-time').html(time);
        });
    }

    /**
     * Refresh Calendar Appointments
     *
     * This method reloads the registered appointments for the selected date period and filter type.
     *
     * @param {Object} $calendar The calendar jQuery object.
     * @param {Number} recordId The selected record id.
     * @param {Number} serviceId The service id selected
     * @param {String} filterType The filter type, could be either FILTER_TYPE_PROVIDER or FILTER_TYPE_SERVICE.
     * @param {Date} startDate Visible start date of the calendar.
     * @param {Date} endDate Visible end date of the calendar.
     */
    function _refreshCalendarAppointments($calendar, recordId, serviceId, filterType, startDate, endDate) {
        let url = GlobalVariables.baseUrl + '/index.php/backend_api/ajax_get_calendar_appointments';
        let data = {
            csrfToken: GlobalVariables.csrfToken,
            provider_id: recordId,
            service_id: serviceId,
            start_date: startDate.format('YYYY-MM-DD'),
            end_date: endDate.format('YYYY-MM-DD'),
            filter_type: filterType
        };


        $.post(url, data, function (response) {
            if (!GeneralFunctions.handleAjaxExceptions(response)) {
                return;
            }

            // Add appointments to calendar.
            let calendarEvents = [];
            let $calendar = $('#calendar');

            $.each(response.appointments, function (index, appointment) {
                let hash = getParameterByName('hash'); //5c673ebbcdfaa8a39e199b832fade1a7

                console.log(appointment)
                let shadowAppointment = (hash == appointment.hash) ? ' shadowAppointment ' : '';
                let event = {
                    id: appointment.id,
                    title: appointment.customer.first_name + ' ' + appointment.customer.last_name,// + ' | '+appointment.service.name, @todo acà arma el evento para mostrar en pantalla
                    start: moment(appointment.start_datetime),
                    end: moment(appointment.end_datetime),
                    allDay: false,
                    data: appointment, // Store appointment data for later use.
                    className: ((appointment.no_show != 0) ? shadowAppointment + ' no-show' : shadowAppointment) + appointment.created_by === 'Online' && ' online'

                };

                let isAfter = moment().isAfter(appointment.start_datetime);

                if (isAfter) {
                    event.editable = false;
                    event.resizable = false;
                }

                calendarEvents.push(event);
            });

            //BackendCalendarApi.getAvailableHours(moment().format('YYYY-MM-DD HH:mm:ss'), serviceId, recordId);
            let service = getService(serviceId);

            $calendar.fullCalendar('removeEvents');
            $calendar.fullCalendar('addEventSource', calendarEvents);

            if ($('#calendar').fullCalendar('option', 'slotDuration') != '00:' + service.duration + ':00') {
                $calendar.fullCalendar('option', 'snapDuration', '00:' + service.duration + ':00');
                $calendar.fullCalendar('option', 'slotDuration', '00:' + service.duration + ':00');
            }

            let weekDays = [
                'sunday',
                'monday',
                'tuesday',
                'wednesday',
                'thursday',
                'friday',
                'saturday'
            ];

            // :: ADD PROVIDER'S UNAVAILABLE TIME PERIODS
            let calendarView = $calendar.fullCalendar('getView').name;

            if (calendarView !== 'month') {
                $.each(GlobalVariables.availableServices, function (index, service) {
                    //$.each(GlobalVariables.availableProviders, function (index, provider) {
                    // if (provider.id == recordId) {
                    if (service.id == serviceId) {

                        let workingPlan = jQuery.parseJSON(service.working_plan);
                        let vacationPlan = jQuery.parseJSON(service.vacation_plan);
                        let closedPlan = jQuery.parseJSON(service.closed_plan);

                        let unavailablePeriod;

                        switch (calendarView) {
                            case 'agendaDay':
                                let selectedDayName = weekDays[$calendar.fullCalendar('getView').start.format('d')];

                                // Add custom unavailable periods.
                                $.each(response.unavailables, function (index, unavailable) {
                                    let notes = unavailable.notes ? ' - ' + unavailable.notes : '';

                                    if (unavailable.notes.length > 30) {
                                        notes = unavailable.notes.substring(0, 30) + '...'
                                    }

                                    let unavailablePeriod = {
                                        title: EALang.unavailable + notes,
                                        start: moment(unavailable.start_datetime),
                                        end: moment(unavailable.end_datetime),
                                        allDay: false,
                                        color: '#879DB4',
                                        editable: true,
                                        className: 'fc-unavailable fc-custom',
                                        data: unavailable
                                    };

                                    $calendar.fullCalendar('renderEvent', unavailablePeriod, false);
                                });

                                // Non-working day.
                                if (workingPlan[selectedDayName] == null) {
                                    unavailablePeriod = {
                                        title: EALang.not_working,
                                        start: $calendar.fullCalendar('getView').intervalStart.clone(),
                                        end: $calendar.fullCalendar('getView').intervalEnd.clone(),
                                        allDay: false,
                                        color: '#BEBEBE',
                                        editable: false,
                                        className: 'fc-unavailable'
                                    };

                                    $calendar.fullCalendar('renderEvent', unavailablePeriod, false);

                                    return; // Go to next loop.
                                }

                                // Add unavailable period before work starts.
                                let calendarDateStart = moment($calendar.fullCalendar('getView').start.format('YYYY-MM-DD') + ' 00:00:00');
                                let startHour = workingPlan[selectedDayName].start.split(':');
                                let workDateStart = calendarDateStart.clone();
                                workDateStart.hour(parseInt(startHour[0]));
                                workDateStart.minute(parseInt(startHour[1]));

                                if (calendarDateStart < workDateStart) {
                                    let unavailablePeriodBeforeWorkStarts = {
                                        title: EALang.not_working,
                                        start: calendarDateStart,
                                        end: workDateStart,
                                        allDay: false,
                                        color: '#BEBEBE',
                                        editable: false,
                                        className: 'fc-unavailable'
                                    };
                                    $calendar.fullCalendar('renderEvent', unavailablePeriodBeforeWorkStarts, false);
                                }

                                // Add unavailable period after work ends.
                                let calendarDateEnd = moment($calendar.fullCalendar('getView').end.format('YYYY-MM-DD') + ' 00:00:00');
                                let endHour = workingPlan[selectedDayName].end.split(':');
                                let workDateEnd = calendarDateStart.clone();

                                workDateEnd.hour(parseInt(endHour[0]));
                                workDateEnd.minute(parseInt(endHour[1]));

                                if (calendarDateEnd > workDateEnd) {
                                    let unavailablePeriodAfterWorkEnds = {
                                        title: EALang.not_working,
                                        start: workDateEnd,
                                        end: calendarDateEnd,
                                        allDay: false,
                                        color: '#BEBEBE',
                                        editable: false,
                                        className: 'fc-unavailable'
                                    };

                                    $calendar.fullCalendar('renderEvent', unavailablePeriodAfterWorkEnds, false);
                                }

                                // Add unavailable periods for breaks.
                                let breakStart;
                                let breakEnd;

                                $.each(workingPlan[selectedDayName].breaks, function (index, currentBreak) {
                                    let breakStartString = currentBreak.start.split(':');
                                    breakStart = calendarDateStart.clone();
                                    breakStart.hour(parseInt(breakStartString[0]));
                                    breakStart.minute(parseInt(breakStartString[1]));

                                    let breakEndString = currentBreak.end.split(':');
                                    breakEnd = calendarDateStart.clone();
                                    breakEnd.hour(parseInt(breakEndString[0]));
                                    breakEnd.minute(parseInt(breakEndString[1]));

                                    let unavailablePeriod = {
                                        title: EALang.break,
                                        start: breakStart,
                                        end: breakEnd,
                                        allDay: false,
                                        color: '#BEBEBE',
                                        editable: false,
                                        className: 'fc-unavailable fc-break'
                                    };

                                    $calendar.fullCalendar('renderEvent', unavailablePeriod, false);
                                });

                                break;


                            case 'agendaWeek':
                                let currentDateStart = $calendar.fullCalendar('getView').start.clone();
                                let currentDateEnd = currentDateStart.clone().add(1, 'days');

                                // Add custom unavailable periods (they are always displayed on the calendar, even if
                                // the provider won't work on that day).
                                $.each(response.unavailables, function (index, unavailable) {
                                    let notes = unavailable.notes ? ' - ' + unavailable.notes : '';

                                    if (unavailable.notes.length > 30) {
                                        notes = unavailable.notes.substring(0, 30) + '...'
                                    }

                                    unavailablePeriod = {
                                        title: EALang.unavailable + notes,
                                        start: moment(unavailable.start_datetime),
                                        end: moment(unavailable.end_datetime),
                                        allDay: false,
                                        color: '#879DB4',
                                        editable: true,
                                        className: 'fc-unavailable fc-custom',
                                        data: unavailable
                                    };

                                    $calendar.fullCalendar('renderEvent', unavailablePeriod, false);
                                });

                                $.each(workingPlan, function (index, workingDay) {
                                    if (workingDay == null) {
                                        // Add a full day unavailable event.
                                        unavailablePeriod = {
                                            title: EALang.not_working,
                                            start: moment(currentDateStart.format('YYYY-MM-DD')),
                                            end: moment(currentDateEnd.format('YYYY-MM-DD')),
                                            allDay: false,
                                            color: '#BEBEBE',
                                            editable: false,
                                            className: 'fc-unavailable'
                                        };

                                        $calendar.fullCalendar('renderEvent', unavailablePeriod, true);
                                        currentDateStart.add(1, 'days');
                                        currentDateEnd.add(1, 'days');

                                        return; // Go to the next loop.
                                    }

                                    let start;
                                    let end;

                                    // Add unavailable period before work starts.
                                    let workingDayStartString = workingDay.start.split(':');
                                    start = currentDateStart.clone();
                                    start.hour(parseInt(workingDayStartString[0]));
                                    start.minute(parseInt(workingDayStartString[1]));

                                    if (currentDateStart < start) {
                                        unavailablePeriod = {
                                            title: EALang.not_working,
                                            start: moment(currentDateStart.format('YYYY-MM-DD') + ' 00:00:00'),
                                            end: moment(currentDateStart.format('YYYY-MM-DD') + ' ' + workingDay.start + ':00'),
                                            allDay: false,
                                            color: '#BEBEBE',
                                            editable: false,
                                            className: 'fc-unavailable'
                                        };

                                        $calendar.fullCalendar('renderEvent', unavailablePeriod, true);
                                    }

                                    // Add unavailable period after work ends.
                                    let workingDayEndString = workingDay.end.split(':');
                                    end = currentDateStart.clone();
                                    end.hour(parseInt(workingDayEndString[0]));
                                    end.minute(parseInt(workingDayEndString[1]));

                                    if (currentDateEnd > end) {
                                        unavailablePeriod = {
                                            title: EALang.not_working,
                                            start: moment(currentDateStart.format('YYYY-MM-DD') + ' ' + workingDay.end + ':00'),
                                            end: moment(currentDateEnd.format('YYYY-MM-DD') + ' 00:00:00'),
                                            allDay: false,
                                            color: '#BEBEBE',
                                            editable: false,
                                            className: 'fc-unavailable'
                                        };

                                        $calendar.fullCalendar('renderEvent', unavailablePeriod, false);
                                    }

                                    // Add unavailable periods during day breaks.
                                    let breakStart;
                                    let breakEnd;

                                    $.each(workingDay.breaks, function (index, currentBreak) {
                                        let breakStartString = currentBreak.start.split(':');
                                        breakStart = currentDateStart.clone();
                                        breakStart.hour(parseInt(breakStartString[0]));
                                        breakStart.minute(parseInt(breakStartString[1]));

                                        let breakEndString = currentBreak.end.split(':');
                                        breakEnd = currentDateStart.clone();
                                        breakEnd.hour(parseInt(breakEndString[0]));
                                        breakEnd.minute(parseInt(breakEndString[1]));

                                        let unavailablePeriod = {
                                            title: EALang.break,
                                            start: moment(currentDateStart.format('YYYY-MM-DD') + ' ' + currentBreak.start),
                                            end: moment(currentDateStart.format('YYYY-MM-DD') + ' ' + currentBreak.end),
                                            allDay: false,
                                            color: '#BEBEBE',
                                            editable: false,
                                            className: 'fc-unavailable fc-break'
                                        };

                                        $calendar.fullCalendar('renderEvent', unavailablePeriod, false);
                                    });

                                    // Add unavailable periods during vacations period.
                                    $.each(vacationPlan, function (ind, vacation) {
                                        let weekDays = {
                                            Domingo: 0,
                                            Lunes: 1,
                                            Martes: 2,
                                            Miercoles: 3,
                                            Jueves: 4,
                                            Viernes: 5,
                                            Sabado: 6
                                        };

                                        if ((currentDateStart.day(index).format() >= moment(vacation.from).format('YYYY-MM-DD')
                                            && currentDateStart.day(index).format() <= moment(vacation.to).format('YYYY-MM-DD'))
                                            && currentDateStart.day(index).weekday() == weekDays[vacation.day]
                                        ) {

                                            let unavailablePeriod = {
                                                title: EALang.vacation_calendar,
                                                start: moment(currentDateStart.format('YYYY-MM-DD') + ' ' + vacation.start),
                                                end: moment(currentDateStart.format('YYYY-MM-DD') + ' ' + vacation.end),
                                                allDay: false,
                                                color: '#be7d9d',
                                                editable: false,
                                                className: 'fc-unavailable fc-vacation'
                                            };

                                            $calendar.fullCalendar('renderEvent', unavailablePeriod, false);
                                        }

                                    });

                                    // Add unavailable periods during closeds period.
                                    $.each(closedPlan, function (ind, closed) {

                                        if ((currentDateStart.day(index).format() >= moment(closed.start).format('Y-MM-DD')
                                            && currentDateStart.day(index).format() <= moment(closed.end).format('Y-MM-DD'))
                                        ) {
                                            $calendar.fullCalendar('removeEvents', function (event) {
                                                if (event.start.format('YYYY-MM-DD') === currentDateStart.day(index).format('YYYY-MM-DD')) {
                                                    return true;
                                                }
                                                /*if(event.start.toDateString()===currentDateStart.day(index).toDateString())
                                                    return true;*/
                                            });

                                            let unavailablePeriod = {
                                                title: EALang.closed_calendar,
                                                start: moment(currentDateStart.format('YYYY-MM-DD')),
                                                end: moment(currentDateEnd.format('YYYY-MM-DD')),
                                                allDay: false,
                                                color: '#BEBEBE',
                                                editable: false,
                                                className: 'fc-unavailable'
                                            };

                                            $calendar.fullCalendar('renderEvent', unavailablePeriod, true);
                                        }

                                    });

                                    currentDateStart.add(1, 'days');
                                    currentDateEnd.add(1, 'days');
                                });

                                break;
                        }
                    }
                });

            }
        }, 'json').fail(GeneralFunctions.ajaxFailureHandler);
    }


    exports.initialize = function () {
        // Dynamic date formats.
        let columnFormat = {};

        let provider = getProvider($('#select-filter-item').find(':selected').data('provider'));
        let service = getService($('#select-filter-item').val());

        switch (GlobalVariables.dateFormat) {
            case 'DMY':
                columnFormat = 'ddd D/M';
                break;
            case 'MDY':
            case 'YMD':
                columnFormat = 'ddd M/D';
                break;
            default:
                throw new Error('Invalid date format setting provided!', GlobalVariables.dateFormat);
        }

        // Time formats
        let timeFormat = '';
        let slotTimeFormat = '';

        switch (GlobalVariables.timeFormat) {
            case 'military':
                timeFormat = 'H:mm';
                slotTimeFormat = 'H(:mm)';
                break;
            case 'regular':
                timeFormat = 'h:mm a';
                slotTimeFormat = 'h(:mm) a';
                break;
            default:
                throw new Error('Invalid time format setting provided!', GlobalVariables.timeFormat);
        }

        let defaultView = window.innerWidth < 468 ? 'agendaDay' : 'agendaWeek';

        // Initialize page calendar
        $('#calendar').fullCalendar({
            defaultView: defaultView,
            height: _getCalendarHeight(),
            editable: true,
            minTime: "07:00:00",
            maxTime: "21:00:00",
            firstDay: 0,
            snapDuration: '00:' + service.duration + ':00',
            slotDuration: '00:' + service.duration + ':00',
            timeFormat: timeFormat,
            slotLabelFormat: slotTimeFormat,
            allDayText: EALang.all_day,
            columnFormat: columnFormat,
            titleFormat: 'MMMM YYYY',
            nowIndicator: true,
            //No se puede hacer click en los dias pasados para sacar un turno
            selectAllow: function (selectInfo) {
                return moment().diff(selectInfo.start) <= 0
            },
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'agendaDay,agendaWeek,month'
            },

            // Selectable
            selectable: true,
            selectHelper: true,
            select: function (start, end, jsEvent, view) {
                if (!start.hasTime() || !end.hasTime()) {
                    return;
                }

                $('#insert-appointment').data({
                    'date': start.format('YYYY-MM-DD'),
                    'start': start.format('HH:mm')
                }).trigger('click');

                // Preselect service & provider.
                if ($('#select-filter-item option:selected').attr('type') === FILTER_TYPE_SERVICE) {
                    let service = GlobalVariables.availableServices.find(function (service) {
                        return service.id == $('#select-filter-item').val()
                    });
                    $('#select-service').val(service.id).trigger('change');

                } else {
                    let provider = GlobalVariables.availableProviders.find(function (provider) {
                        return provider.id == $('#select-filter-item').val();
                    });

                    let service = GlobalVariables.availableServices.find(function (service) {
                        return provider.services.indexOf(service.id) !== -1
                    });

                    $('#select-service').val(service.id).trigger('change');
                    $('#select-provider').val(provider.id).trigger('change');
                }

                return false;
            },

            // Translations
            monthNames: [EALang.january, EALang.february, EALang.march, EALang.april,
                EALang.may, EALang.june, EALang.july, EALang.august,
                EALang.september, EALang.october, EALang.november,
                EALang.december],
            monthNamesShort: [EALang.january.substr(0, 3), EALang.february.substr(0, 3),
                EALang.march.substr(0, 3), EALang.april.substr(0, 3),
                EALang.may.substr(0, 3), EALang.june.substr(0, 3),
                EALang.july.substr(0, 3), EALang.august.substr(0, 3),
                EALang.september.substr(0, 3), EALang.october.substr(0, 3),
                EALang.november.substr(0, 3), EALang.december.substr(0, 3)],
            dayNames: [EALang.sunday, EALang.monday, EALang.tuesday, EALang.wednesday,
                EALang.thursday, EALang.friday, EALang.saturday],
            dayNamesShort: [EALang.sunday.substr(0, 3), EALang.monday.substr(0, 3),
                EALang.tuesday.substr(0, 3), EALang.wednesday.substr(0, 3),
                EALang.thursday.substr(0, 3), EALang.friday.substr(0, 3),
                EALang.saturday.substr(0, 3)],
            dayNamesMin: [EALang.sunday.substr(0, 2), EALang.monday.substr(0, 2),
                EALang.tuesday.substr(0, 2), EALang.wednesday.substr(0, 2),
                EALang.thursday.substr(0, 2), EALang.friday.substr(0, 2),
                EALang.saturday.substr(0, 2)],
            buttonText: {
                today: EALang.today,
                day: EALang.day,
                week: EALang.week,
                month: EALang.month
            },

            // Calendar events need to be declared on initialization.
            windowResize: _calendarWindowResize,
            viewRender: _calendarViewRender,
            dayClick: _calendarDayClick,
            eventClick: _calendarEventClick,
            eventResize: _calendarEventResize,
            eventDrop: _calendarEventDrop,
            eventAfterAllRender: _convertTitlesToHtml
        });

        // Trigger once to set the proper footer position after calendar initialization.
        _calendarWindowResize();

        // Fill the select list boxes of the page.
        if (GlobalVariables.availableProviders.length > 0) {
            $.each(GlobalVariables.availableProviders, function (index, provider) {
                let areService = false;
                let optgroupHtml = '<optgroup label="' + provider.first_name + ' ' + provider.last_name + '" type="services-group">';

                $.each(GlobalVariables.availableServices, function (index, service) {
                    if (provider.services.includes(service.id)) {
                        optgroupHtml += '<option value="' + service.id + '" type="' + FILTER_TYPE_SERVICE + '" data-provider="' + provider.id + '">' +
                            service.name + '</option>';
                        areService = true;
                    }
                });

                optgroupHtml += '</optgroup>';

                if (areService){
                    let select_filter_item = GeneralFunctions.getUrlParameter(location.href, 'select-filter-item');
                    $('select#select-filter-item').append(optgroupHtml).val((select_filter_item === '' || select_filter_item == null) ? getFirstAvailableServiceId() : select_filter_item)
                }
            });
        }

        // Check permissions.
        if (GlobalVariables.user.role_slug == Backend.DB_SLUG_PROVIDER) {
            $('#select-filter-item optgroup:eq(0)')
                .find('option[value="' + GlobalVariables.user.id + '"]')
                .prop('selected', true);
            $('#select-filter-item').prop('disabled', true);
        }

        /*
        if (GlobalVariables.user.role_slug == Backend.DB_SLUG_SECRETARY) {
            $('#select-filter-item optgroup:eq(1)').remove();
        }
        if (GlobalVariables.user.role_slug == Backend.DB_SLUG_SECRETARY) {
            // Remove the providers that are not connected to the secretary.
            $('#select-filter-item option[type="provider"]').each(function (index, option) {
                let found = false;

                $.each(GlobalVariables.secretaryProviders, function (index, id) {
                    if ($(option).val() == id) {
                        found = true;
                        return false;
                    }
                });

                if (!found) {
                    $(option).remove();
                }
            });

            if ($('#select-filter-item option[type="provider"]').length == 0) {
                $('#select-filter-item optgroup[type="providers-group"]').remove();
            }
        }*/

        // Bind the default event handlers.
        _bindEventHandlers();

        $('#select-filter-item').trigger('change');

        // Display the edit dialog if an appointment hash is provided.
        if (GlobalVariables.editAppointment != null) {
            let $dialog = $('#manage-appointment');

            let appointment = GlobalVariables.editAppointment;
            BackendCalendarAppointmentsModal.resetAppointmentDialog();

            $dialog.find('.modal-header h3').text(EALang.edit_appointment_title);
            $dialog.find('#appointment-id').val(appointment.id);
            $dialog.find('#select-service').val(appointment.id_services).change();
            $dialog.find('#select-provider').val(appointment.id_users_provider);

            // Set the start and end datetime of the appointment.
            let startDatetime = Date.parseExact(appointment.start_datetime, 'yyyy-MM-dd HH:mm:ss');
            $dialog.find('#start-datetime').val(GeneralFunctions.formatDate(startDatetime, GlobalVariables.dateFormat, true));

            let endDatetime = Date.parseExact(appointment.end_datetime, 'yyyy-MM-dd HH:mm:ss');
            $dialog.find('#end-datetime').val(GeneralFunctions.formatDate(endDatetime, GlobalVariables.dateFormat, true));

            let customer = appointment.customer;
            $dialog.find('#customer-id').val(appointment.id_users_customer);
            $dialog.find('#first-name').val(customer.first_name);
            $dialog.find('#last-name').val(customer.last_name);
            $dialog.find('#document-number').val(customer.document_number);
            $dialog.find('#email').val(customer.email);
            $dialog.find('#phone-number').val(customer.phone_number);
            $dialog.find('#address').val(customer.address);
            $dialog.find('#city').val(customer.city);
            $dialog.find('#zip-code').val(customer.zip_code);
            $dialog.find('#appointment-notes').val(appointment.notes);
            $dialog.find('#customer-notes').val(customer.notes);

            $dialog.modal('show');
        }

        // Apply qtip to control tooltips.
        $('#calendar-toolbar button').qtip({
            position: {
                my: 'top center',
                at: 'bottom center'
            },
            style: {
                classes: 'qtip-green qtip-shadow custom-qtip'
            }
        });

        $('#select-filter-item').qtip({
            position: {
                my: 'middle left',
                at: 'middle right'
            },
            style: {
                classes: 'qtip-green qtip-shadow custom-qtip'
            }
        });

        if ($('#select-filter-item option').length == 0) {
            $('#calendar-actions button').prop('disabled', true);
        }

        // Fine tune the footer's position only for this page.
        if (window.innerHeight < 700) {
            $('#footer').css('position', 'static');
        }

        let gotoDate = getParameterByName('gotoDate');
        let typeName = getParameterByName('type');
        let optionId = getParameterByName('optionId');

        if (gotoDate) {
            $('#select-filter-item option[type=' + typeName + ']').filter(function () {
                return ($(this).val() == optionId); //To select Blue
            }).prop('selected', 'selected');
            $('#select-filter-item').trigger('change');

            $('#calendar').fullCalendar('gotoDate', gotoDate);

            setTimeout(() => {
                window.history.replaceState({}, document.title, "/" + "index.php/backend"); // limpio la url
            }, 5000);
        }
    };
})(window.BackendCalendarDefaultView);

/**
 * @param String name
 * @return String
 */
function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    let regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

function getProvider(provider_id) {
    if (provider_id == null) {
        return GlobalVariables.availableProviders[0];
    }

    return GlobalVariables.availableProviders.find(function (provider) {
        return provider.id == provider_id;
    });
}

function getService(service_id) {
    if (service_id == null) {
        return GlobalVariables.availableServices[0];
    }
    return GlobalVariables.availableServices.find(function (service) {
        return service.id == service_id;
    });
}

function getFirstAvailableServiceId() {
    let service_id = null;

    $.each(GlobalVariables.availableServices, function (index, service) {
        if (service.active == true && service_id == null) {
            service_id = service.id;
        }
    });

    return service_id;
}