/* ----------------------------------------------------------------------------
 * Easy!Appointments - Open Source Web Scheduler
 *
 * @package     EasyAppointments
 * @author      A.Tselegidis <alextselegidis@gmail.com>
 * @copyright   Copyright (c) 2013 - 2018, Alex Tselegidis
 * @license     http://opensource.org/licenses/GPL-3.0 - GPLv3
 * @link        http://easyappointments.org
 * @since       v1.2.0
 * ---------------------------------------------------------------------------- */

/**
 * Backend Calendar Appointments Modal
 *
 * This module implements the appointments modal functionality.
 *
 * @module BackendCalendarAppointmentsModal
 */
window.BackendCalendarAppointmentsModal = window.BackendCalendarAppointmentsModal || {};

(function (exports) {

    'use strict';

    function _bindEventHandlers() {
        /**
         * Event: Manage Appointments Dialog Cancel Button "Click"
         *
         * Closes the dialog without saving any changes to the database.
         */
        $('#manage-appointment #cancel-appointment').click(function () {
            $('#manage-appointment').modal('hide');
        });

        /**
         * Event: Manage Appointments Dialog Save Button "Click"
         *
         * Stores the appointment changes or inserts a new appointment depending the dialog mode.
         */
        $('#manage-appointment #save-appointment').click(function () {
            // Before doing anything the appointment data need to be validated.
            if (!_validateAppointmentForm()) {
                return;
            }

            // Prepare appointment data for AJAX request.
            let $dialog = $('#manage-appointment');

            // ID must exist on the object in order for the model to update the record and not to perform
            // an insert operation.

            let service = getService($dialog.find('#select-service').val());
            let hour = $('.available-hour.selected-hour').text();
            let date = $('#appointment-date').val();


            let startDatetime = moment(date + ' ' + hour).format('YYYY-MM-DD HH:mm:ss');
            let endDatetime = moment(date + ' ' + hour).add(service.duration, 'minutes').format('YYYY-MM-DD HH:mm:ss');

            //let startDatetime = $dialog.find('#start-datetime').datetimepicker('getDate').toString('yyyy-MM-dd HH:mm:ss');
            //let endDatetime = $dialog.find('#end-datetime').datetimepicker('getDate').toString('yyyy-MM-dd HH:mm:ss');

            let appointment = {
                id_services: $dialog.find('#select-service').val(),
                id_users_provider: $dialog.find('#select-provider').val(),
                start_datetime: startDatetime,
                end_datetime: endDatetime,
                is_unavailable: false,
                created_by: GlobalVariables.user.id,
                observations: $dialog.find('#observations').val(),
            };

            if ($dialog.find('#appointment-id').val() !== '') {
                // Set the id value, only if we are editing an appointment.
                appointment.id = $dialog.find('#appointment-id').val();
            }

            let customer = {
                first_name: $dialog.find('#first-name').val(),
                last_name: $dialog.find('#last-name').val(),
                document_number: $dialog.find('#document-number').val(),
                email: $dialog.find('#email').val(),
                phone_number: $dialog.find('#phone-number').val(),
                address: $dialog.find('#address').val(),
                id_prestadora: $dialog.find('#id_prestadora').val(),
            };

            if ($dialog.find('#customer-id').val() !== '') {
                // Set the id value, only if we are editing an appointment.
                customer.id = $dialog.find('#customer-id').val();
                appointment.id_users_customer = customer.id;
            }

            // Define success callback.
            let successCallback = function (response) {
                if (!GeneralFunctions.handleAjaxExceptions(response)) {
                    $dialog.find('.modal-message').text(EALang.unexpected_issues_occurred);
                    $dialog.find('.modal-message').addClass('alert-danger').removeClass('hidden');
                    return false;
                }

                // Display success message to the user.
                $dialog.find('input', 'textarea', 'select').attr('disabled', true);

                $dialog.find('.modal-message').text(EALang.appointment_saved);
                $dialog.find('.modal-message').addClass('alert-success').removeClass('alert-danger hidden');
                $dialog.find('.modal-body').scrollTop(0);

                // Close the modal dialog and refresh the calendar appointments after one second.
                setTimeout(function () {
                    $dialog.find('.alert').addClass('hidden');
                    $dialog.modal('hide');
                    $('#select-filter-item').trigger('change');
                    $dialog.find('input', 'textarea', 'select').attr('disabled', false);
                }, 1000);
            };

            // Define error callback.
            let errorCallback = function () {
                $dialog.find('.modal-message').text(EALang.service_communication_error);
                $dialog.find('.modal-message').addClass('alert-danger').removeClass('hidden');
                $dialog.find('.modal-body').scrollTop(0);
            };

            // Save appointment data.
            BackendCalendarApi.saveAppointment(appointment, customer, successCallback, errorCallback);
        });

        /**
         * Event: Insert Appointment Button "Click"
         *
         * When the user presses this button, the manage appointment dialog opens and lets the user to
         * create a new appointment.
         */
        $('#insert-appointment').click(function (e) {
            BackendCalendarAppointmentsModal.resetAppointmentDialog();
            let $dialog = $('#manage-appointment');

            // Set the selected filter item and find the next appointment time as the default modal values.
            let providerId = parseInt($('#select-filter-item option:selected').data('provider'));
            let serviceId = parseInt($('#select-filter-item').val());

            let providers = GlobalVariables.availableProviders.filter(function (provider) {
                return parseInt(provider.id) === providerId;
            });

            if (providers.length) {
                $dialog.find('#select-provider').val(providerId).trigger('change');
                //$dialog.find('#select-service').val(providers[0].services[0]);
                $dialog.find('#select-service').val(serviceId);
            }

            //let start = new Date($(this).data('date'));
            let date = null;

            if($(this).data('date')){
                date = $(this).data('date');
            }else{
                date = moment().add(1, 'days').format('YYYY-MM-DD');
            }

            $('#appointment-date').val(date);

            refreshTime();

            // Display modal form.
            $dialog.find('.modal-header h3').text(EALang.new_appointment_title);
            $dialog.modal('show');
        });

        /**
         * Event: Pick Existing Customer Button "Click"
         */
        $('#select-customer').click(function () {
            let $list = $('#existing-customers-list');

            if (!$list.is(':visible')) {
                $(this).text(EALang.hide);
                $list.empty();
                $list.slideDown('slow');
                $('#filter-existing-customers').fadeIn('slow');
                $('#filter-existing-customers').val('');
                $.each(GlobalVariables.customers, function (index, c) {
                    $list.append('<div data-id="' + c.id + '">'
                        + c.first_name + ' ' + c.last_name + '</div>');
                });
            } else {
                $list.slideUp('slow');
                $('#filter-existing-customers').fadeOut('slow');
                $(this).text(EALang.select);
            }
        });

        /**
         * Event: Select Existing Customer From List "Click"
         */
        $('#manage-appointment').on('click', '#existing-customers-list div', function () {
            let id = $(this).attr('data-id');

            $.each(GlobalVariables.customers, function (index, c) {
                if (c.id == id) {
                    $('#customer-id').val(c.id);
                    $('#first-name').val(c.first_name);
                    $('#last-name').val(c.last_name);
                    $('#document-number').val(c.document_number);
                    $('#email').val(c.email);
                    $('#phone-number').val(c.phone_number);
                    $('#id_prestadora').val(c.id_prestadora);

                    return false;
                }
            });

            $('#select-customer').trigger('click'); // hide list
        });

        /**
         * Event: Filter Existing Customers "Change"
         */
        $('#filter-existing-customers').keyup(function () {
            let key = $(this).val().toLowerCase();
            let $list = $('#existing-customers-list');
            let postUrl = GlobalVariables.baseUrl + '/index.php/backend_api/ajax_filter_customers';
            let postData = {
                csrfToken: GlobalVariables.csrfToken,
                key: key
            };

            // Try to get the updated customer list.
            $.ajax({
                type: 'POST',
                url: postUrl,
                data: postData,
                dataType: 'json',
                timeout: 1000,
                global: false,
                success: function (response) {
                    $list.empty();
                    $.each(response, function (index, c) {
                        $list.append('<div data-id="' + c.id + '">'
                            + c.first_name + ' ' + c.last_name + '</div>');

                        // Verify if this customer is on the old customer list.
                        let result = $.grep(GlobalVariables.customers,
                            function (e) {
                                return e.id == c.id;
                            });

                        // Add it to the customer list.
                        if (result.length == 0) {
                            GlobalVariables.customers.push(c);
                        }
                    });
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    // If there is any error on the request, search by the local client database.
                    $list.empty();
                    $.each(GlobalVariables.customers, function (index, c) {
                        if (c.first_name.toLowerCase().indexOf(key) != -1
                            || c.last_name.toLowerCase().indexOf(key) != -1
                            || c.email.toLowerCase().indexOf(key) != -1
                            || c.phone_number.toLowerCase().indexOf(key) != -1
                            || c.address.toLowerCase().indexOf(key) != -1) {
                            $list.append('<div data-id="' + c.id + '">'
                                + c.first_name + ' ' + c.last_name + '</div>');
                        }
                    });
                }
            });
        });

        /**
         * Event: Available Hour "Click"
         *
         * Triggered whenever the user clicks on an available hour
         * for his appointment.
         */
        $('#available-hours').on('click', '.available-hour', function () {
            $('.selected-hour').removeClass('selected-hour');
            $(this).addClass('selected-hour');
        });

        // Cambio el orden de los selects

        $('#select-provider').change(function () {
            let currProviderId = $('#select-provider').val();
            let $serviceSelect = $('#select-service');

            $serviceSelect.attr('disabled', false);
            $serviceSelect.empty();
            $serviceSelect.append('<option selected disabled>Seleccioná un Servicio</option>');

            $.each(GlobalVariables.availableServices, function (indexService, service) {
                $.each(service.providers_id, function (indexProvider, providerId) {
                    // If the current provider is able to provide the selected service,
                    // add him to the listbox.
                    if (providerId === currProviderId) {
                        let optionHtml = '<option value="' + service.id + '">'
                            + service.name
                            + '</option>';
                        $serviceSelect.append(optionHtml);
                    }
                });
            });

            $('#available-hours').html('');

        });

        $('#select-service').change(function () {
            refreshTime();
        });

        $('#appointment-date').change(function () {
            refreshTime();
        });

        /**
         * Event: Enter New Customer Button "Click"
         */
        $('#new-customer').click(function () {
            $('#manage-appointment').find('#customer-id, #first-name, #last-name, #email, #document-number, '
                + '#phone-number, #address, #id_prestadora').val('');
        });
    }

    /**
     * Reset Appointment Dialog
     *
     * This method resets the manage appointment dialog modal to its initial state. After that you can make
     * any modification might be necessary in order to bring the dialog to the desired state.
     */
    exports.resetAppointmentDialog = function () {
        let $dialog = $('#manage-appointment');

        // Empty form fields.
        $dialog.find('input, textarea').val('');
        $dialog.find('.modal-message').fadeOut();
        $dialog.find('#available-hours').html('');

        // Prepare service and provider select boxes.
        $dialog.find('#select-provider').val($dialog.find('#select-provider').eq(0).attr('value'));

        // Fill the providers listbox with providers that can serve the appointment's
        // service and then select the user's provider.
        $dialog.find('#select-provider').empty();

        $.each(GlobalVariables.availableProviders, function (index, provider) {
            let option = new Option(provider.first_name
                + ' ' + provider.last_name, provider.id);
            $dialog.find('#select-provider').append(option);
        });

        $dialog.find('#id_prestadora').empty();

        $dialog.find('#id_prestadora').append('<option selected disabled>'+EALang.select_an_option+'</option>');
        $.each(GlobalVariables.prestadoras, function (index, prestador) {
            let option = new Option(prestador.name, prestador.id);
            $dialog.find('#id_prestadora').append(option);
        });

        // Close existing customers-filter frame.
        $('#existing-customers-list').slideUp('slow');
        $('#filter-existing-customers').fadeOut('slow');
        $('#select-customer').text(EALang.select);


        // Setup start and datetimepickers.
        // Get the selected service duration. It will be needed in order to calculate the appointment end datetime.
        let serviceDuration = 0;

        $.each(GlobalVariables.availableServices, function (index, service) {
            if (service.id === $dialog.find('#select-service').val()) {
                serviceDuration = service.duration;
                return false;
            }
        });

    };

    function refreshTime(){
        let $dialog = $('#manage-appointment');

        BackendCalendarUnavailabilitiesModal.resetDialogAvailablesHours($dialog);
    }

    /**
     * Validate the manage appointment dialog data. Validation checks need to
     * run every time the data are going to be saved.
     *
     * @returns {Boolean} Returns the validation result.
     */
    function _validateAppointmentForm() {
        let $dialog = $('#manage-appointment');

        // Reset previous validation css formatting.
        $dialog.find('.has-error').removeClass('has-error');
        $dialog.find('.modal-message').addClass('hidden');

        try {
            // Check required fields.
            let missingRequiredField = false;

            $dialog.find('.required').each(function () {
                if ($(this).val() == '' || $(this).val() == null) {
                    $(this).closest('.form-group').addClass('has-error');
                    missingRequiredField = true;
                }

                if($(this).is('select')){
                    if(!Number.isInteger($(this).children("option:selected").val() *1)){
                        $(this).parents('.form-group').addClass('has-error');
                        missingRequiredField = true;
                    }
                }

                let minLenght = $(this).attr('minlength');
                let maxLenght = $(this).attr('maxlength');

                if((typeof maxLenght !== 'undefined') && (typeof minLenght === 'undefined')){
                    if ($(this).val().length > maxLenght) {
                        $(this).parents('.form-group').addClass('has-error');
                        missingRequiredField = true;
                    }
                }

                if((typeof maxLenght !== 'undefined') && (typeof minLenght !== 'undefined')){
                    if ($(this).val().length < minLenght || $(this).val().length > maxLenght) {
                        $(this).parents('.form-group').addClass('has-error');
                        missingRequiredField = true;
                    }
                }

            });

            if (missingRequiredField) {
                throw EALang.fields_are_required;
            }

            // Check email address.
            /*if (!GeneralFunctions.validateEmail($dialog.find('#email').val())) {
                $dialog.find('#email').closest('.form-group').addClass('has-error');
                throw EALang.invalid_email;
            }*/

            // Check appointment start and end time.
            let start = $('#start-datetime').datetimepicker('getDate');
            let end = $('#end-datetime').datetimepicker('getDate');
            if (start > end) {
                $dialog.find('#start-datetime, #end-datetime').closest('.form-group').addClass('has-error');
                throw EALang.start_date_before_end_error;
            }

            return true;
        } catch (exc) {
            $dialog.find('.modal-message').addClass('alert-danger').text(exc).removeClass('hidden');
            return false;
        }
    }

    exports.initialize = function () {
        _bindEventHandlers();
    };

})(window.BackendCalendarAppointmentsModal); 
