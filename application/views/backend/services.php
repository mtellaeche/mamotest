<script src="<?= asset_url('assets/js/backend_services_helper.js') ?>"></script>
<script src="<?= asset_url('assets/js/backend_categories_helper.js') ?>"></script>
<script src="<?= asset_url('assets/js/backend_services.js') ?>"></script>
<script src="<?= asset_url('assets/js/working_plan.js') ?>"></script>
<script src="<?= asset_url('assets/ext/jquery-ui/jquery-ui-timepicker-addon.js') ?>"></script>
<script src="<?= asset_url('assets/ext/jquery-jeditable/jquery.jeditable.min.js') ?>"></script>
<script>
    let GlobalVariables = {
        csrfToken: <?= json_encode($this->security->get_csrf_hash()) ?>,
        baseUrl: <?= json_encode($base_url) ?>,
        dateFormat: <?= json_encode($date_format) ?>,
        timeFormat: <?= json_encode($time_format) ?>,
        services: <?= json_encode($services) ?>,
        categories: <?= json_encode($categories) ?>,
        workingPlan: <?= json_encode(json_decode($working_plan)) ?>,
        user: {
            id: <?= $user_id ?>,
            email: <?= json_encode($user_email) ?>,
            role_slug: <?= json_encode($role_slug) ?>,
            privileges: <?= json_encode($privileges) ?>
        }
    };

    $(document).ready(function () {
        BackendServices.initialize(true);
    });
</script>

<div id="services-page" class="container-fluid backend-page">
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active"><a href="#services" aria-controls="services" role="tab"
                                                  data-toggle="tab"><?= lang('services') ?></a></li>
        <li role="presentation"><a href="#categories" aria-controls="categories" role="tab"
                                   data-toggle="tab"><?= lang('categories') ?></a></li>
    </ul>

    <div class="tab-content">

        <!-- SERVICES TAB -->

        <div role="tabpanel" class="tab-pane active" id="services">
            <div class="row">
                <div id="filter-services" class="filter-records column col-xs-12 col-sm-3">
                    <form>
                        <div class="input-group">
                            <input type="text" class="key form-control">

                            <span class="input-group-addon">
                        <div>
                            <button class="filter btn btn-success" type="submit" title="<?= lang('filter') ?>">
                                <span class="glyphicon glyphicon-search"></span>
                            </button>
                            <button class="clear btn btn-info" type="button" title="<?= lang('clear') ?>">
                                <span class="glyphicon glyphicon-repeat"></span>
                            </button>
                        </div>
                    </span>
                        </div>
                    </form>

                    <h3><?= lang('services') ?></h3>
                    <div class="results"></div>
                </div>

                <div class="record-details column col-xs-12 col-sm-7">
                    <!--<div class="btn-toolbar">-->
                    <div class="pull-left">
                        <div class="add-edit-delete-group btn-group">
                            <button id="add-service" class="btn btn-success">
                                <span class="glyphicon glyphicon-plus"></span>
                                <?= lang('add') ?>
                            </button>
                            <button id="edit-service" class="btn btn-warning" disabled="disabled">
                                <span class="glyphicon glyphicon-pencil"></span>
                                <?= lang('edit') ?>
                            </button>
                            <button id="delete-service" class="btn btn-danger" disabled="disabled">
                                <span class="glyphicon glyphicon-remove"></span>
                                <?= lang('delete') ?>
                            </button>
                        </div>

                        <div class="save-cancel-group btn-group" style="display:none;">
                            <button id="save-service" class="btn btn-success">
                                <span class="glyphicon glyphicon-ok"></span>
                                <?= lang('save') ?>
                            </button>
                            <button id="cancel-service" class="btn btn-info">
                                <span class="glyphicon glyphicon-ban-circle"></span>
                                <?= lang('cancel') ?>
                            </button>
                        </div>
                    </div>

                    <div class="switch-view pull-right">
                        <strong><?= lang('current_view') ?></strong>
                        <div class="display-details current"><?= lang('details') ?></div>
                        <div class="display-working-plan"><?= lang('working_plan') ?></div>
                    </div>

                    <div class="details-view provider-view">
                        <h3><?= lang('details') ?></h3>

                        <div class="form-message alert" style="display:none;"></div>

                        <input type="hidden" id="service-id">

                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="service-name"><?= lang('name') ?> *</label>
                                <input id="service-name" class="form-control required" maxlength="128">
                            </div>

                            <div class="form-group">
                                <label for="service-duration"><?= lang('duration_minutes') ?> *</label>
                                <input id="service-duration" class="form-control required" type="number" min="15">
                            </div>

                            <div class="form-group">
                                <label for="service-price"><?= lang('price') ?> *</label>
                                <input id="service-price" class="form-control required">
                            </div>

                            <div class="form-group">
                                <label for="service-currency"><?= lang('currency') ?></label>
                                <input id="service-currency" class="form-control" maxlength="32" value="ARS">
                            </div>

                            <div class="form-group">
                                <label for="service-category"><?= lang('category') ?></label>
                                <select id="service-category" class="form-control"></select>
                            </div>

                            <div class="form-group">
                                <label for="service-active"><?= lang('service_active') ?> </label>
                                <input type="checkbox" id="service-active" value="1">
                            </div>

                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="service-availabilities-type"><?= lang('availabilities_type') ?></label>
                                <select id="service-availabilities-type" class="form-control">
                                    <option value="<?= AVAILABILITIES_TYPE_FLEXIBLE ?>">
                                        <?= lang('flexible') ?>
                                    </option>
                                    <option value="<?= AVAILABILITIES_TYPE_FIXED ?>">
                                        <?= lang('fixed') ?>
                                    </option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="service-attendants-number"><?= lang('attendants_number') ?> *</label>
                                <input id="service-attendants-number" class="form-control required" type="number"
                                       min="1">
                            </div>

                            <div class="form-group">
                                <label for="service-description"><?= lang('description') ?></label>
                                <textarea id="service-description" rows="4" class="form-control"></textarea>
                            </div>

                            <div class="form-group">
                                <label for="service-active_warning"><?= lang('active_warning') ?> </label>
                                <input type="checkbox" id="service-active_warning" value="1">
                            </div>
                            <div class="form-group">
                                <label for="service-warning"><?= lang('warning') ?></label>
                                <textarea id="service-warning" rows="4" class="form-control"></textarea>
                            </div>

                            <p id="form-message" class="text-danger">
                                <em><?= lang('fields_are_required') ?></em>
                            </p>
                        </div>
                    </div>
                    <div class="working-plan-view provider-view" style="display: none;">
                        <h3><?= lang('working_plan') ?></h3>
                        <button id="reset-working-plan" class="btn btn-custom-primary"
                                title="<?= lang('reset_working_plan') ?>">
                            <span class="glyphicon glyphicon-repeat"></span>
                            <?= lang('reset_plan') ?></button>
                        <table class="working-plan table table-striped">
                            <thead>
                            <tr>
                                <th><?= lang('day') ?></th>
                                <th><?= lang('start') ?></th>
                                <th><?= lang('end') ?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" id="sunday">
                                            <?= lang('sunday') ?>
                                        </label>
                                    </div>
                                </td>
                                <td><input id="sunday-start" class="work-start form-control input-sm"></td>
                                <td><input id="sunday-end" class="work-end form-control input-sm"></td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" id="monday">
                                            <?= lang('monday') ?>
                                        </label>
                                    </div>
                                </td>
                                <td><input id="monday-start" class="work-start form-control input-sm"></td>
                                <td><input id="monday-end" class="work-end form-control input-sm"></td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" id="tuesday">
                                            <?= lang('tuesday') ?>
                                        </label>
                                    </div>
                                </td>
                                <td><input id="tuesday-start" class="work-start form-control input-sm"></td>
                                <td><input id="tuesday-end" class="work-end form-control input-sm"></td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" id="wednesday">
                                            <?= lang('wednesday') ?>
                                        </label>
                                    </div>
                                </td>
                                <td><input id="wednesday-start" class="work-start form-control input-sm"></td>
                                <td><input id="wednesday-end" class="work-end form-control input-sm"></td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" id="thursday">
                                            <?= lang('thursday') ?>
                                        </label>
                                    </div>
                                </td>
                                <td><input id="thursday-start" class="work-start form-control input-sm"></td>
                                <td><input id="thursday-end" class="work-end form-control input-sm"></td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" id="friday">
                                            <?= lang('friday') ?>
                                        </label>
                                    </div>
                                </td>
                                <td><input id="friday-start" class="work-start form-control input-sm"></td>
                                <td><input id="friday-end" class="work-end form-control input-sm"></td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" id="saturday">
                                            <?= lang('saturday') ?>
                                        </label>
                                    </div>
                                </td>
                                <td><input id="saturday-start" class="work-start form-control input-sm"></td>
                                <td><input id="saturday-end" class="work-end form-control input-sm"></td>
                            </tr>
                            </tbody>
                        </table>

                        <br>

                        <h3><?= lang('breaks') ?></h3>

                        <span class="help-block">
                            <?= lang('add_breaks_during_each_day') ?>
                        </span>

                        <div>
                            <button type="button" class="add-break btn btn-custom-primary">
                                <span class="glyphicon glyphicon-plus"></span>
                                <?= lang('add_break') ?>
                            </button>
                        </div>

                        <br>

                        <table class="breaks table table-striped">
                            <thead>
                            <tr>
                                <th><?= lang('day') ?></th>
                                <th><?= lang('start') ?></th>
                                <th><?= lang('end') ?></th>
                                <th><?= lang('actions') ?></th>
                            </tr>
                            </thead>
                            <tbody><!-- Dynamic Content --></tbody>
                        </table>

                        <br>

                        <h3><?= lang('closed') ?></h3>

                        <span class="help-block">
                            <?= lang('add_closed_days') ?>
                        </span>

                        <div>
                            <button type="button" class="add-closed btn btn-custom-primary">
                                <span class="glyphicon glyphicon-plus"></span>
                                <?= lang('add_closed') ?>
                            </button>
                        </div>

                        <br>

                        <table class="closed table table-striped">
                            <thead>
                            <tr>
                                <th><?= lang('start') ?></th>
                                <th><?= lang('end') ?></th>
                                <th><?= lang('actions') ?></th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>

                        <br>

                        <!-- VACATION PERIOD -->

                        <h3><?= lang('vacations_period') ?></h3>

                        <span class="help-block">
                            <?= lang('add_vacations_period_days') ?>
                        </span>

                        <div>
                            <button type="button" class="add-vacation-period btn btn-custom-primary">
                                <span class="glyphicon glyphicon-plus"></span>
                                <?= lang('add_vacation_period') ?>
                            </button>
                        </div>

                        <br>

                        <table class="vacation table table-striped">
                            <thead>
                            <tr>
                                <th><?= lang('day') ?></th>
                                <th><?= lang('start') ?></th>
                                <th><?= lang('end') ?></th>
                                <th><?= lang('from') ?></th>
                                <th><?= lang('to') ?></th>
                                <th><?= lang('actions') ?></th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <!-- CATEGORIES TAB -->

        <div role="tabpanel" class="tab-pane" id="categories">
            <div class="row">
                <div id="filter-categories" class="filter-records column col-xs-12 col-sm-5">
                    <form class="input-append">
                        <div class="input-group">
                            <input type="text" class="key form-control">

                            <span class="input-group-addon">
                        <div>
                            <button class="filter btn btn-success" type="submit" title="<?= lang('filter') ?>">
                                <span class="glyphicon glyphicon-search"></span>
                            </button>
                            <button class="clear btn btn-info" type="button" title="<?= lang('clear') ?>">
                                <span class="glyphicon glyphicon-repeat"></span>
                            </button>
                        </div>
                    </span>
                        </div>
                    </form>

                    <h3><?= lang('categories') ?></h3>
                    <div class="results"></div>
                </div>

                <div class="record-details col-xs-12 col-sm-5">
                    <div class="btn-toolbar">
                        <div class="add-edit-delete-group btn-group">
                            <button id="add-category" class="btn btn-success">
                                <span class="glyphicon glyphicon-plus glyphicon glyphicon-white"></span>
                                <?= lang('add') ?>
                            </button>
                            <button id="edit-category" class="btn btn-warning" disabled="disabled">
                                <span class="glyphicon glyphicon-pencil"></span>
                                <?= lang('edit') ?>
                            </button>
                            <button id="delete-category" class="btn btn-danger" disabled="disabled">
                                <span class="glyphicon glyphicon-remove"></span>
                                <?= lang('delete') ?>
                            </button>
                        </div>

                        <div class="save-cancel-group btn-group" style="display:none;">
                            <button id="save-category" class="btn btn-success">
                                <span class="glyphicon glyphicon-ok glyphicon glyphicon-white"></span>
                                <?= lang('save') ?>
                            </button>
                            <button id="cancel-category" class="btn btn-info">
                                <span class="glyphicon glyphicon-ban-circle"></span>
                                <?= lang('cancel') ?>
                            </button>
                        </div>
                    </div>

                    <h3><?= lang('details') ?></h3>

                    <div class="form-message alert" style="display:none;"></div>

                    <input type="hidden" id="category-id">

                    <div class="form-group">
                        <label for="category-name"><?= lang('name') ?> *</label>
                        <input id="category-name" class="form-control required">
                    </div>

                    <div class="form-group">
                        <label for="category-description"><?= lang('description') ?></label>
                        <textarea id="category-description" rows="4" class="form-control"></textarea>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
</script>