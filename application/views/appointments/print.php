<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="theme-color" content="#35A768">

    <title>Imprimir turno</title>

    <link rel="stylesheet" type="text/css" href="<?= asset_url('/assets/ext/bootstrap/css/bootstrap.min.css') ?>">
    <link rel="stylesheet" type="text/css" href="<?= asset_url('/assets/ext/jquery-ui/jquery-ui.min.css') ?>">

    <link rel="icon" type="image/x-icon" href="<?= asset_url('/assets/img/favicon.ico') ?>">
    <link rel="icon" sizes="192x192" href="<?= asset_url('/assets/img/logo.png') ?>">
</head>
<style>
    #printable{
        border: 3px #333 dashed;
        margin: auto;
        padding: 10px 20px;
        width: 400px;
    }
    #printable h4{
        text-align: center;
        text-transform: uppercase;
        width: 100%;
    }
    button{
        margin: 20px auto !important;
        max-width: 200px;
    }
    #printable ul li strong{
        font-size: 16px;
    }
    #printable ul li{
        text-decoration: none;
        list-style: none;
    }
    .logo{
        text-align: center;
        width: 100%;
    }
    img{
        width: 300px;
    }

    @media print {
        button, .title {
            display: none !important;
        }
        #printable ul li strong {
            font-size: 18px;
        }
        #printable{
            font-size: 16px;
            margin-top: 100px;
        }
    }
</style>

<body>
    <div class="container">
        <div class="logo">
            <img src="/assets/img/logo.png" alt="Logo">
        </div>
        <div class="title">
            <h1 class="text-center">Imprimir Turno</h1>
        </div>

        <button onclick="printDiv('printable')" class="btn btn-primary btn-block"><i class="glyphicon glyphicon-print"> </i> Imprimir</button>

        <div id="printable">
            <h4>Datos del centro</h4>
            <ul>
                <li>
                    <strong>Centro: </strong> <?= $appointment['provider']['first_name'] .' '. $appointment['provider']['last_name']?>
                </li>
                <li>
                    <strong>Telefóno: </strong> <?= $appointment['provider']['phone_number'] ?>
                </li>
                <li>
                    <strong>Servicio: </strong> <?= $appointment['service']['name']?>
                </li>
                <li>
                    <strong>Precio: </strong> <?= $appointment['service']['currency'] . ' ' .  $appointment['service']['price']?>
                </li>
                <li>
                    <strong>Día: </strong> <?= date("d-m-Y", strtotime($appointment['start_datetime']));?>
                </li>
                <li>
                    <strong>Horario: </strong> <?= date("H:i", strtotime($appointment['start_datetime'])) . ' a ' .  date("H:i", strtotime($appointment['end_datetime'])) ?>
                </li>
            </ul>
            <h4>Datos del paciente</h4>
            <ul>
                <li>
                    <strong>Nombre: </strong> <?= $appointment['customer']['first_name'] .' '. $appointment['customer']['last_name']?>
                </li>
                <li>
                    <strong>Telefóno: </strong> <?= $appointment['customer']['phone_number'] ?>
                </li>
                <li>
                    <strong>Dni: </strong> <?= $appointment['customer']['document_number']?>
                </li>
                <li>
                    <strong>Email: </strong> <?= $appointment['customer']['email']?>
                </li>
            </ul>
        </div>
    </div>
    <script>
        function printDiv(){
            window.print();
        }
    </script>
</body>
</html>
