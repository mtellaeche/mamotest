<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="theme-color" content="#35A768">

    <title><?= lang('page_title') . ' ' . $company_name ?></title>

    <link rel="stylesheet" type="text/css" href="<?= asset_url('/assets/ext/bootstrap/css/bootstrap.min.css') ?>">
    <link rel="stylesheet" type="text/css" href="<?= asset_url('/assets/ext/jquery-ui/jquery-ui.min.css') ?>">
    <link rel="stylesheet" type="text/css" href="<?= asset_url('/assets/ext/jquery-qtip/jquery.qtip.min.css') ?>">
    <link rel="stylesheet" type="text/css" href="<?= asset_url('/assets/ext/cookieconsent/cookieconsent.min.css') ?>">
    <link rel="stylesheet" type="text/css" href="<?= asset_url('/assets/css/frontend.css') ?>">
    <link rel="stylesheet" type="text/css" href="<?= asset_url('/assets/css/general.css') ?>">

    <link rel="icon" type="image/x-icon" href="<?= asset_url('/assets/img/favicon.ico') ?>">
    <link rel="icon" sizes="192x192" href="<?= asset_url('/assets/img/logo.png') ?>">
    <?php if (!isset($_GET['iframe'])): ?>
        <style>
            #main{
                margin-top: 100px;
            }
        </style>
    <?php endif; ?>
</head>

<body>
<div id="main" class="container">
    <div class="wrapper row">
        <!--<div id="book-appointment-wizard" class="col-xs-12 col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2">-->
        <div id="book-appointment-wizard" class="col-xs-12">
            <?php if (!isset($_GET['iframe'])): ?>
                <div id="company-name">
                    <img src="/assets/img/logo.png" alt="Logo">
                </div>
            <?php endif; ?>
            <div id="header">
                <div id="steps">
                    <div class="col-sm-3 titles">
                        <div id="step-1" class="book-step active-step">
                            <div class="left">1</div>
                            <div class="right">Centro y servicio</div>
                        </div>
                    </div>
                    <div class="col-sm-3 titles">
                        <div id="step-2" class="book-step">
                            <div class="left">2</div>
                            <div class="right">Fecha y horario</div>
                        </div>
                    </div>
                    <div class="col-sm-3 titles">
                        <div id="step-3" class="book-step">
                            <div class="left">3</div>
                            <div class="right">Datos</div>
                        </div>
                    </div>
                    <div class="col-sm-3 titles">
                        <div id="step-4" class="book-step">
                            <div class="left">4</div>
                            <div class="right">Confirmación</div>
                        </div>
                    </div>
                </div>
            </div>

            <?php if ($manage_mode): ?>
                <div id="cancel-appointment-frame" class="booking-header-bar row">
                    <div class="col-xs-12 col-sm-10">
                        <p><?= lang('cancel_appointment_hint') ?></p>
                    </div>
                    <div class="col-xs-12 col-sm-2">
                        <form id="cancel-appointment-form" method="post"
                              action="<?= site_url('appointments/cancel/' . $appointment_data['hash']) ?>">
                            <input type="hidden" name="csrfToken" value="<?= $this->security->get_csrf_hash() ?>"/>
                            <textarea name="cancel_reason" style="display:none"></textarea>
                            <button id="cancel-appointment"
                                    class="btn btn-custom-default btn-sm"><?= lang('cancel') ?></button>
                        </form>
                    </div>
                    <div class="col-xs-12">
                        <p>Si querés modificar tu turno, continuá con el formulario.</p>
                    </div>
                </div>
            <?php endif; ?>

            <?php
            if (isset($exceptions)) {
                echo '<div style="margin: 10px">';
                echo '<h4>' . lang('unexpected_issues') . '</h4>';
                foreach ($exceptions as $exception) {
                    echo exceptionToHtml($exception);
                }
                echo '</div>';
            }
            ?>

            <!--<div id="book-appointment-wizard" class="col-xs-12 col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2">-->

            <!-- SELECT SERVICE AND PROVIDER -->
            <div id="wizard-frame-1" class="wizard-frame col-xs-12 col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2">
                <div class="frame-container">
                    <div class="frame-content">
                        <div class="form-group">
                            <label for="select-provider">
                                <strong><?= lang('select_provider') ?></strong>
                            </label>
                            <!-- TODO: ACA TAMBIEN TENGO QUE TOCAR -->
                            <select id="select-provider" class="col-xs-12 col-sm-4 form-control">
                                <option value="" selected disabled hidden>Seleccioná un Centro</option>
                                <?php
                                // Group services by category, only if there is at least one service with a parent category.
                                foreach ($available_providers as $provider) {
                                    echo '<option value="' . $provider['id'] . '">' . $provider['first_name'] . ' ' . $provider['last_name'] . '</option>';
                                }
                                ?>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="select-service">
                                <strong><?= lang('select_service') ?></strong>
                            </label>

                            <select id="select-service" class="col-xs-12 col-sm-4 form-control" disabled>
                                <option value="" selected disabled hidden title="Seleccioná primero un centro">
                                    Seleccioná un Servicio
                                </option>
                            </select>
                        </div>

                        <div id="service-description" style="display:none;"></div>
                    </div>
                </div>

                <div class="command-buttons">
                    <button type="button" id="button-next-1" class="btn button-next btn-custom-primary"
                            data-step_index="1">
                        <?= lang('next') ?>
                        <span class="glyphicon glyphicon-chevron-right"></span>
                    </button>
                </div>
            </div>

            <!-- SELECT APPOINTMENT DATE -->

            <div id="wizard-frame-2" class="wizard-frame col-xs-12 col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2" style="display:none;">
                <div class="frame-container">
                    <div class="frame-content row">
                        <div class="col-xs-12 col-sm-5">
                            <div id="select-date"></div>
                        </div>

                        <div class="col-xs-12 col-sm-7">
                            <div id="available-hours"></div>
                        </div>
                    </div>
                </div>

                <div class="command-buttons">
                    <button type="button" id="button-back-2" class="btn button-back btn-custom-default"
                            data-step_index="2">
                        <span class="glyphicon glyphicon-chevron-left"></span>
                        <?= lang('back') ?>
                    </button>
                    <button type="button" id="button-next-2" class="btn button-next btn-custom-primary"
                            data-step_index="2">
                        <?= lang('next') ?>
                        <span class="glyphicon glyphicon-chevron-right"></span>
                    </button>
                </div>
            </div>

            <!-- ENTER CUSTOMER DATA -->

            <div id="wizard-frame-3" class="wizard-frame col-xs-12 col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2" style="display:none;">
                <div class="frame-container">
                    <div class="frame-content row">
                        <div class="col-xs-12 col-sm-6">
                            <div class="form-group">
                                <label for="first-name" class="control-label"><?= lang('first_name') ?> *</label>
                                <input type="text" id="first-name" class="required form-control" maxlength="100"/>
                            </div>
                            <div class="form-group">
                                <label for="last-name" class="control-label"><?= lang('last_name') ?> *</label>
                                <input type="text" id="last-name" class="required form-control" maxlength="120"/>
                            </div>
                            <div class="form-group">
                                <label for="document-number" class="control-label"><?= lang('document_number') ?>
                                    *</label>
                                <input type="text" id="document-number" class="required form-control" minlength="7"
                                       maxlength="8" onkeypress="validate(event)"/>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            <div class="form-group">
                                <label for="email" class="control-label"><?= lang('email') ?></label>
                                <input type="email" id="email" class="form-control" maxlength="120"/>
                            </div>
                            <div class="form-group">
                                <label for="phone-number" class="control-label"><?= lang('phone_number') ?> *</label>
                                <input type="text" id="phone-number" class="required form-control" minlength="10"
                                       maxlength="10" onkeypress="validate(event)" placeholder="Número sin 0 y sin 15"/>
                            </div>
                            <div class="form-group">
                                <label for="id_prestadora" class="control-label"><?= lang('prestadora') ?> *</label>
                                <select name="id_prestadora" id="id_prestadora" required class="required form-control"></select>
                            </div>
                        </div>
                        <?php if ($display_terms_and_conditions): ?>
                            <label>
                                <input type="checkbox" class="required" id="accept-to-terms-and-conditions">
                                <?= strtr(lang('read_and_agree_to_terms_and_conditions'),
                                    [
                                        '{$link}' => '<a href="#" data-toggle="modal" data-target="#terms-and-conditions-modal">',
                                        '{/$link}' => '</a>'
                                    ])
                                ?>
                            </label>
                            <br>
                        <?php endif ?>

                        <?php if ($display_privacy_policy): ?>
                            <label>
                                <input type="checkbox" class="required" id="accept-to-privacy-policy">
                                <?= strtr(lang('read_and_agree_to_privacy_policy'),
                                    [
                                        '{$link}' => '<a href="#" data-toggle="modal" data-target="#privacy-policy-modal">',
                                        '{/$link}' => '</a>'
                                    ])
                                ?>
                            </label>
                            <br>
                        <?php endif ?>
                    </div>
                    <div class="frame-content row">
                        <div class="col-xs-12 col-sm-6">
                            <span id="form-message" class="text-danger"><?= lang('fields_are_required') ?></span>
                        </div>
                    </div>
                </div>

                <div class="command-buttons">
                    <button type="button" id="button-back-3" class="btn button-back btn-custom-default"
                            data-step_index="3"><span class="glyphicon glyphicon-chevron-left"></span>
                        <?= lang('back') ?>
                    </button>
                    <button type="button" id="button-next-3" class="btn button-next btn-custom-primary"
                            data-step_index="3">
                        <?= lang('next') ?>
                        <span class="glyphicon glyphicon-chevron-right"></span>
                    </button>
                </div>
            </div>

            <!-- APPOINTMENT DATA CONFIRMATION -->

            <div id="wizard-frame-4" class="wizard-frame col-xs-12 col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2" style="display:none;">
                <div class="frame-container">
                    <div class="frame-content row">
                        <div id="appointment-details" class="col-xs-12 col-sm-6"></div>
                        <div id="customer-details" class="col-xs-12 col-sm-6"></div>
                    </div>
                    <?php if ($this->settings_model->get_setting('require_captcha') === '1'): ?>
                        <div class="frame-content row">
                            <div class="col-xs-12 col-sm-6">
                                <h4 class="captcha-title">
                                    CAPTCHA
                                    <small class="glyphicon glyphicon-refresh"></small>
                                </h4>
                                <img class="captcha-image" src="<?= site_url('captcha') ?>">
                                <input class="captcha-text" type="text" value=""/>
                                <span id="captcha-hint" class="help-block" style="opacity:0">&nbsp;</span>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>

                <div class="command-buttons">
                    <button type="button" id="button-back-4" class="btn button-back btn-custom-default"
                            data-step_index="4">
                        <span class="glyphicon glyphicon-chevron-left"></span>
                        <?= lang('back') ?>
                    </button>
                    <form id="book-appointment-form" style="display:inline-block" method="post">
                        <button id="book-appointment-submit" type="button" class="btn btn-custom-primary">
                            <?= !$manage_mode ? lang('confirm') : lang('update') ?>
                            <span class="glyphicon glyphicon-ok"></span>
                        </button>
                        <input type="hidden" name="csrfToken"/>
                        <input type="hidden" name="post_data"/>
                    </form>
                </div>
            </div>

            <!-- FRAME FOOTER -->

            <!-- <div id="frame-footer">
                    Powered By
                    <a href="http://easyappointments.org" target="_blank">Easy!Appointments</a>
                    |
                    <span id="select-language" class="label label-success">
    		        	<? /*= ucfirst($this->config->item('language')) */ ?>
    		        </span>
                    |
                    <a href="<? /*= site_url('backend'); */ ?>">
                        <? /*= $this->session->user_id ? lang('backend_section') : lang('login') */ ?>
                    </a>
                </div>-->
        </div>
    </div>
</div>

<!-- Modal warning services -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"><?= lang('service_has_alert'); ?></h5>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?= lang('close'); ?></button>
            </div>
        </div>
    </div>
</div>

<?php if ($display_cookie_notice === '1'): ?>
    <?php require 'cookie_notice_modal.php' ?>
<?php endif ?>

<?php if ($display_terms_and_conditions === '1'): ?>
    <?php require 'terms_and_conditions_modal.php' ?>
<?php endif ?>

<?php if ($display_privacy_policy === '1'): ?>
    <?php require 'privacy_policy_modal.php' ?>
<?php endif ?>

<script>
    var GlobalVariables = {
        availableServices: <?= json_encode($available_services) ?>,
        availableProviders: <?= json_encode($available_providers) ?>,
        prestadoras: <?= json_encode($prestadoras) ?>,
        baseUrl: <?= json_encode(config('base_url')) ?>,
        manageMode: <?= $manage_mode ? 'true' : 'false' ?>,
        customerToken: <?= json_encode($customer_token) ?>,
        dateFormat: <?= json_encode($date_format) ?>,
        timeFormat: <?= json_encode($time_format) ?>,
        displayCookieNotice: <?= json_encode($display_cookie_notice === '1') ?>,
        appointmentData: <?= json_encode($appointment_data) ?>,
        providerData: <?= json_encode($provider_data) ?>,
        customerData: <?= json_encode($customer_data) ?>,
        csrfToken: <?= json_encode($this->security->get_csrf_hash()) ?>
    };

    var EALang = <?= json_encode($this->lang->language) ?>;
    var availableLanguages = <?= json_encode($this->config->item('available_languages')) ?>;
</script>

<script src="<?= asset_url('/assets/js/general_functions.js') ?>"></script>
<script src="<?= asset_url('/assets/ext/jquery/jquery.min.js') ?>"></script>
<script src="<?= asset_url('/assets/ext/jquery-ui/jquery-ui.min.js') ?>"></script>
<script src="<?= asset_url('/assets/ext/jquery-qtip/jquery.qtip.min.js') ?>"></script>
<script src="<?= asset_url('/assets/ext/cookieconsent/cookieconsent.min.js') ?>"></script>
<script src="<?= asset_url('/assets/ext/bootstrap/js/bootstrap.min.js') ?>"></script>
<script src="<?= asset_url('/assets/ext/datejs/date.js') ?>"></script>
<script src="<?= asset_url('/assets/js/frontend_book_api.js') ?>"></script>
<script src="<?= asset_url('/assets/js/frontend_book.js') ?>"></script>

<script>
    $(document).ready(function () {
        FrontendBook.initialize(true, GlobalVariables.manageMode);
        GeneralFunctions.enableLanguageSelection($('#select-language'));
    });
</script>

<?php google_analytics_script(); ?>
</body>
</html>