Easy!Appointments: 
    for mamotest
================

### Organize your business! Exploit human resources that can be used in other tasks more efficiently.

**Easy!Appointments** is a highly customizable web application that allows your customers to book
appointments with you via the web. Moreover, it provides the ability to sync your data with
Google Calendar so you can use them with other services. It is an open source project and you
can download and install it **even for commercial use**. Easy!Appointments will run smoothly with
your existing website, because it can be installed in a single folder of the server and of course,
both sites can share the same database.

### WEB access
================
http://localhost/index.php/installation/index
http://localhost/index.php/backend/
http://localhost/index.php/user/login
http://localhost/index.php/backend/update

### Credenciales
================
*Admin de backend*
User: demo
Pass: demo12345

User: superroot
Pass: mytoor1234

*Users demos*
User: dummy101
Pass: hola1234


### Installation
================

### ambiente de: desarrollo by Nikos con Docker, hard reset

*Hard reset: delete*
docker-compose rm -v
docker-compose down -v
rm src/config.php

*Hard reset: re build*
docker-compose up -d --no-deps --build application
docker-compose up -d --no-deps --build database

docker-compose up -d --no-deps --build mamoapp
docker-compose up -d --no-deps --build mamodatabase

### ambiente de: desarrollo by Nikos con Docker, setup

*Ambiente de desarrollo, iniciar*
export $(cat .env | xargs)
docker-compose up -d

*Container up*
docker-compose up -d

*container shell*
docker exec -it easyappointments_database_1 /bin/bash
docker exec -it mamotest_mamodatabase_1 /bin/bash

*Acceder a mysql: restore from file*
mysql -u root -p
mysql> use eamamo;
mysql> source /volumes/database/mamotest.sql;

### ambiente de: desarrollo by Nikos con Docker, reset credenciales admin

*Acceder a mysql: crear user y db*
mysql -u root -p
mysql> use eamamo;
mysql> show tables;

*ver user admins de abm*
select * from ea_users where id_roles = 1;

*ver user admins de backend*
select id_users, username, password, salt from ea_user_settings;


select * from ea_users;
SELECT User FROM mysql.user;
select User, Host, Password from mysql.user;

SHOW GRANTS FOR 'eamamouser';


mysql> CREATE DATABASE mamotest;
mysql> CREATE USER 'eamamouser'@'localhost' IDENTIFIED BY 'veryhardpassword';
mysql> GRANT ALL PRIVILEGES ON mamotest.* TO 'easyappuser'@'localhost';
mysql> FLUSH PRIVILEGES;
mysql> use mamotest;


### notas varias

docker run -it --network mamotest_default --rm mysql mysql -h mamotest_database_1 -u easyappuser -p

cat mamotest.sql | docker exec -i mamotest_database_1 /usr/bin/mysql -u root --password=veryhardpassword mamotest

cat mamotest.sql | /usr/bin/mysql -u root --password=veryhardpassword mamotest

mysql -u easyappuser --password=veryhardpassword mamotest < mamotest.sql

mysql mamotest < mamotest.sql

mysql -u eamamo -p


*Acceder a mysql: crear user y db*
mysql -u root -p

select * from mysql.user;
SELECT User FROM mysql.user;
select User, Host, Password from mysql.user;

SHOW GRANTS FOR 'eamamouser';


mysql> CREATE DATABASE mamotest;
mysql> CREATE USER 'eamamouser'@'localhost' IDENTIFIED BY 'veryhardpassword';
mysql> GRANT ALL PRIVILEGES ON mamotest.* TO 'easyappuser'@'localhost';
mysql> FLUSH PRIVILEGES;
mysql> use mamotest;



### Installation: requicitos y pasos
Since Easy!Appointments is a web application, it runs on a web server and thus you will need to
perform the following steps in order to install the system on your server:

* Make sure that your server has Apache/Nginx, PHP and MySQL installed.
* Create a new database (or use an existing).
* Copy the "easyappointments" source folder on your server.
* Make sure that the "storage" directory is writable.
* Rename the "config-sample.php" file to "config.php" and set your server properties.
* Open your browser on the Easy!Appointments URL and follow the installation guide.
* That's it! You can now use Easy!Appointments at your will.

You will find the latest release at [easyappointments.org](http://easyappointments.org).
If you have problems installing or configuring the application take a look on the
[wiki pages](https://github.com/alextselegidis/easyappointments/wiki) or visit the
[official support group](https://groups.google.com/forum/#!forum/easy-appointments).
You can also report problems on the [issues page](https://github.com/alextselegidis/easyappointments/issues)
and help the development progress.

### Docker
To start Easy!Appointments using Docker in development configuration, with source files mounted into container, run:
```
docker-compose up
```

Production deployment can be made by changing required values in .env file (DB_PASSWORD, APP_URL, APP_PORT) and running:
```
docker-compose -f docker-compose.prod.yml up -d
```

Database data will be stored in named volume `easyappointments_easy-appointments-data`, and app storage (logs, cache, uploads) in `easyappointments_easy-appointments-storage`.
To find where exactly they are stored, you can run 
```
docker volume inspect easyappointments_easy-appointments-storage
```

Production containers will automatically be restarted in case of crash / server reboot. For more info, take a look into `docker-compose.prod.yml` file.

